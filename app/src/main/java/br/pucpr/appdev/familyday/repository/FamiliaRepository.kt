package br.pucpr.appdev.familyday.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.enums.ApiState
import br.pucpr.appdev.familyday.model.Familia
import br.pucpr.appdev.familyday.model.ServiceStatus
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.rest.FamiliaService
import br.pucpr.appdev.familyday.rest.RetrofitFactory
import com.google.gson.Gson
import com.google.gson.JsonObject
import java.lang.Exception
import java.net.UnknownHostException

class FamiliaRepository private constructor() {

    private val mFamiliaService : FamiliaService = RetrofitFactory.getInstance().get().create(FamiliaService::class.java)

    private val mApiState = MutableLiveData<ApiState>(ApiState.STOPPED)

    private val status = ServiceStatus()
    private val statusLiveData = MutableLiveData<ServiceStatus>()

    companion object {
        private var instance : FamiliaRepository? = null
        private const val TAG = "FAMILIA-REPOSITORY"

        fun getInstance() : FamiliaRepository {
            if (instance == null)
                instance = FamiliaRepository()

            return instance!!
        }
    }

    fun status() = statusLiveData as LiveData<ServiceStatus>

    private fun updateStatus(json: JsonObject, state: ApiState) {
        status.codigo = json.getAsJsonPrimitive("codigo").asString
        status.mensagem = json.getAsJsonPrimitive("mensagem").asString
        status.state = state
    }

    suspend fun listarMembros() : List<Usuario> {
        mApiState.postValue(ApiState.RUNNING)
        try {
            val response = mFamiliaService.listarMembros()

            when (response.code()) {
                200 -> {
                    mApiState.postValue(ApiState.OK)
                    return response.body()!!
                }

                else -> {
                    Log.i(TAG, "Erro ao retornar informações da API")
                    Log.d(TAG, "Retorno da API, ${response.errorBody()?.string()}")
                    mApiState.postValue(ApiState.API_ERROR)
                }
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            Log.e(TAG, "Uma exceção foi lançada.")
            Log.e(TAG, e.localizedMessage)
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }

        return listOf()
    }

    suspend fun criar(obj: Familia): Boolean {
        status.state = ApiState.RUNNING
        try {
            val response = mFamiliaService.criar(obj)

            when (response.code()) {
                201 -> {
                    FamilyDayApp.getInstance().saveToken(response.body()!!.getAsJsonObject("retorno").getAsJsonPrimitive("token").asString)
                    status.state = ApiState.OK
                    statusLiveData.postValue(status)
                    return true
                }

                400 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "400: Chamada incorreta para a API")
                    Log.d(TAG, jsonErro.toString())
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                401 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "401: Login incorreto")
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                else -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "${response.code()}: Erro na API")
                    updateStatus(jsonErro, ApiState.UNKNOWN_ERROR)
                }
            }

        } catch (hostException: UnknownHostException) {
            Log.e(TAG, "Não foi possível conectar no host. Mensagem de erro: ${hostException.localizedMessage}", hostException)
            status.state = ApiState.NETWORK_ERROR
        } catch (e: Exception) {
            Log.e(TAG, "Algo não saiu como esperado. Mensagem de erro: ${e.localizedMessage}", e)
            status.state = ApiState.EXCEPTION
        }

        statusLiveData.postValue(status)
        return false
    }
}