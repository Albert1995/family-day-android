package br.pucpr.appdev.familyday.components

import android.R
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.util.AttributeSet
import android.widget.ArrayAdapter
import fr.ganfra.materialspinner.MaterialSpinner


class MultiSpinner : MaterialSpinner, DialogInterface.OnMultiChoiceClickListener,
    DialogInterface.OnCancelListener {

    private var items: List<String>? = null
    private var selected: BooleanArray = booleanArrayOf()
    private var defaultText: String? = null
    private var defaultTextNothing: String? = null
    private var listener: MultiSpinnerListener? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?, attrs: AttributeSet?, resourceId: Int): super(context, attrs, resourceId)

    override fun onClick(dialog: DialogInterface?, which: Int, isChecked: Boolean) {
        selected[which] = isChecked
    }

    override fun onCancel(dialog: DialogInterface?) {
        // refresh text on spinner
        val spinnerBuffer = StringBuffer()
        var someUnselected = false
        var someSelected = false
        for (i in items!!.indices) {
            someSelected = someSelected.or(selected[i])
            if (selected[i]) {
                spinnerBuffer.append(items!![i])
                spinnerBuffer.append(", ")
            } else {
                someUnselected = true
            }
        }
        var spinnerText: String?
        if (!someSelected) {
            spinnerText = defaultTextNothing
        } else if (someUnselected) {
            spinnerText = spinnerBuffer.toString()
            if (spinnerText.length > 2) spinnerText =
                spinnerText.substring(0, spinnerText.length - 2)
        } else {
            spinnerText = defaultText
        }
        val adapter = ArrayAdapter(
            context,
            R.layout.simple_spinner_item, arrayOf(spinnerText)
        )
        setAdapter(adapter)
        setSelection(1)
        listener!!.onItemsSelected(selected)
    }

    override fun performClick(): Boolean {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setMultiChoiceItems(
            items!!.toTypedArray(), selected, this
        )
        builder.setPositiveButton(
            R.string.ok,
            DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })
        builder.setOnCancelListener(this)
        builder.show()
        return true
    }

    fun setItems(
        items: List<String>, allText: String?, nothingText: String?,
        listener: MultiSpinnerListener?
    ) {
        this.items = items
        defaultText = allText
        defaultTextNothing = nothingText
        this.listener = listener

        selected = BooleanArray(items.size)
        //for (i in selected.indices) selected[i] = false

        // all text on the spinner
        val adapter = ArrayAdapter(
            context,
            R.layout.simple_spinner_item, arrayOf(nothingText)
        )
        setAdapter(adapter)
        setSelection(1)
    }

    interface MultiSpinnerListener {
        fun onItemsSelected(selected: BooleanArray?)
    }
}