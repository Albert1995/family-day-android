package br.pucpr.appdev.familyday.ui.usuarios.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.pucpr.appdev.familyday.repository.UsuarioRepository
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CadastroDependenteViewModel: ViewModel() {

    private val mRepo = UsuarioRepository.instance!!

    private val mIsUpdating = MutableLiveData<Boolean>()

    private val mUsuario = MutableLiveData<Usuario>()

    private val mIsSaved = MutableLiveData<Boolean>(false)

    var apelido: String = ""
    var senha: String = ""
    var confirmaSenha: String = ""

    fun isUpdating() = mIsUpdating as LiveData<Boolean>
    fun isSaved() = mIsSaved as LiveData<Boolean>

    fun atualizar() {
        mIsUpdating.postValue(true)
        GlobalScope.launch {
            val usuario = mUsuario.value!!
            usuario.apelido = apelido
            usuario.senha = senha
            usuario.confirmarSenha = confirmaSenha
            usuario.ativo = true
            mRepo.atualizar(usuario)

            mIsUpdating.postValue(false)
            mIsSaved.postValue(true)
        }
    }

    fun buscar() : LiveData<Usuario> {
        mIsUpdating.postValue(true)

        GlobalScope.launch {
            val usuario = mRepo.recuperarInfo()
            mUsuario.postValue(usuario)
            mIsUpdating.postValue(false)
        }

        return mUsuario
    }

}