package br.pucpr.appdev.familyday.ui.tarefas.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.pucpr.appdev.familyday.repository.TarefaRepository
import br.pucpr.appdev.familyday.ui.tarefas.model.TarefaCard
import br.pucpr.appdev.familyday.utils.DateUtils
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class TarefaDashboardViewModel : ViewModel() {

    private val mRepo = TarefaRepository.getInstance()
    private val cards = MutableLiveData<List<TarefaCard>>(listOf())
    private val mIsUpdating = MutableLiveData<Boolean>(false)

    var dataInicio: Date = DateUtils.getTodayDate(false)
    var dataFim: Date = DateUtils.getTodayDate(false)

    fun isUpdating() = mIsUpdating as LiveData<Boolean>

    fun cards() = cards as LiveData<List<TarefaCard>>

    fun buscarDashboardDados(dataInicio: Date, dataFim: Date) {
        mIsUpdating.postValue(true)

        viewModelScope.launch {
            val lista = mRepo.dashboard(dataInicio, dataFim)
            cards.postValue(lista)
            mIsUpdating.postValue(false)
        }
    }

    fun buscarDashboardDados() {
        mIsUpdating.postValue(true)

        viewModelScope.launch {
            val lista = mRepo.dashboard(DateUtils.getTodayDate(false), DateUtils.getTodayDate(false))
            cards.postValue(lista)
            mIsUpdating.postValue(false)
        }
    }

}