package br.pucpr.appdev.familyday.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.databinding.ActivityCadastroFamiliaBinding
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.enums.Constants
import br.pucpr.appdev.familyday.viewmodel.FamiliaViewModel
import com.google.firebase.analytics.FirebaseAnalytics

/**
 * Tela para cadastro da nova família
 */
class CadastroFamiliaActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "FAMILIA-CADASTRO"
        const val PARAM_INTENT_NOME = "nome"
        const val PARAM_INTENT_USUARIO = "usuario"
    }

    private lateinit var familiaViewModel: FamiliaViewModel
    private lateinit var loadingProgress: ProgressDialog
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var binding: ActivityCadastroFamiliaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cadastro_familia)
        familiaViewModel = ViewModelProvider(this).get(FamiliaViewModel::class.java)
        loadingProgress = ProgressDialog()
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        binding.viewModel = familiaViewModel
        familiaViewModel.familia.nome = intent.getStringExtra(PARAM_INTENT_NOME)
        familiaViewModel.familia.dono = intent.getParcelableExtra(PARAM_INTENT_USUARIO)

        initializeViewModel()
    }

    /**
     * Realiza a ação de salvar
     */
    fun salvarOnClick(v: View) {
        familiaViewModel.salvar()
    }

    /**
     * Inicializa os observadores do view model
     */
    private fun initializeViewModel() {
        familiaViewModel.isUpdating().observe(this, Observer {
            if (it) {
                loadingProgress.showMe(TAG, this@CadastroFamiliaActivity.supportFragmentManager)
            } else if (loadingProgress.isShowing()) {
                loadingProgress.dismiss()
            }
        })

        familiaViewModel.isSaved().observe(this, Observer {
            if (it) {
                val bundleAnalytics = Bundle()
                bundleAnalytics.putBoolean("NOVA_FAMILIA", true)
                firebaseAnalytics.logEvent(Constants.GoogleAnalyticsEvents.CADASTRO, bundleAnalytics)
                startActivity(Intent(this, MainActivity::class.java))
                this@CadastroFamiliaActivity.finish()
            }
        })
    }


}
