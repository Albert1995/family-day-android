package br.pucpr.appdev.familyday.model

data class MetaUsuario (
    var usuario: String = "",
    var metas: List<MetaX> = listOf()
) {
    data class MetaX (
        var id: String = "",
        var nome: String = "",
        var pontosAlvo: Int = 0,
        var pontosGanhos: Int = 0
    )
}