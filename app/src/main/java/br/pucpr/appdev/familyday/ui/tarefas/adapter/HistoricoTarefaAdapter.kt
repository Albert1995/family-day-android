package br.pucpr.appdev.familyday.ui.tarefas.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.ui.tarefas.model.HistoricoTarefa
import java.text.SimpleDateFormat
import java.util.*

class HistoricoTarefaAdapter(var lista: List<HistoricoTarefa>) : RecyclerView.Adapter<HistoricoTarefaAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val ctx: Context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(ctx)
        val item: View = inflater.inflate(R.layout.item_list_historico_tarefa, parent, false)
        return ViewHolder(item)
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val historicoTarefa = lista[position]

        if (historicoTarefa.dataExecucao!!.after(Date())) {
            holder.data.text =
                "Será executado em ".plus(SimpleDateFormat("dd/MM/yyyy").format(historicoTarefa.dataExecucao!!))
        } else {
            holder.data.text =
                "Foi executado em ".plus(SimpleDateFormat("dd/MM/yyyy").format(historicoTarefa.dataExecucao!!))
        }

        holder.status.text = historicoTarefa.status
        holder.status.setTextColor(ContextCompat.getColor(FamilyDayApp.getInstance().applicationContext, R.color.status_progresso))
    }

    fun getByPosition(position: Int) : HistoricoTarefa {
        return lista[position]
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val data : TextView = itemView.findViewById(R.id.data)
        val status : TextView = itemView.findViewById(R.id.status)
    }

}