package br.pucpr.appdev.familyday.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.pucpr.appdev.familyday.repository.UsuarioRepository
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private val usuarioRepository = UsuarioRepository.instance!!

    fun getUsuario(): LiveData<Usuario> {
        val usuario = MutableLiveData<Usuario>()

        viewModelScope.launch {
            usuario.postValue(usuarioRepository.recuperarInfo())
        }

        return usuario
    }
}