package br.pucpr.appdev.familyday.ui.usuarios.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.repository.FamiliaRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ListarUsuarioViewModel : ViewModel() {

    private val mRepoFamilia = FamiliaRepository.getInstance()

    private val mUsuarios = MutableLiveData<List<Usuario>>()

    private val mIsUpdating = MutableLiveData<Boolean>(false)

    fun isUpdating() = mIsUpdating as LiveData<Boolean>

    fun usuarios() = mUsuarios as LiveData<List<Usuario>>

    fun listarUsuarios() {
        mIsUpdating.postValue(true)
        GlobalScope.launch {
            val lista = mRepoFamilia.listarMembros()
            mUsuarios.postValue(lista)
            mIsUpdating.postValue(false)
        }
    }



}