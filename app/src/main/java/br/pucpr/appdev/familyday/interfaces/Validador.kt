package br.pucpr.appdev.familyday.interfaces

import br.pucpr.appdev.familyday.model.Erro

abstract class Validador {

    var mensagem = ""

    protected var valido = false

    fun isValido() = valido

    abstract fun validar() : Boolean

}