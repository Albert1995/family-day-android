package br.pucpr.appdev.familyday.ui.tarefas

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.ui.tarefas.adapter.TarefasAdapter
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa
import br.pucpr.appdev.familyday.ui.tarefas.model.TarefaCard
import br.pucpr.appdev.familyday.ui.tarefas.viewmodel.ListarTarefasViewModel
import kotlinx.android.synthetic.main.activity_listar_tarefas.*
import java.util.*

class ListarTarefaActivity : AppCompatActivity() {

    private lateinit var viewModel : ListarTarefasViewModel
    private lateinit var loadingDialog: ProgressDialog
    private lateinit var adapter: TarefasAdapter
    private lateinit var gestureDetector: GestureDetector

    private var retorno = Activity.RESULT_CANCELED

    companion object {
        const val PARAM_INTENT_STATUS = "status"
        const val PARAM_INTENT_DATA_INICIO = "dataInicio"
        const val PARAM_INTENT_DATA_FIM = "dataFim"

        private const val TAG = "LISTAR-TAREFA"
        private const val EDITAR = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listar_tarefas)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel = ViewModelProvider(this).get(ListarTarefasViewModel::class.java)
        loadingDialog = ProgressDialog(TAG, supportFragmentManager)
        loadingDialog.liveDataListener(viewModel.isRunning(), this)

        initializeRecyclerView()
        initializeViewModel()
        initializeGestureDetector()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            setResult(retorno)
            finish()
        }

        return true
    }

    override fun onBackPressed() {
        setResult(retorno)
        super.onBackPressed()
    }

    private fun initializeRecyclerView() {
        adapter = TarefasAdapter(viewModel.getTarefas().value ?: listOf())
        lista.layoutManager = LinearLayoutManager(this)
        lista.adapter = adapter
        lista.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) { }

            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                return gestureDetector.onTouchEvent(e)
            }

            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) { }

        })
    }

    private fun initializeViewModel() {
        viewModel.listarTarefas(intent.getSerializableExtra(PARAM_INTENT_DATA_INICIO) as Date,
            intent.getSerializableExtra(PARAM_INTENT_DATA_FIM) as Date,
            (intent.getSerializableExtra(PARAM_INTENT_STATUS) as TarefaCard.Categoria).status)

        viewModel.getTarefas().observe(this, Observer {
            adapter.setLista(it)
            adapter.notifyDataSetChanged()
        })
    }

    private fun initializeGestureDetector() {
        gestureDetector = GestureDetector(this, object : GestureDetector.SimpleOnGestureListener() {
            override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
                val viewTouched = lista.findChildViewUnder(e.x, e.y)
                if (viewTouched != null) {
                    val vh = lista.getChildViewHolder(viewTouched) as TarefasAdapter.ViewHolder
                    val intent = Intent(this@ListarTarefaActivity, DetalheHistoricoActivity::class.java)
                    intent.putExtra(DetalheHistoricoActivity.PARAM_INTENT_TAREFA_HISTORICO, vh.historico)
                    intent.putExtra(DetalheHistoricoActivity.PARAM_INTENT_TAREFA, vh.tarefa)
                    startActivityForResult(intent, EDITAR)
                }

                return true
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == EDITAR && resultCode == Activity.RESULT_OK) {
            viewModel.listarTarefas(intent.getSerializableExtra(PARAM_INTENT_DATA_INICIO) as Date,
                intent.getSerializableExtra(PARAM_INTENT_DATA_FIM) as Date,
                (intent.getSerializableExtra(PARAM_INTENT_STATUS) as TarefaCard.Categoria).status)
            retorno = resultCode
        }
    }

}