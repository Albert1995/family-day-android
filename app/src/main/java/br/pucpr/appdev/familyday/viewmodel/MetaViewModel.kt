package br.pucpr.appdev.familyday.viewmodel

import android.app.Application
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import br.pucpr.appdev.familyday.enums.Constants
import br.pucpr.appdev.familyday.interfaces.ICadastroCallback
import br.pucpr.appdev.familyday.interfaces.SimpleTextWatcher
import br.pucpr.appdev.familyday.model.Erro
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import br.pucpr.appdev.familyday.rest.RetrofitFactory
import br.pucpr.appdev.familyday.utils.ActivityUtils
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MetaViewModel(application: Application, val cadastroCallback: ICadastroCallback) : AndroidCallbackViewModel(application, cadastroCallback) {

    companion object {
        private const val TAG : String = "META-VM"
    }

    private var meta: Meta =
        Meta()
    private val token : String = ActivityUtils.getOnSharedPreferences(getApplication(), Constants.SharedPreferencesKeys.TOKEN)

    fun setMeta(meta: Meta) {
        this.meta = meta
    }

    fun salvar() {
        if (meta.id.isEmpty()) {
            RetrofitFactory.getInstance().metaService.salvar(token, meta).apply {
                enqueue(object : Callback<JsonObject> {
                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                        Log.e(TAG, "Falha ao salvar a meta")
                        Log.d(TAG, t.message)
                        cadastroCallback.cadastroErro()
                    }

                    override fun onResponse(
                        call: Call<JsonObject>,
                        response: Response<JsonObject>
                    ) {
                        Log.d(TAG, "API Status : %d".format(response.code()))

                        when (response.code()) {
                            201 -> {
                                Log.i(TAG, "Cadastro realizado com sucesso")
                                cadastroCallback.cadastroSucesso()
                            }

                            400 -> {
                                Log.i(TAG, "Falha na validação")
                                val mapper = ObjectMapper()
                                val erro: Erro = mapper.readValue(
                                    response.errorBody()?.string(),
                                    Erro::class.java
                                )
                                Log.d(TAG, erro.codigo.plus(" - ").plus(erro.mensagem))
                                Log.d(TAG, erro.motivo.joinToString())
                                cadastroCallback.cadastroErro()
                            }

                            else -> {
                                Log.i(TAG, "Falha na execução da API")

                                cadastroCallback.cadastroErro()
                            }
                        }
                    }
                })
            }
        } else {
            RetrofitFactory.getInstance().metaService.atualizar(token, meta.id, meta).apply {
                enqueue(object : Callback<JsonObject> {
                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                        Log.e(TAG, "Falha ao salvar a meta")
                        Log.d(TAG, t.message)
                        cadastroCallback.cadastroErro()
                    }

                    override fun onResponse(
                        call: Call<JsonObject>,
                        response: Response<JsonObject>
                    ) {
                        Log.d(TAG, "API Status : %d".format(response.code()))

                        when (response.code()) {
                            200 -> {
                                Log.i(TAG, "Cadastro realizado com sucesso")
                                cadastroCallback.cadastroSucesso()
                            }

                            400 -> {
                                Log.i(TAG, "Falha na validação")
                                val mapper = ObjectMapper()
                                val erro: Erro = mapper.readValue(
                                    response.errorBody()?.string(),
                                    Erro::class.java
                                )
                                Log.d(TAG, erro.codigo.plus(" - ").plus(erro.mensagem))
                                Log.d(TAG, erro.motivo.joinToString())
                                cadastroCallback.cadastroErro()
                            }

                            else -> {
                                Log.i(TAG, "Falha na execução da API")
                                Log.d(TAG, response.errorBody()?.string())
                                cadastroCallback.cadastroErro()
                            }
                        }
                    }
                })
            }
        }
    }

    fun tituloTextWatcher() : TextWatcher {
        return object : SimpleTextWatcher() {
            override fun afterTextChanged(edit: Editable?) {
                meta.titulo = edit.toString()
            }
        }
    }

    fun descricaoTextWatcher() : TextWatcher {
        return object : SimpleTextWatcher() {
            override fun afterTextChanged(edit: Editable?) {
                meta.descricao = edit.toString()
            }
        }
    }

    fun pontosTextWatcher() : TextWatcher {
        return object : SimpleTextWatcher() {
            override fun afterTextChanged(edit: Editable?) {
                meta.pontos = edit.toString().toInt()
            }
        }
    }

    fun setFotoURL(url: String) {
        meta.url = url
    }

}