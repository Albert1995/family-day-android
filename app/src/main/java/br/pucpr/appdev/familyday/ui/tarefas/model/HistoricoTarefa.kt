package br.pucpr.appdev.familyday.ui.tarefas.model

import android.os.Parcelable
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class HistoricoTarefa(
    @get:JsonProperty("id")
    @set:JsonProperty("_id")
    var id: String = "",

    var dataExecucao: Date? = null,
    var status: String = "",
    var membro: String = "",

    @field:JsonProperty("pontosGanhos")
    var pontos: Int = 0,
    var fotos: Array<String> = arrayOf(),
    var comentario: String = "",

    @field:JsonIgnore
    var usuario: Usuario? = null,

    @field:JsonIgnore
    var tarefa: Tarefa? = null
): Parcelable {

    enum class Status(val labelId: Int, val status: String, val color: Int) {
        REFAZER(R.string.historico_refazer, "REFAZER", R.color.status_pendente),
        VALIDACAO(R.string.historico_validacao, "VALIDACAO", R.color.status_validacao),
        APROVADO(R.string.historico_aprovado, "APROVADO", R.color.status_aprovado),
        REPROVADO(R.string.historico_reprovada, "REPROVADO", R.color.status_reprovado),
        PENDENTE(R.string.historico_pendente, "PENDENTE", R.color.status_pendente),
        EXPIRADO(R.string.historico_expirada, "EXPIRADO", R.color.status_expirado);
    }

}