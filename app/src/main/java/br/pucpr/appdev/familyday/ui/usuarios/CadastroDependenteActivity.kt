package br.pucpr.appdev.familyday.ui.usuarios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.databinding.ActivityCadastroDependenteBinding
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.enums.Constants
import br.pucpr.appdev.familyday.ui.MainActivity
import br.pucpr.appdev.familyday.ui.usuarios.viewmodel.CadastroDependenteViewModel

class CadastroDependenteActivity : AppCompatActivity() {

    private lateinit var mViewModel: CadastroDependenteViewModel

    private val mProgress: ProgressDialog = ProgressDialog()

    companion object {
        const val PARAM_INTENT_TOKEN = "token"
        const val TAG = "CADASTRO-DEPENDENTE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityCadastroDependenteBinding = DataBindingUtil.setContentView(this, R.layout.activity_cadastro_dependente)

        FamilyDayApp.getInstance().save(Constants.SharedPreferencesKeys.TOKEN, intent.getStringExtra(PARAM_INTENT_TOKEN))

        mViewModel = ViewModelProvider(this).get(CadastroDependenteViewModel::class.java)
        binding.viewModel = mViewModel

        mViewModel.buscar().observe(this, Observer {

        })

        mViewModel.isUpdating().observe(this, Observer {
            if (it) {
                mProgress.showMe(TAG, supportFragmentManager)
            } else if (mProgress.isShowing()) {
                mProgress.dismiss()
            }
        })

        mViewModel.isSaved().observe(this, Observer {
            if (it) {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        })
    }

    fun atualizarCadastro(v: View) {
        mViewModel.atualizar()
    }
}
