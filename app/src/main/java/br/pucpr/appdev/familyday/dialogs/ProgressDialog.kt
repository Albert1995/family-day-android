package br.pucpr.appdev.familyday.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import br.pucpr.appdev.familyday.R

class ProgressDialog() : DialogFragment() {

    private var showing = false

    private var myTag = ""
    private var myFragmentManager: FragmentManager? = null

    constructor(tag: String, fragmentManager: FragmentManager) : this() {
        this.myTag = tag
        this.myFragmentManager = fragmentManager
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_progress, container)
    }

    fun showMe(tag: String, fragmentManager: FragmentManager) {
        val ft: FragmentTransaction = fragmentManager.beginTransaction()
        val prev: Fragment? = fragmentManager.findFragmentByTag(tag)
        if(prev != null) {
            ft.remove(prev)
        }
        ft.addToBackStack(null)
        this.show(ft, tag)

        showing = true
    }

    fun isShowing() = showing

    fun liveDataListener(liveData: LiveData<Boolean>, lifecycle: LifecycleOwner) {
        liveData.observe(lifecycle, Observer {
            if (it) {
                showMe(myTag, myFragmentManager!!)
            } else if (isShowing()) {
                dismiss()
            }
        })
    }

}