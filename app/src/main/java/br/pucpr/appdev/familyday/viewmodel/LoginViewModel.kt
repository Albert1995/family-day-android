package br.pucpr.appdev.familyday.viewmodel

import androidx.lifecycle.*
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.model.LoginErro
import br.pucpr.appdev.familyday.repository.AutenticacaoRepository
import kotlinx.coroutines.launch

/**
 * Responsável pelas ações de login
 */
class LoginViewModel : ViewModel() {

    companion object {
        private const val TAG: String = "LOGIN-VIEWMODEL"
    }

    private val regexEmail: Regex = Regex("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
    private val updating = MutableLiveData<Boolean>()
    private val logged = MutableLiveData<Boolean>()
    private val autenticacaoRepository = AutenticacaoRepository.instance!!

    var email: String = ""
    var senha: String = ""
    var erro: LoginErro = LoginErro()

    /**
     * Valida os campos do formulário de login
     */
    private fun validar() {
        val app = FamilyDayApp.getInstance()
        erro.email = if (email.isBlank()) app.getString(R.string.login_email_branco)
            else if (email.contains("@") && !email.matches(regexEmail)) app.getString(R.string.login_email_invalido)
            else ""

        erro.senha = if (senha.isBlank()) app.getString(R.string.login_senha_branco)
            else if (senha.length < 6 || senha.length > 12) app.getString(R.string.login_senha_limite)
            else ""
    }

    /**
     * Executa a ação de login, validando as informações e enviando para API
     */
    fun login() {
        validar()

        erro.notifyChange()

        if (erro.hasErro()) {
            logged.postValue(false)
            return
        }

        updating.postValue(true)

        viewModelScope.launch {
            val token = autenticacaoRepository.login(email, senha)
            FamilyDayApp.getInstance().saveToken(token ?: "")
            logged.postValue(token != null)
            updating.postValue(false)
        }
    }

    // GETTERS

    fun isUpdating() = updating as LiveData<Boolean>

    fun isLogged() = logged as LiveData<Boolean>

    fun apiStatus() = autenticacaoRepository.status()

}
