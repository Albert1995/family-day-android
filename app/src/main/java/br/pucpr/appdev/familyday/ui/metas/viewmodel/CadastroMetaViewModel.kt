package br.pucpr.appdev.familyday.ui.metas.viewmodel

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.repository.FirebaseStorageRepository
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import br.pucpr.appdev.familyday.repository.MetaRepository
import br.pucpr.appdev.familyday.ui.metas.model.MetaErro
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CadastroMetaViewModel : ViewModel() {

    companion object {
        private const val TAG = "CADASTRO-META-VM"
    }

    var meta : Meta = Meta()

    var erro : MetaErro = MetaErro()

    var urlFoto : Uri? = null

    var foto: Bitmap? = null

    private val mUrlFoto : MutableLiveData<Uri> = MutableLiveData()

    private val mIsUpdating : MutableLiveData<Boolean> = MutableLiveData()

    private val mIsSaved : MutableLiveData<Boolean> = MutableLiveData(false)

    private val mRepo : MetaRepository = MetaRepository.getInstance()

    private val storageRepository = FirebaseStorageRepository.instance!!

    private fun validar() {
        erro.titulo = if (meta.titulo == "") FamilyDayApp.getInstance().getString(R.string.meta_titulo_branco) else ""
        erro.pontos = if (meta.pontos <= 0) FamilyDayApp.getInstance().getString(R.string.meta_pontos_zero) else ""
    }

    fun salvar() {
        validar()

        erro.notifyChange()
        if (erro.hasErros()) {
            return
        }

        mIsUpdating.postValue(true)

        GlobalScope.launch {
            if (meta.id != "") {
                mRepo.atualizar(meta, urlFoto, foto)
            } else {
                mRepo.criar(meta, urlFoto, foto)
            }

            mIsSaved.postValue(true)
            mIsUpdating.postValue(false)
        }

    }

    fun recuperarFoto() {
        GlobalScope.launch {
            mUrlFoto.postValue(storageRepository.recuperarFoto(meta.url))
        }
    }

    fun isUpdating() = mIsUpdating as LiveData<Boolean>

    fun urlFoto() = mUrlFoto as LiveData<Uri>

    fun apiState() = mRepo.apiState()

    fun isSaved() = mIsSaved as LiveData<Boolean>

}