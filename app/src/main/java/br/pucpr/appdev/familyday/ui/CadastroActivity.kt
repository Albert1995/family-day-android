package br.pucpr.appdev.familyday.ui

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.DatePicker
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.databinding.ActivityCadastroBinding
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.enums.ApiState
import br.pucpr.appdev.familyday.enums.Constants
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.viewmodel.CadastroViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_cadastro.*
import org.apache.commons.lang3.time.DateFormatUtils
import org.apache.commons.lang3.time.DateUtils
import java.util.*

/**
 * Tela de cadastro de novos usuários
 *
 * @author Albert Weihermann
 * @since 1.0
 */
class CadastroActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "CADASTRO-ACTIVITY"

        const val PARAM_INTENT_USUARIO = "usuario"
        const val PARAM_INTENT_TOKEN = "token"
    }

    private lateinit var cadastroViewModel: CadastroViewModel
    private val loadingProgress = ProgressDialog()
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var usuarioViaQRCode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityCadastroBinding = DataBindingUtil.setContentView(this, R.layout.activity_cadastro)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        cadastroViewModel = ViewModelProvider(this).get(CadastroViewModel::class.java)
        binding.viewModel = cadastroViewModel

        group_genero.setOnCheckedChangeListener(this::onChangeGenero)
        edit_data_nascimento.setOnClickListener(this::abrirCalendarioOnClick)

        initializeObservers()

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        val usuario = intent.getParcelableExtra<Usuario>(PARAM_INTENT_USUARIO)
        usuarioViaQRCode = usuario != null
        if (usuarioViaQRCode) {
            termos.visibility = View.GONE
            termos_erro.visibility = View.GONE
            cadastroViewModel.termosAceitos = true
            cadastroViewModel.usuario = usuario
            cadastroViewModel.dataNascimentoTexto = DateFormatUtils.format(usuario.dataNascimento, "dd/MM/yyyy")
            group_genero.check(cadastroViewModel.usuario.genero!!.id)
            cadastroViewModel.usuario.ativo = true

            if (cadastroViewModel.usuario.tipo == Usuario.Tipo.DEPENDENTE) {
                apelido.visibility = View.VISIBLE
                email.hint = getString(R.string.cadastro_usuario_campo_email_opcional)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()

        return true
    }

    /**
     * Salva o usuário
     */
    fun salvarOnClick(v: View) {
        if (usuarioViaQRCode) {
            FamilyDayApp.getInstance().saveToken(intent.getStringExtra(PARAM_INTENT_TOKEN))
        }
        cadastroViewModel.salvar()
    }

    /**
     * Abre a dialog com o calendário para seleção do usuário
     */
    private fun abrirCalendarioOnClick(v: View) {
        val year = DateUtils.toCalendar(cadastroViewModel.usuario.dataNascimento ?: Date()).get(Calendar.YEAR)
        val month = DateUtils.toCalendar(cadastroViewModel.usuario.dataNascimento ?: Date()).get(Calendar.MONTH)
        val day = DateUtils.toCalendar(cadastroViewModel.usuario.dataNascimento ?: Date()).get(Calendar.DAY_OF_MONTH)
        val listener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { _: DatePicker, year: Int, month: Int, day: Int -> run {
            edit_data_nascimento.setText(String.format("%02d/%02d/%04d", day, month + 1, year))
            cadastroViewModel.atualizarDataNascimento()
        } }

        val d = DatePickerDialog(this, listener, year, month, day)
        d.datePicker.maxDate = Date().time
        d.show()
    }

    /**
     * Altera o valor do genero no usuario
     */
    private fun onChangeGenero(rg: RadioGroup, id: Int) {
        cadastroViewModel.usuario.genero = Usuario.Genero.buscarPorId(id)
    }

    /**
     * Inicializa os observadores da UI na ViewModel
     */
    private fun initializeObservers() {
        cadastroViewModel.isUpdating().observe(this, Observer {
            if (it) {
                loadingProgress.showMe(TAG, this@CadastroActivity.supportFragmentManager)
            } else if (loadingProgress.isShowing()) {
                loadingProgress.dismiss()
            }
        })

        cadastroViewModel.isSaved().observe(this, Observer {
            if (it) {
                if (usuarioViaQRCode) {
                    startActivity(Intent(this, MainActivity::class.java))
                } else {
                    val bundleAnalytics = Bundle()
                    bundleAnalytics.putBoolean("NOVO_USUARIO", true)
                    firebaseAnalytics.logEvent(
                        Constants.GoogleAnalyticsEvents.CADASTRO,
                        bundleAnalytics
                    )

                    val intentCadastroFamilia = Intent(this, CadastroFamiliaActivity::class.java)
                    intentCadastroFamilia.putExtra(
                        CadastroFamiliaActivity.PARAM_INTENT_USUARIO,
                        cadastroViewModel.usuario
                    )
                    intentCadastroFamilia.putExtra(
                        CadastroFamiliaActivity.PARAM_INTENT_NOME,
                        cadastroViewModel.usuario.ultimoNome()
                    )
                    startActivity(intentCadastroFamilia)
                }

                this@CadastroActivity.finish()
            }
        })

        cadastroViewModel.apiState().observe(this, Observer {
            if (it.state == ApiState.API_ERROR) {
                when (it.codigo) {
                    "USUARIO_003" -> {
                        cadastroViewModel.erro.email = getString(R.string.cadastro_usuario_email_cadastrado)
                    }
                }
            }
        })
    }

}
