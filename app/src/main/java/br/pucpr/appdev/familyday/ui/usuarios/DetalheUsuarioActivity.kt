package br.pucpr.appdev.familyday.ui.usuarios

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.databinding.ActivityDetalheUsuarioBinding
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.ui.usuarios.viewmodel.DetalheUsuarioViewModel
import br.pucpr.appdev.familyday.utils.DateUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detalhe_usuario.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs

class DetalheUsuarioActivity : AppCompatActivity() {

    companion object {
        const val PARAM_INTENT_USUARIO = "usuario"

        private const val TAG = "DETALHE-USUARIO"
        private val EDITAR = 100
    }

    private lateinit var mViewModel : DetalheUsuarioViewModel
    private lateinit var loadingDialog: ProgressDialog
    private var retorno = Activity.RESULT_CANCELED

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhe_usuario)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mViewModel = ViewModelProvider(this).get(DetalheUsuarioViewModel::class.java)

        loadingDialog = ProgressDialog(TAG, supportFragmentManager)
        loadingDialog.liveDataListener(mViewModel.isUpdating(), this)

        val usuario = intent.getParcelableExtra<Usuario>(PARAM_INTENT_USUARIO)
        val age: Long = TimeUnit.DAYS.convert(abs(Date().time - usuario.dataNascimento!!.time), TimeUnit.MILLISECONDS)
        mViewModel.usuario = usuario
        mViewModel.dataNascimentoTexto = "${DateUtils.safeDateToString("dd/MM/yyyy", usuario.dataNascimento!!)!!} (${age / 365} ${getString(R.string.idade)})"

        popularUI()

        if (!usuario.ativo) {
            mViewModel.buscarQRCode(usuario.id).observe(this, Observer {
                Glide.with(this).load(it).into(qrcode)
            })
        } else {
            qrcode.visibility = View.GONE
            texto_qrcode_1.visibility= View.GONE
            texto_qrcode_2.visibility= View.GONE
        }

        if (FamilyDayApp.getInstance().usuarioLogado!!.tipo == Usuario.Tipo.DEPENDENTE) {
            editar_btn.visibility = View.GONE
        }

        if (usuario.urlFoto.isNotBlank()) {
            mViewModel.carregarFoto().observe(this, Observer {
                Glide.with(this).load(it).into(foto)
            })
        } else {
            foto.borderWidth = 0f
            foto.isOval = false

            if (usuario.tipo == Usuario.Tipo.RESPONSAVEL) {
                if (usuario.genero == Usuario.Genero.FEMININO) {
                    Glide.with(this).load(R.drawable.mother).into(foto)
                } else {
                    Glide.with(this).load(R.drawable.father).into(foto)
                }
            } else {
                if (usuario.genero == Usuario.Genero.FEMININO) {
                    Glide.with(this).load(R.drawable.girl).into(foto)
                } else {
                    Glide.with(this).load(R.drawable.boy).into(foto)
                }
            }
        }
    }

    private fun popularUI() {
        nome.text = mViewModel.usuario.nome
        data.text = mViewModel.dataNascimentoTexto
        genero.text = getString(mViewModel.usuario.genero!!.resId)
        email.text = mViewModel.usuario.email
    }

    override fun onBackPressed() {
        setResult(retorno)
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            setResult(retorno)
            finish()
        }

        return true
    }

    fun editarOnClick(v: View) {
        val intent = Intent(this, CadastroUsuarioActivity::class.java)
        intent.putExtra(CadastroUsuarioActivity.PARAM_INTENT_USUARIO, mViewModel.usuario)
        startActivityForResult(intent, EDITAR)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        retorno = if (requestCode == EDITAR) resultCode else retorno

        if (requestCode == EDITAR && resultCode == Activity.RESULT_OK) {
            mViewModel.atualizar().observe(this, Observer {
                popularUI()
                if (it)
                    mViewModel.carregarFoto()
            })
        }
    }
}
