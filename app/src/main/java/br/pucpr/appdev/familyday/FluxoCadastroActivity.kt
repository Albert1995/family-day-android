package br.pucpr.appdev.familyday

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.pucpr.appdev.familyday.ui.CadastroActivity

class FluxoCadastroActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fluxo_cadastro)

    }

    fun responsavelOnClick(v: View) {
        startActivity(Intent(this, CadastroActivity::class.java))
        finish()
    }

    fun dependenteOnClick(v: View) {
        startActivity(Intent(this, LoginDependenteActivity::class.java))
        finish()
    }

}
