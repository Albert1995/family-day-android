package br.pucpr.appdev.familyday.ui.metas.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable

class MetaErro : BaseObservable() {

    @Bindable var titulo: String = ""
    @Bindable var pontos: String = ""

    fun hasErros() : Boolean {
        return (titulo.isNotBlank())
            .or(pontos.isNotBlank())
    }

}