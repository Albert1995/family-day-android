package br.pucpr.appdev.familyday.ui.usuarios.viewmodel

import android.net.Uri
import android.util.Base64
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.pucpr.appdev.familyday.repository.FirebaseStorageRepository
import br.pucpr.appdev.familyday.repository.UsuarioRepository
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.apache.commons.lang3.mutable.Mutable
import org.apache.commons.lang3.time.DateFormatUtils
import java.util.*

class DetalheUsuarioViewModel: ViewModel() {

    private val mRepo = UsuarioRepository.instance!!
    private val storageRepository = FirebaseStorageRepository.instance!!

    var usuario : Usuario = Usuario()
    var dataNascimentoTexto: String = ""

    private val updating = MutableLiveData<Boolean>()

    fun isUpdating() = updating as LiveData<Boolean>

    fun buscarQRCode(id : String) : LiveData<ByteArray> {
        var m = MutableLiveData<ByteArray>()

        GlobalScope.launch {
            val qrcode = mRepo.buscarQRCode(id)
            m.postValue(Base64.decode(qrcode, Base64.DEFAULT))
        }

        return m
    }

    fun carregarFoto(): LiveData<Uri> {
        val uri = MutableLiveData<Uri>()

        viewModelScope.launch {
            uri.postValue(storageRepository.recuperarFoto(usuario.urlFoto))
        }

        return uri
    }

    fun atualizar(): LiveData<Boolean> {
        val retorno = MutableLiveData<Boolean>()
        updating.postValue(true)

        viewModelScope.launch {
            usuario = mRepo.recuperarInfo()!!
            dataNascimentoTexto = DateFormatUtils.format(usuario.dataNascimento, "dd/MM/yyyy")
            updating.postValue(false)
            retorno.postValue(true)
        }

        return retorno
    }

}