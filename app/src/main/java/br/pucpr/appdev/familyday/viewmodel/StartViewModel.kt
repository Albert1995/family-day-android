package br.pucpr.appdev.familyday.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.repository.UsuarioRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class StartViewModel : ViewModel() {

    private val mRepo : UsuarioRepository = UsuarioRepository.instance!!

    private val mUsuario : MutableLiveData<Usuario> = MutableLiveData()

    init {
        GlobalScope.launch {
            if(FamilyDayApp.getInstance().userToken.isNotBlank())
                mUsuario.postValue( mRepo.recuperarInfo())
            else {
                Thread.sleep(1000)
                mUsuario.postValue(null)
            }
        }
    }

    fun usuario() = mUsuario as LiveData<Usuario>

}