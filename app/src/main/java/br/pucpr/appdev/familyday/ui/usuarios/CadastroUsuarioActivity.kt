package br.pucpr.appdev.familyday.ui.usuarios

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.MenuItem
import android.view.View
import android.widget.DatePicker
import android.widget.RadioGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.databinding.ActivityCadastroUsuarioBinding
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.ui.metas.CadastroMetaActivity
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario

import br.pucpr.appdev.familyday.viewmodel.CadastroViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_cadastro_usuario.*
import org.apache.commons.lang3.time.DateFormatUtils
import org.apache.commons.lang3.time.DateUtils
import pub.devrel.easypermissions.EasyPermissions
import java.util.*

/**
 * Tela de cadastro de novos usuários para a família
 *
 * @author Albert Weihermann
 * @since 1.0
 */
class CadastroUsuarioActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    companion object {
        private const val TAG = "CADASTRO-NOVO-USUARIO"
        const val PARAM_INTENT_USUARIO = "usuario"

        private const val RESULT_CAPTURE_IMAGE = 100
        private const val REQUEST_CAMERA = 101

        private const val RESULT_LOAD_IMAGE = 200
        private const val REQUEST_READ = 201
    }

    private lateinit var mViewModel : CadastroViewModel
    private lateinit var binding : ActivityCadastroUsuarioBinding
    private lateinit var mLoading : ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cadastro_usuario)
        mViewModel = ViewModelProvider(this).get(CadastroViewModel::class.java)
        mLoading = ProgressDialog()

        binding.viewModel = mViewModel

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        initializeGeneroListener()
        initializeTipoListener()
        initializeViewModel()

        add_foto.setOnClickListener(this::loadImageOnClick)
        foto_icon.setOnClickListener(this::loadImageOnClick)
        edit_data_nascimento.setOnClickListener(this::calendarioOnClick)

        val usuario = intent.getParcelableExtra<Usuario>(PARAM_INTENT_USUARIO)
        if (usuario != null) {
            mViewModel.usuario = usuario
            mViewModel.dataNascimentoTexto = DateFormatUtils.format(mViewModel.usuario.dataNascimento!!, "dd/MM/yyyy")
            salvar_btn.text = getString(R.string.atualizar)

            group_genero.check(mViewModel.usuario.genero!!.id)
            group_tipo.check(mViewModel.usuario.tipo!!.id)

            if (usuario.urlFoto.isNotBlank()) {
                mViewModel.carregarFoto().observe(this, Observer {
                    loadPicture(it)
                })
            }
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)

        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        return true
    }

    /**
     * Salva o usuario na API
     */
    fun salvarOnClick(v: View) {
        mViewModel.salvar()
    }

    /**
     * Inicializa os observadores na View Model
     */
    private fun initializeViewModel() {
        mViewModel.termosAceitos = true
        mViewModel.usuario.ativo = false
        mViewModel.usuario.senha = UUID.randomUUID().toString().substring(0..12)
        mViewModel.novoMembroFamilia = true

        mViewModel.isUpdating().observe(this, Observer {  update ->
            if (update) {
                mLoading.showMe(TAG, supportFragmentManager)
            } else if (mLoading.isShowing()) {
                mLoading.dismiss()
            }
        })

        mViewModel.isSaved().observe(this, Observer { saved ->
            if (saved) {
                setResult(Activity.RESULT_OK)
                finish()
            }
        })
    }

    /**
     * Inicializa o observador no genero
     */
    private fun initializeGeneroListener() {
        group_genero.setOnCheckedChangeListener { _: RadioGroup, id: Int ->
            mViewModel.usuario.genero = Usuario.Genero.buscarPorId(id)
        }
    }

    /**
     * Inicializa o observador no tipo
     */
    private fun initializeTipoListener() {
        group_tipo.setOnCheckedChangeListener { _: RadioGroup, id: Int ->
            mViewModel.usuario.tipo = Usuario.Tipo.buscarPorId(id)

            if (id == R.id.radio_dependente) {
                email.hint = getString(R.string.cadastro_usuario_campo_email_opcional)
            } else {
                email.hint = getString(R.string.cadastro_usuario_campo_email)
            }
        }
    }

    /**
     * Abre uma dialog com o calendario
     */
    private fun calendarioOnClick(v: View) {
        val year = DateUtils.toCalendar(mViewModel.usuario.dataNascimento ?: Date()).get(Calendar.YEAR)
        val month = DateUtils.toCalendar(mViewModel.usuario.dataNascimento ?: Date()).get(Calendar.MONTH)
        val day = DateUtils.toCalendar(mViewModel.usuario.dataNascimento ?: Date()).get(Calendar.DAY_OF_MONTH)
        val listener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { _: DatePicker, year: Int, month: Int, day: Int -> run {
            edit_data_nascimento.setText(String.format("%02d/%02d/%04d", day, month + 1, year))
            mViewModel.atualizarDataNascimento()
        } }

        val d = DatePickerDialog(this, listener, year, month, day)
        d.datePicker.maxDate = Date().time
        d.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            loadPicture(data.data as Uri)
        }

        if (requestCode == RESULT_CAPTURE_IMAGE && resultCode == RESULT_OK && data != null) {
            loadPicture(data.extras?.get("data") as Bitmap)
        }
    }

    private fun loadPicture(data: Uri) {
        Glide.with(this).load(data).into(foto_icon)
        placeholder_foto.visibility = View.GONE
        mViewModel.urlFoto = data
        mViewModel.foto = null
    }

    private fun loadPicture(data: Bitmap) {
        Glide.with(this).load(data).into(foto_icon)
        placeholder_foto.visibility = View.GONE
        mViewModel.foto = data
        mViewModel.urlFoto = null
    }

    private fun loadImageOnClick(v: View) {
        val opt = resources.getStringArray(R.array.captura_foto)

        AlertDialog.Builder(this).setTitle("Como deseja resgatar a foto?")
            .setItems(opt) { dialog: DialogInterface, item: Int ->

                when (opt[item]) {
                    opt[0] -> {
                        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)) {
                            openCamera()
                        } else {
                            EasyPermissions.requestPermissions(this, "Necessário acesso para adicionar fotos",
                                REQUEST_CAMERA, Manifest.permission.CAMERA)
                        }
                    }

                    opt[1] -> {
                        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            openGallery()
                        } else {
                            EasyPermissions.requestPermissions(this, "Necessário acesso para adicionar fotos",
                                REQUEST_READ, Manifest.permission.READ_EXTERNAL_STORAGE)
                        }
                    }
                }
            }.create().show()
    }

    private fun openCamera() {
        val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(takePicture, RESULT_CAPTURE_IMAGE)
    }

    private fun openGallery() {
        val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(i, RESULT_LOAD_IMAGE)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        AlertDialog.Builder(this)
            .setMessage(R.string.acesso_imagens_negado)
            .setNeutralButton(AlertDialog.BUTTON_NEUTRAL, null)
            .create()
            .show()
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        when (requestCode) {
            REQUEST_CAMERA -> openCamera()
            REQUEST_READ -> openGallery()
        }
    }
}
