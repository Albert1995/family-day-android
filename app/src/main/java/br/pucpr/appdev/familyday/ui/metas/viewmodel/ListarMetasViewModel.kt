package br.pucpr.appdev.familyday.ui.metas.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import br.pucpr.appdev.familyday.repository.MetaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*
import kotlin.coroutines.CoroutineContext


class ListarMetasViewModel : ViewModel() {

    private val job : Job = Job()
    private val coroutineCtx : CoroutineContext get() = job + Dispatchers.Default
    private val scope : CoroutineScope = CoroutineScope(coroutineCtx)

    private val mRepo : MetaRepository = MetaRepository.getInstance()

    private var mIsRunning : MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = false
    }

    private val mMetas : MutableLiveData<List<Meta>> = MutableLiveData()

    private var dataUltimaBusca : Date

    init {
        atualizarLista()
        dataUltimaBusca = Date()
    }

    fun getMetas() = mMetas as LiveData<List<Meta>>

    fun isRunnning() = mIsRunning as LiveData<Boolean>

    fun atualizarLista() {
        mIsRunning.value = true

        scope.launch {
            var lista = mRepo.buscarTodos()
            mMetas.postValue(lista)
            mIsRunning.postValue(false)
        }
    }

}