package br.pucpr.appdev.familyday.ui

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.interfaces.ICallbackConfiguracoes
import br.pucpr.appdev.familyday.ui.metas.CadastroMetaActivity
import br.pucpr.appdev.familyday.ui.tarefas.CadastroTarefaActivity
import br.pucpr.appdev.familyday.ui.usuarios.CadastroUsuarioActivity
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header_home.*


class MainActivity : AppCompatActivity(), ICallbackConfiguracoes {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)

        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home,
                R.id.navigation_metas,
                R.id.navigation_tarefas,
                R.id.navigation_membros
            )
        )

        navView.setupWithNavController(navController)

        fab_menu_membro.setOnClickListener(this::fabMenuMembroOnClick)
        fab_menu_meta.setOnClickListener(this::fabMenuMetaOnClick)
        fab_menu_tarefa.setOnClickListener(this::fabMenuTarefaOnClick)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""

        val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        drawerNavView.setNavigationItemSelectedListener {
            if (it.itemId == R.id.menu_logout) {
                sair()
            }

            true
        }
    }

    override fun sair() {
        FamilyDayApp.getInstance().removeToken()
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun fabMenuMembroOnClick(v: View?) {
        startActivity(Intent(this, CadastroUsuarioActivity::class.java))
        fab_menu.collapse()
    }

    private fun fabMenuMetaOnClick(v: View?) {
        startActivity(Intent(this, CadastroMetaActivity::class.java))
        fab_menu.collapse()
    }

    private fun fabMenuTarefaOnClick(v: View?) {
        startActivity(Intent(this, CadastroTarefaActivity::class.java))
        fab_menu.collapse()
    }

    fun showCalendarioFiltro() {
        calendario_filtro.visibility = View.VISIBLE
    }

    fun hideCalendarioFiltro() {
        calendario_filtro.visibility = View.GONE
    }

    fun showDadosUsuario(usuario: Usuario) {
        nome_usuario.text = usuario.nome
        perfil_usuario.text = usuario.tipo.toString()

        if (usuario.tipo === Usuario.Tipo.DEPENDENTE) {
            fab_menu.visibility = View.GONE
        }
    }
}
