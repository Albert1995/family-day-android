package br.pucpr.appdev.familyday.ui.usuarios.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.repository.UsuarioRepository
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.ui.usuarios.model.UsuarioErro
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CadastroUsuarioViewModel : ViewModel() {

    companion object {
        private const val REGEX_EMAIL = "(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        private const val REGEX_NOME = "[A-z]+( [A-z]+)+"
    }

    var usuario : Usuario = Usuario()

    var erro : UsuarioErro = UsuarioErro()

    var novoLogin = false

    private val mIsUpdating = MutableLiveData<Boolean>(false)

    private val mIsSaved = MutableLiveData<Boolean>(false)

    private val mRepo : UsuarioRepository = UsuarioRepository.instance!!

    fun isUpdating() = mIsUpdating as LiveData<Boolean>

    fun isSaved() = mIsSaved as LiveData<Boolean>

    fun validar() {

        val app = FamilyDayApp.getInstance()

        erro.nome = if (usuario.nome == "")
            app.getString(R.string.usuario_nome_branco)
        else if (!usuario.nome.matches(Regex(REGEX_NOME)))
            app.getString(R.string.usuario_nome_incompleto)
        else ""

        erro.dataNascimento = if (usuario.dataNascimento == null)
            app.getString(R.string.usuario_data_nascimento_branco)
        else ""

        erro.email = if (usuario.email == "")
            app.getString(R.string.usuario_email_branco)
        else if (!usuario.email.matches(Regex(REGEX_EMAIL)))
            app.getString(R.string.usuario_email_regex)
        else ""

        erro.genero = if (usuario.genero == null)
            app.getString(R.string.usuario_genero_nao_marcado)
        else ""

        erro.tipo = if (usuario.tipo == null && !novoLogin)
            app.getString(R.string.usuario_tipo_nao_marcado)
        else ""

        if (novoLogin) {
            erro.senha = if (usuario.senha == "")
                app.getString(R.string.usuario_senha_branco)
            else if ((usuario.senha.length < 6).or(usuario.senha.length > 12))
                app.getString(R.string.usuario_senha_regra)
            else ""

            erro.confirmarSenha = if (usuario.senha != usuario.confirmarSenha)
                app.getString(R.string.usuario_confirmar_senha_regra)
            else ""
        }

    }

    fun salvar() {
        mIsUpdating.postValue(true)

        usuario.familiaId = FamilyDayApp.getInstance().idFamilia

        GlobalScope.launch {
            validar()

            erro.notifyChange()
            if (erro.hasErro()) {
                mIsUpdating.postValue(false)
                return@launch
            }

            if (usuario.id != "")
                mRepo.atualizar(usuario)
            else
                mRepo.criar(usuario)

            mIsUpdating.postValue(false)
            mIsSaved.postValue(true)
        }
    }

}