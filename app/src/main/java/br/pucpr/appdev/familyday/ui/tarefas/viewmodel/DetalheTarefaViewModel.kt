package br.pucpr.appdev.familyday.ui.tarefas.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa

class DetalheTarefaViewModel: ViewModel() {

    private val mTarefa = MutableLiveData<Tarefa>()

    private val mIsUpdating = MutableLiveData<Boolean>()

    fun isUpdating() = mIsUpdating as LiveData<Boolean>

    fun tarefa() = mTarefa as LiveData<Tarefa>

    init {

    }

    fun buscarTarefa(id: String) {

    }

}