package br.pucpr.appdev.familyday.ui.usuarios

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.ui.usuarios.adapter.UsuarioAdapter
import br.pucpr.appdev.familyday.ui.usuarios.viewmodel.ListarUsuarioViewModel
import kotlinx.android.synthetic.main.fragment_listar_usuarios.*

class ListarUsuarioFragment : Fragment() {

    companion object {
        private const val TAG = "LISTAR-USUARIOS"

        private const val EDITAR_USUARIO = 100
    }

    private lateinit var mViewModel : ListarUsuarioViewModel
    private lateinit var mLoadingDialog : ProgressDialog
    private lateinit var adapter: UsuarioAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_listar_usuarios, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mLoadingDialog = ProgressDialog()
        mViewModel = ViewModelProvider(this).get(ListarUsuarioViewModel::class.java)
        adapter = UsuarioAdapter(mViewModel.usuarios().value ?: listOf(), FamilyDayApp.getInstance().usuarioLogado)

        initializeViewModel()
        initializeGridView()
    }

    private fun initializeViewModel() {
        mViewModel.listarUsuarios()

        mViewModel.isUpdating().observe(viewLifecycleOwner, Observer { updating ->
            if (updating) {
                mLoadingDialog.showMe(TAG, requireActivity().supportFragmentManager)
            } else if (mLoadingDialog.isShowing()) {
                mLoadingDialog.dismiss()
            }
        })

        mViewModel.usuarios().observe(viewLifecycleOwner, Observer {
            adapter.lista = it
            adapter.notifyDataSetChanged()
        })
    }

    private fun initializeGridView() {
        grid_usuarios.adapter = adapter

        grid_usuarios.setOnItemClickListener { _, _, position, _ ->
            val usuarioSelecionado = adapter.getItem(position) as Usuario
            val intentUsuario = Intent(requireContext(), DetalheUsuarioActivity::class.java)
            intentUsuario.putExtra(DetalheUsuarioActivity.PARAM_INTENT_USUARIO, usuarioSelecionado)
            startActivityForResult(intentUsuario, EDITAR_USUARIO)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == EDITAR_USUARIO && resultCode == Activity.RESULT_OK) {
            mViewModel.listarUsuarios()
        }
    }

}