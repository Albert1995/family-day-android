package br.pucpr.appdev.familyday.ui.metas


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.ui.metas.adapter.MetaAdapter
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import br.pucpr.appdev.familyday.ui.metas.viewmodel.ListarMetasViewModel
import kotlinx.android.synthetic.main.fragment_listar_metas.*

class ListarMetasFragment : Fragment() {

    companion object {
        private const val TAG = "META-FRAGMENT"

        private const val EDITAR = 100
    }

    private lateinit var adapter : MetaAdapter
    private lateinit var mViewModel : ListarMetasViewModel
    private lateinit var mProgress : ProgressDialog

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_listar_metas, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mProgress = ProgressDialog()
        mViewModel = ViewModelProvider(this).get(ListarMetasViewModel::class.java)
        adapter = MetaAdapter(mViewModel.getMetas().value ?: listOf(), FamilyDayApp.getInstance().usuarioLogado?.tipo!!)

        initializeGridView()
        initializeViewModel()
    }

    private fun initializeGridView() {
        grid_metas.adapter = adapter

        grid_metas.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, position: Int, id: Long ->
            val meta = adapter.getItem(position) as Meta
            val intent = Intent(this.requireContext(), DetalheMetaActivity::class.java)
            intent.putExtra(DetalheMetaActivity.PARAM_INTENT_META, meta)
            startActivityForResult(intent, EDITAR)
        }
    }

    private fun initializeViewModel() {
        mViewModel.getMetas().observe(viewLifecycleOwner, Observer {
            adapter.lista = it
            adapter.notifyDataSetChanged()
        })

        mViewModel.isRunnning().observe(viewLifecycleOwner, Observer { running ->
            if (running) {
                mProgress.showMe(TAG, requireActivity().supportFragmentManager)
            } else if (mProgress.isShowing()) {
                mProgress.dismiss()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == EDITAR && resultCode == RESULT_OK) {
            mViewModel.atualizarLista()
        }
    }

}