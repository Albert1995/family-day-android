package br.pucpr.appdev.familyday.ui.tarefas

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.components.MultiSpinner
import br.pucpr.appdev.familyday.databinding.ActivityCadastroTarefaBinding
import br.pucpr.appdev.familyday.dialogs.CalendarRangePicker
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import br.pucpr.appdev.familyday.ui.tarefas.viewmodel.CadastroTarefaViewModel
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import com.appeaser.sublimepickerlibrary.SublimePicker
import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate
import com.appeaser.sublimepickerlibrary.helpers.SublimeListenerAdapter
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker
import kotlinx.android.synthetic.main.activity_cadastro_tarefa.*
import org.apache.commons.lang3.time.DateFormatUtils
import org.apache.commons.lang3.time.DateUtils
import java.util.*


class CadastroTarefaActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "CADASTRO-TAREFA"
        const val PARAM_INTENT_TAREFA = "tarefa"
    }

    private lateinit var cadastroTarefaViewModel : CadastroTarefaViewModel
    private lateinit var binding: ActivityCadastroTarefaBinding
    private lateinit var loadingDialog: ProgressDialog
    private lateinit var calendarRangePicker: CalendarRangePicker
    private var dateRange : SelectedDate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cadastro_tarefa)
        cadastroTarefaViewModel = ViewModelProvider(this).get(CadastroTarefaViewModel::class.java)

        binding.activity = this
        binding.viewModel = cadastroTarefaViewModel

        loadingDialog = ProgressDialog(TAG, supportFragmentManager)
        loadingDialog.liveDataListener(cadastroTarefaViewModel.isUpdating(), this)

        initializeListeners()
        initializeViewModel()
        initializeCalendarRangePicker()

        popularSpinnerMetas()
        popularSpinnerUsuarios()

        pontos_edit.setText(cadastroTarefaViewModel.tarefa.pontos.toString())

        recorrencia.setOnCheckedChangeListener { _, isChecked ->
            cadastroTarefaViewModel.recorrencia = isChecked

            if (isChecked) {
                data_fim.visibility = View.VISIBLE
                selecao_dia_semana.visibility = View.VISIBLE
            } else {
                data_fim.visibility = View.GONE
                selecao_dia_semana.visibility = View.GONE

                val semanaArr = cadastroTarefaViewModel.tarefa.semana.toIntArray()
                semanaArr.forEach {
                    when (it) {
                        0 -> diaSemanaOnClick(domingo, it)
                        1 -> diaSemanaOnClick(segunda, it)
                        2 -> diaSemanaOnClick(terca, it)
                        3 -> diaSemanaOnClick(quarta, it)
                        4 -> diaSemanaOnClick(quinta, it)
                        5 -> diaSemanaOnClick(sexta, it)
                        6 -> diaSemanaOnClick(sabado, it)
                    }
                }
            }
        }

    }

    private fun popularSpinnerUsuarios() {
        cadastroTarefaViewModel.getUsuarios().observe(this, Observer { lista ->
            val arr = mutableListOf<String>()

            lista.forEach { usuario ->
                arr.add(usuario.primeiroNome())
            }

            usuarios_multi.setItems(arr, getString(R.string.cadastro_tarefa_item_usuario_todos)
                , getString(R.string.cadastro_tarefa_item_usuario_nenhum)
                , object : MultiSpinner.MultiSpinnerListener {
                override fun onItemsSelected(selected: BooleanArray?) {
                    cadastroTarefaViewModel.tarefa.membrosLista.clear()

                    for (i: Int in lista.indices) {
                        if (selected?.get(i) == true) {
                            cadastroTarefaViewModel.tarefa.membrosLista.add(lista[i])
                        }
                    }
                }
            })
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()

        return true
    }

    fun adicionarPontos(pontos: Int) {
        cadastroTarefaViewModel.tarefa.pontos += pontos

        if (cadastroTarefaViewModel.tarefa.pontos > 1000) {
            cadastroTarefaViewModel.tarefa.pontos = 1000
        } else if (cadastroTarefaViewModel.tarefa.pontos < 0) {
            cadastroTarefaViewModel.tarefa.pontos = 0
        }

        pontos_edit.setText(cadastroTarefaViewModel.tarefa.pontos.toString())
    }

    private fun initializeViewModel() {
        cadastroTarefaViewModel.listarUsuarios()
        cadastroTarefaViewModel.listarMetas()

        cadastroTarefaViewModel.isSaved().observe(this, Observer {
            if (it) {
                finish()
            }
        })
    }

    private fun initializeListeners() {
        edit_data_inicio.setOnClickListener(this::calendarioOnClick)
        edit_data_fim.setOnClickListener(this::calendarioOnClick)

        requer_comprovacao.setOnCheckedChangeListener { _, isChecked ->
            cadastroTarefaViewModel.tarefa.necessarioComprovacao = isChecked
        }
    }

    private fun initializeCalendarRangePicker() {
        val listener = object : SublimeListenerAdapter() {
            override fun onDateTimeRecurrenceSet(
                sublimeMaterialPicker: SublimePicker?,
                selectedDate: SelectedDate?,
                hourOfDay: Int,
                minute: Int,
                recurrenceOption: SublimeRecurrencePicker.RecurrenceOption?,
                recurrenceRule: String?
            ) {
                cadastroTarefaViewModel.tarefa.dataInicio = selectedDate?.startDate?.time
                cadastroTarefaViewModel.tarefa.dataFim = selectedDate?.endDate?.time

                edit_data_inicio.setText(DateFormatUtils.format(selectedDate?.startDate?.time!!, "dd/MM/yyyy"))
                edit_data_fim.setText(DateFormatUtils.format(selectedDate.endDate?.time!!, "dd/MM/yyyy"))

                dateRange = selectedDate

                dismissDialog()
            }

            override fun onCancelled() {
                dismissDialog()
            }

        }

        calendarRangePicker = CalendarRangePicker(listener)
    }

    private fun calendarioOnClick (view: View) {
        val options = SublimeOptions()
        var displayOptions = 0

        displayOptions = displayOptions or SublimeOptions.ACTIVATE_DATE_PICKER
        options.pickerToShow = SublimeOptions.Picker.DATE_PICKER

        options.setDisplayOptions(displayOptions)
        options.setCanPickDateRange(cadastroTarefaViewModel.recorrencia)
        options.setDateRange(Date().time, Long.MIN_VALUE)
        if (dateRange != null) {
            options.dateParams = dateRange!!
        }

        val bundle = Bundle()
        bundle.putParcelable("SUBLIME_OPTIONS", options)
        calendarRangePicker.arguments = bundle
        calendarRangePicker.setStyle(DialogFragment.STYLE_NO_TITLE, 0)

        calendarRangePicker.show(supportFragmentManager, "SUBLIME_PICKER")
    }

    private fun dismissDialog() {
        calendarRangePicker.dismiss()
    }

    private fun popularSpinnerMetas() {
        val arrayCarregando = arrayOf("Carregando as metas...")
        var arrayMetas = arrayOf<String>()
        var spinnerMetasAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayCarregando)
        spinnerMetasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        metas_spinner.adapter = spinnerMetasAdapter

        cadastroTarefaViewModel.getMetas().observe(this, Observer { metas ->
            metas.forEach { meta ->
                arrayMetas += getString(R.string.cadastro_tarefa_item_meta, meta.titulo, meta.pontos)
            }

            spinnerMetasAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayMetas)
            spinnerMetasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            metas_spinner.adapter = spinnerMetasAdapter

            metas_spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) { }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position >= 0)
                        cadastroTarefaViewModel.tarefa.metaTarefa = metas[position]
                }

            }
        })

    }

    fun diaSemanaOnClick(view: View, diaSemanaIndex: Int) {
        val v = view as TextView
        if (v.currentTextColor != ResourcesCompat.getColor(resources, R.color.colorAccent, null)) {
            cadastroTarefaViewModel.tarefa.semana.add(diaSemanaIndex)
            v.setTextColor(ResourcesCompat.getColor(resources, R.color.colorAccent, null))
        } else {
            cadastroTarefaViewModel.tarefa.semana.remove(diaSemanaIndex)
            v.setTextColor(ResourcesCompat.getColor(resources, android.R.color.darker_gray, null))
        }
    }

    fun selecionarMembrosOnClick(v: View) {
        val dialog = AlertDialog.Builder(this)
        var lista: Array<String> = arrayOf<String>()
        var listaObj: Array<Usuario> = arrayOf()
        val selecionados = mutableListOf<Usuario>()
        var bSelecionados = booleanArrayOf()

        cadastroTarefaViewModel.getUsuarios().value?.forEach {
            if (it.tipo == Usuario.Tipo.DEPENDENTE) {
                lista += it.nome
                listaObj += it
                if (cadastroTarefaViewModel.tarefaHasMembro(it)) {
                    bSelecionados += true
                    selecionados.add(it)
                } else {
                    bSelecionados += false
                }
            }
        }

        dialog.setMultiChoiceItems(lista, bSelecionados) { _: DialogInterface, which: Int, isChecked: Boolean ->
            if (isChecked) {
                selecionados.add(listaObj[which])
            } else {
                selecionados.remove(listaObj[which])
            }
        }

        dialog.setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
            val q = selecionados.size
            cadastroTarefaViewModel.tarefa.membrosLista = selecionados

            val s = when (q) {
                0 -> R.string.qtde_membros_nenhum
                1 -> R.string.qtde_membros_singular
                else -> R.string.qtde_membros_plural
            }

            membros_selecionados_text.text = String.format(getString(s), q)
        }

        dialog.setNegativeButton(android.R.string.cancel, null)
        dialog.create().show()
    }

    fun salvarOnClick(v: View) {
        cadastroTarefaViewModel.salvar()
    }

}