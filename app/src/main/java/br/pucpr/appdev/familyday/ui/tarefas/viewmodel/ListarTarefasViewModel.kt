package br.pucpr.appdev.familyday.ui.tarefas.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.repository.FamiliaRepository
import br.pucpr.appdev.familyday.repository.TarefaRepository
import br.pucpr.appdev.familyday.services.TarefaService
import br.pucpr.appdev.familyday.ui.tarefas.model.HistoricoTarefa
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa
import br.pucpr.appdev.familyday.ui.tarefas.model.TarefaCard
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.utils.DateUtils
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class ListarTarefasViewModel : ViewModel() {

    private val mTarefas = MutableLiveData<List<Tarefa>>()

    private val updating = MutableLiveData(false)

    private val tarefaRepository = TarefaRepository.getInstance()
    private val familiaRepository = FamiliaRepository.getInstance()

    private val mTarefaService: TarefaService = TarefaService()

    fun getTarefas() = mTarefas as LiveData<List<Tarefa>>

    fun isRunning() = updating as LiveData<Boolean>
    fun isUpdating() = updating as LiveData<Boolean>

    fun listarTarefas(dataInicio: Date, dataFim: Date, status: String = "null") {
        updating.postValue(true)

        viewModelScope.launch {
            val listaUsuarios = familiaRepository.listarMembros()
            val listaTarefas = tarefaRepository.listar(dataInicio, dataFim, status, FamilyDayApp.getInstance().usuarioLogado!!.tipo!!)

            listaTarefas.forEach { tarefa ->
                tarefa.historico.forEach { historico ->
                    historico.usuario = listaUsuarios.first { usuario -> usuario.id == historico.membro }
                }
            }

            mTarefas.postValue(listaTarefas)
            updating.postValue(false)
        }
    }

    fun listarTarefasPendentesHoje() {
        updating.postValue(true)

        viewModelScope.launch {
            val dataHoje = DateUtils.getTodayDate(false)
            val listaUsuarios = familiaRepository.listarMembros()
            val listaTarefas = mutableListOf<Tarefa>()

            if (FamilyDayApp.getInstance().usuarioLogado!!.tipo!! == Usuario.Tipo.RESPONSAVEL) {
                listaTarefas.addAll(tarefaRepository.listar(dataHoje, dataHoje, TarefaCard.Categoria.VALIDACAO.status, FamilyDayApp.getInstance().usuarioLogado!!.tipo!!))
            }

            listaTarefas.addAll(tarefaRepository.listar(dataHoje, dataHoje, TarefaCard.Categoria.REFAZER.status, FamilyDayApp.getInstance().usuarioLogado!!.tipo!!))
            listaTarefas.addAll(tarefaRepository.listar(dataHoje, dataHoje, TarefaCard.Categoria.PENDENTE.status, FamilyDayApp.getInstance().usuarioLogado!!.tipo!!))
            listaTarefas.addAll(tarefaRepository.listar(dataHoje, dataHoje, TarefaCard.Categoria.APROVADO.status, FamilyDayApp.getInstance().usuarioLogado!!.tipo!!))

            listaTarefas.forEach { tarefa ->
                tarefa.historico.forEach { historico ->
                    historico.usuario = listaUsuarios.first { usuario -> usuario.id == historico.membro }
                }
            }

            mTarefas.postValue(listaTarefas)
            updating.postValue(false)
        }
    }

    fun buscarTarefa(id: String) : LiveData<Tarefa> {
        var mutable = MutableLiveData<Tarefa>()

        GlobalScope.launch {
            var t = mTarefaService.buscarTarefa(id)
            mutable.postValue(t)
        }

        return mutable
    }

    fun completarTarefa(tarefa: Tarefa) : LiveData<Tarefa> {
        var m = MutableLiveData<Tarefa>()

        GlobalScope.launch {
            var t = mTarefaService.completarAtributos(tarefa)
            m.postValue(t)
        }

        return m
    }

}