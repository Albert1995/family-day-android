package br.pucpr.appdev.familyday.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable

class LoginErro: BaseObservable() {

    @Bindable var mensagem: String = ""
    @Bindable var email: String = ""
    @Bindable var senha: String = ""

    fun hasErro(): Boolean {
        return mensagem.isNotBlank()
            .or(email.isNotBlank())
            .or(senha.isNotBlank())
    }

    override fun toString(): String {
        return "LoginErro(mensagem='$mensagem', email='$email', senha='$senha')"
    }

}