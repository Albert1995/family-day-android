package br.pucpr.appdev.familyday.rest

import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.converter.JSONConverter
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

class RetrofitFactory {

    private val authInterceptor = Interceptor { chain ->
        val req = chain
            .request()
            .newBuilder()
            .addHeader("x-access-token", FamilyDayApp.getInstance().userToken)
            .build()

        chain.proceed(req)
    }

    private val apiClient = OkHttpClient().newBuilder()
        .addInterceptor(authInterceptor)
        .build()

    private val retrofit: Retrofit = Retrofit.Builder()
        .client(apiClient)
        .baseUrl("https://api-family-day.herokuapp.com/api/")
        .addConverterFactory(JSONConverter())
        .addConverterFactory(JacksonConverterFactory.create())
        .build()

    val loginService: LoginService by service(LoginService::class.java)
    val usuarioService: UsuarioService by service(UsuarioService::class.java)
    val familiaService: FamiliaService by service(FamiliaService::class.java)
    val metaService: MetaService by service(MetaService::class.java)
    val tarefaService: TarefaService by service(TarefaService::class.java)

    companion object {

        fun getInstance(): RetrofitFactory {
            return RetrofitFactory()
        }

    }


    private fun <T> service(clazz: Class<T>): Lazy<T> {
        return lazy { retrofit.create(clazz) }
    }

    fun get() : Retrofit {
        return retrofit
    }

}