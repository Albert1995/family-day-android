package br.pucpr.appdev.familyday.repository

import androidx.lifecycle.MutableLiveData

interface IRepository<T> {

    fun criar(obj : T, onResult: (criado: Boolean, codigo: Int, mensagem: String?) -> Unit)

    fun atualizar(obj: T, onResult: (atualizado: Boolean, codigo: Int, mensagem: String?) -> Unit)

    fun deletar(id: String, onResult: (excluido: Boolean, codigo: Int, mensagem: String?) -> Unit)

    fun buscarTodos(onResult: (lista: List<T>, codigo: Int, mensagem: String?) -> Unit)

    fun buscarPorId(onResult: (obj: T?, codigo: Int, mensagem: String?) -> Unit)

}