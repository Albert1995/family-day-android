package br.pucpr.appdev.familyday.ui.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.enums.Constants
import br.pucpr.appdev.familyday.fragments.ItemMetaHomeFragment
import br.pucpr.appdev.familyday.ui.MainActivity
import br.pucpr.appdev.familyday.ui.metas.viewmodel.ListarMetasViewModel
import br.pucpr.appdev.familyday.ui.tarefas.DetalheHistoricoActivity
import br.pucpr.appdev.familyday.ui.tarefas.ListarTarefaActivity
import br.pucpr.appdev.familyday.ui.tarefas.adapter.TarefasAdapter
import br.pucpr.appdev.familyday.ui.tarefas.model.TarefaCard
import br.pucpr.appdev.familyday.ui.tarefas.viewmodel.ListarTarefasViewModel
import br.pucpr.appdev.familyday.utils.DateUtils
import br.pucpr.appdev.familyday.viewmodel.MetaViewModel
import com.getbase.floatingactionbutton.FloatingActionsMenu
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_listar_tarefas.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.lista
import kotlinx.android.synthetic.main.fragment_home.login_logo
import kotlinx.android.synthetic.main.item_list_meta.*
import java.util.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var tarefaViewModel : ListarTarefasViewModel
    private lateinit var adapterTarefa : TarefasAdapter
    private lateinit var metaViewModel: ListarMetasViewModel
    private lateinit var gestureDetector: GestureDetector

    companion object {
        private const val EDITAR = 100
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        tarefaViewModel = ViewModelProvider(this).get(ListarTarefasViewModel::class.java)
        metaViewModel = ViewModelProvider(this).get(ListarMetasViewModel::class.java)
        initializeViewModel()
        initializeGestureDetector()
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        carregarTutorial()



        adapterTarefa = TarefasAdapter(tarefaViewModel.getTarefas().value ?: listOf())
        lista.layoutManager = LinearLayoutManager(requireContext())
        lista.adapter = adapterTarefa
        lista.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) { }

            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                return gestureDetector.onTouchEvent(e)
            }

            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) { }

        })
    }

    private fun initializeViewModel() {
        homeViewModel.getUsuario().observe(requireActivity(), Observer {
            FamilyDayApp.getInstance().usuarioLogado = it
            (requireActivity() as MainActivity).showDadosUsuario(it)

            tarefaViewModel.listarTarefasPendentesHoje()

            tarefaViewModel.getTarefas().observe(requireActivity(), Observer {
                adapterTarefa.setLista(it)
                adapterTarefa.notifyDataSetChanged()
            })
        })

        metaViewModel.getMetas().observe(requireActivity(), Observer {
            val adapterFragment = MetaFragmentAdapter(requireActivity().supportFragmentManager, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT)
            val listaMeta = mutableListOf<Fragment>()
            it.forEach {
                listaMeta.add(ItemMetaHomeFragment(it.titulo))
            }
            adapterFragment.lista = listaMeta
            carousel_metas.adapter = adapterFragment
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (this.activity as AppCompatActivity).setSupportActionBar(null)
    }

    private fun initializeGestureDetector() {
        gestureDetector = GestureDetector(requireContext(), object : GestureDetector.SimpleOnGestureListener() {
            override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
                val viewTouched = lista.findChildViewUnder(e.x, e.y)
                if (viewTouched != null) {
                    val vh = lista.getChildViewHolder(viewTouched) as TarefasAdapter.ViewHolder
                    val intent = Intent(requireContext(), DetalheHistoricoActivity::class.java)
                    intent.putExtra(DetalheHistoricoActivity.PARAM_INTENT_TAREFA_HISTORICO, vh.historico)
                    intent.putExtra(DetalheHistoricoActivity.PARAM_INTENT_TAREFA, vh.tarefa)
                    startActivityForResult(intent, EDITAR)
                }

                return true
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == EDITAR && resultCode == Activity.RESULT_OK) {
            tarefaViewModel.listarTarefasPendentesHoje()
            metaViewModel.atualizarLista()
        }
    }

    private fun carregarTutorial() {
        if (FamilyDayApp.getInstance().getBooleanKey(Constants.SharedPreferencesKeys.TUTORIAL)) {
            TapTargetSequence(activity)
                .targets(
                    TapTarget.forView(
                        login_logo,
                        "Seja bem vindo a plataforma Family Day!",
                        "Agora vamos lhe mostrar como navegar no app. Para continuar clique na logo."
                    ).transparentTarget(true),
                    TapTarget.forView(
                        requireActivity().findViewById(R.id.helper_target_fab),
                        "Novo Membro, Novas Metas ou Novas Tarefas",
                        "Quando quiser inserir um desses itens, clique aqui"
                    ).transparentTarget(true).id(2),
                    TapTarget.forView(
                        requireActivity().findViewById(R.id.fab_menu_membro),
                        "Adicione sua família",
                        "Aqui você pode adicionar os membros da sua familia quando quiser"
                    ).transparentTarget(true),
                    TapTarget.forView(
                        requireActivity().findViewById(R.id.fab_menu_meta),
                        "Metas para conquistar",
                        "Já aqui você consegue adicionar as novas metas que desejar"
                    ).transparentTarget(true),
                    TapTarget.forView(
                        requireActivity().findViewById(R.id.fab_menu_tarefa),
                        "Tarefas para manter as metas em dia",
                        "E por último, aqui você adiciona as tarefas e diz quem irá realizar"
                    ).transparentTarget(true).id(5),
                    TapTarget.forView(
                        requireActivity().findViewById<BottomNavigationView>(R.id.nav_view)[0],
                        "Agora a visuzalição",
                        "Navegue entre as telas e descubra o que lhe espera"
                    ).transparentTarget(true),
//                TapTarget.forToolbarNavigationIcon(
//                    (activity as MainActivity).supportActionBar,
//                    "Configurações sempre a postos",
//                    "Sinta-se a vontade para personalizar as configurações como desejar"
//                ).transparentTarget(true),
                    TapTarget.forView(
                        login_logo,
                        "Divirta-se",
                        "Explore, incentive e ensine!"
                    ).transparentTarget(true)
                )
                .listener(
                    object : TapTargetSequence.Listener {
                        override fun onSequenceCanceled(lastTarget: TapTarget?) {
                            FamilyDayApp.getInstance()
                                .save(Constants.SharedPreferencesKeys.TUTORIAL, false)
                        }

                        override fun onSequenceFinish() {

                        }

                        override fun onSequenceStep(
                            lastTarget: TapTarget?,
                            targetClicked: Boolean
                        ) {
                            when (lastTarget?.id()) {
                                2 -> {
                                    requireActivity().findViewById<FloatingActionsMenu>(R.id.fab_menu)
                                        .expand()
                                }

                                5 -> {
                                    requireActivity().findViewById<FloatingActionsMenu>(R.id.fab_menu)
                                        .collapse()
                                }

                            }
                        }

                    }
                ).start()
        }
    }

    class MetaFragmentAdapter(fm: FragmentManager, behavior: Int) :
        FragmentStatePagerAdapter(fm, behavior) {

        var lista : List<Fragment> = listOf()

        override fun getItem(position: Int): Fragment {
            return lista[position]
        }

        override fun getCount(): Int {
            return lista.size
        }

    }

}