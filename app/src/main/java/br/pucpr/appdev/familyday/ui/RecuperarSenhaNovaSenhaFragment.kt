package br.pucpr.appdev.familyday.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.databinding.FragmentRecuperarSenhaCodigoBinding
import br.pucpr.appdev.familyday.databinding.FragmentRecuperarSenhaNovaSenhaBinding
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.viewmodel.RecuperarSenhaViewModel
import kotlinx.android.synthetic.main.fragment_recuperar_senha_email.*
import kotlinx.android.synthetic.main.fragment_recuperar_senha_email.enviar_btn
import kotlinx.android.synthetic.main.fragment_recuperar_senha_email.texto
import kotlinx.android.synthetic.main.fragment_recuperar_senha_nova_senha.*

class RecuperarSenhaNovaSenhaFragment : Fragment() {

    companion object {
        private const val TAG = "RECOVER-PASS-3"
    }

    private lateinit var viewModel : RecuperarSenhaViewModel
    private lateinit var loadingDialog: ProgressDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(requireActivity()).get(RecuperarSenhaViewModel::class.java)
        loadingDialog = ProgressDialog(TAG, requireActivity().supportFragmentManager)

        val binding : FragmentRecuperarSenhaNovaSenhaBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_recuperar_senha_nova_senha, container, false)
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadingDialog.liveDataListener(viewModel.isUpdating(), this)

        initializeViewModel()

        enviar_btn.setOnClickListener(this::enviarOnClick)
    }

    private fun enviarOnClick(v: View) {
        senha.error = ""
        confirmaSenha.error = ""

        if (viewModel.senha.isBlank() && (viewModel.senha.length < 6 || viewModel.senha.length > 12)) {
            senha.error = getString(R.string.recuperar_senha_error_senha)
        } else if (viewModel.senha != viewModel.confirmaSenha) {
            confirmaSenha.error = getString(R.string.recuperar_senha_error_senha)
        } else {
            viewModel.renovarSenha()
        }
    }

    private fun initializeViewModel() {
        viewModel.hasSent().observe(viewLifecycleOwner, Observer {
            if (it && viewModel.email.isNotBlank() && viewModel.codigo.isNotBlank() && viewModel.senha.isNotBlank()) {
                requireActivity().finish()
            }
        })
    }

}
