package br.pucpr.appdev.familyday.services

import br.pucpr.appdev.familyday.repository.FamiliaRepository
import br.pucpr.appdev.familyday.repository.MetaRepository
import br.pucpr.appdev.familyday.repository.TarefaRepository
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa

class TarefaService {

    private val tarefaRepo : TarefaRepository = TarefaRepository.getInstance()
    private val metaRepo : MetaRepository = MetaRepository.getInstance()
    private val familiaRepo : FamiliaRepository = FamiliaRepository.getInstance()

    suspend fun buscarTarefa(id: String): Tarefa? {

        val tarefa = tarefaRepo.buscarPorId(id)

        if (tarefa != null) {
            tarefa.metaTarefa = metaRepo.buscarPorId(id)

            val membrosFamilia = familiaRepo.listarMembros()
            for (i in 0..tarefa.membrosLista.size) {
                membrosFamilia.forEach {
                    if (tarefa.membrosLista[i].id == it.id) {
                        tarefa.membrosLista[i] = it
                    }
                }
            }
        }

        return tarefa
    }

    suspend fun completarAtributos(tarefa: Tarefa) : Tarefa {
        if (tarefa != null) {
            tarefa.metaTarefa = metaRepo.buscarPorId(tarefa.metaTarefa!!.id)

            val membrosFamilia = familiaRepo.listarMembros()
            for (i in 0..(tarefa.membrosLista.size - 1)) {
                membrosFamilia.forEach {
                    if (tarefa.membrosLista[i].id == it.id) {
                        tarefa.membrosLista[i] = it
                    }
                }
            }
        }

        return tarefa
    }

}