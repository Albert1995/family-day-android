package br.pucpr.appdev.familyday.dto

data class MetaDTO(
    var id : String = "",
    var titulo : String = "",
    var descricao : String = "",
    var pontosAlvo : Int = 0
)