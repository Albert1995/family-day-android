package br.pucpr.appdev.familyday.components

import android.content.Context
import android.util.AttributeSet
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout

class CustomTextInputLayout(context: Context?, attrs: AttributeSet?) :
    TextInputLayout(context, attrs) {

    companion object {

        @BindingAdapter("app:errorText")
        @JvmStatic
        fun setErrorText(view: TextInputLayout, text: String) {
            view.error = text
        }

    }

}
