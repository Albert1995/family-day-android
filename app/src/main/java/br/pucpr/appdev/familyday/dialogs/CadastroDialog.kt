package br.pucpr.appdev.familyday.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import br.pucpr.appdev.familyday.R
import java.lang.IllegalArgumentException

class CadastroDialog: DialogFragment() {

    companion object {
        fun newInstance(): CadastroDialog {
            val inst: CadastroDialog = CadastroDialog()
            val args: Bundle = Bundle()

            return inst
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_cadastro_loading, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog?.window?.setGravity(Gravity.CENTER)
    }

}