package br.pucpr.appdev.familyday.ui.metas.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import com.bumptech.glide.Glide
import com.google.firebase.storage.FirebaseStorage
import com.makeramen.roundedimageview.RoundedImageView
import java.io.File
import kotlin.random.Random

class MetaFragmentAdapter(var metas: List<Meta>) : RecyclerView.Adapter<MetaFragmentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val ctx: Context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(ctx)
        val item: View = inflater.inflate(R.layout.item_list_meta, parent, false)
        return ViewHolder(
            item
        )
    }

    override fun getItemCount(): Int {
        return metas.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val m = metas[position]

        holder.meta = m
        holder.text.text = m.titulo
        holder.pontuacao.text = String.format("0 / ${m.pontos}")
        //holder.progresso.progress = Random.nextInt(0, 100)
        holder.loadImage()
    }

    fun getByPosition(position: Int) : Meta {
        return metas[position]
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val text : TextView = itemView.findViewById(R.id.titulo)
        val pontuacao : TextView = itemView.findViewById(R.id.pontos)
//        val progresso : ProgressBar = itemView.findViewById(R.id.progresso)
//        val foto : RoundedImageView = itemView.findViewById(R.id.foto)
        lateinit var meta: Meta

        fun loadImage() {
            if (meta.url != "") {
                val temp = File.createTempFile("image", ".jpg")
                val ref = FirebaseStorage.getInstance().getReference(meta.url)
//                Glide.with(itemView).load(ref).into(foto)
            }
        }

    }

}