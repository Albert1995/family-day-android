package br.pucpr.appdev.familyday.model

import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import com.fasterxml.jackson.annotation.JsonGetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

data class Familia(
    @set:JsonProperty("_id")
    @get:JsonIgnore
    var id: String = "",

    var nome: String = "",

    @field:JsonIgnore
    var dono: Usuario,

    @field:JsonIgnore
    var membros: List<Usuario> = mutableListOf()
) {

    @JsonGetter
    @JsonProperty("dono")
    fun getUsuarioDono(): String {
        return dono.id
    }

    @JsonGetter
    @JsonProperty("membros")
    fun getUsuariosMembros(): Array<String> {
        var result = arrayOf<String>()

        membros.forEach { usuario ->
            result += usuario.id
        }

        return result
    }

}