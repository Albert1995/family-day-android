package br.pucpr.appdev.familyday.model

import androidx.databinding.BaseObservable

class FamiliaErro: BaseObservable() {

    var nome = ""

    fun hasErros(): Boolean {
        return nome.isNotBlank()
    }
}