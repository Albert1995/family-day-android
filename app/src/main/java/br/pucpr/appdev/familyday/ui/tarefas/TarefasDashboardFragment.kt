package br.pucpr.appdev.familyday.ui.tarefas

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.dialogs.CalendarRangePicker
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.generated.callback.OnClickListener
import br.pucpr.appdev.familyday.ui.MainActivity
import br.pucpr.appdev.familyday.ui.tarefas.model.TarefaCard
import br.pucpr.appdev.familyday.ui.tarefas.viewmodel.TarefaDashboardViewModel
import com.appeaser.sublimepickerlibrary.SublimePicker
import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate
import com.appeaser.sublimepickerlibrary.helpers.SublimeListenerAdapter
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker
import kotlinx.android.synthetic.main.fragment_tarefas_dashboard.*
import kotlinx.android.synthetic.main.item_card.view.*

class TarefasDashboardFragment : Fragment() {

    companion object {
        private const val TAG = "DASH-TAREFAS"
        private const val EDITAR = 100
    }

    private lateinit var viewModel : TarefaDashboardViewModel
    private lateinit var progressDialog : ProgressDialog
    private lateinit var calendarRangePicker: CalendarRangePicker

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(TarefaDashboardViewModel::class.java)
        viewModel.buscarDashboardDados()

        progressDialog = ProgressDialog(TAG, requireActivity().supportFragmentManager)

        return inflater.inflate(R.layout.fragment_tarefas_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        progressDialog.liveDataListener(viewModel.isUpdating(), viewLifecycleOwner)
        initializeViewModel()
        initializeCalendarRangePicker()

        requireActivity().findViewById<ImageButton>(R.id.calendario_filtro).setOnClickListener(this::calendarioOnClick)
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).showCalendarioFiltro()
    }

    override fun onPause() {
        super.onPause()
        (requireActivity() as MainActivity).hideCalendarioFiltro()
    }

    private fun initializeCalendarRangePicker() {
        val listener = object : SublimeListenerAdapter() {
            override fun onDateTimeRecurrenceSet(
                sublimeMaterialPicker: SublimePicker?,
                selectedDate: SelectedDate?,
                hourOfDay: Int,
                minute: Int,
                recurrenceOption: SublimeRecurrencePicker.RecurrenceOption?,
                recurrenceRule: String?
            ) {
                requireActivity().findViewById<ImageButton>(R.id.calendario_filtro).isEnabled = true
                viewModel.buscarDashboardDados(selectedDate?.startDate?.time!!, selectedDate.endDate?.time!!)
                viewModel.dataInicio = selectedDate.startDate?.time!!
                viewModel.dataFim = selectedDate.endDate?.time!!
                dismissDialog()
            }

            override fun onCancelled() {
                requireActivity().findViewById<ImageButton>(R.id.calendario_filtro).isEnabled = true
                dismissDialog()
            }

        }

        calendarRangePicker = CalendarRangePicker(listener)
    }

    private fun calendarioOnClick(view: View) {
        (view as ImageButton).isEnabled = false

        val options = SublimeOptions()
        var displayOptions = 0

        displayOptions = displayOptions or SublimeOptions.ACTIVATE_DATE_PICKER
        options.pickerToShow = SublimeOptions.Picker.DATE_PICKER

        options.setDisplayOptions(displayOptions)
        options.setCanPickDateRange(true)

        val bundle = Bundle()
        bundle.putParcelable("SUBLIME_OPTIONS", options)
        calendarRangePicker.arguments = bundle
        calendarRangePicker.setStyle(DialogFragment.STYLE_NO_TITLE, 0)
        calendarRangePicker.show(requireActivity().supportFragmentManager, "SUBLIME_PICKER")
    }

    private fun dismissDialog() {
        calendarRangePicker.dismiss()
    }

    private fun initializeViewModel() {
        viewModel.cards().observe(requireActivity(), Observer { lista ->
            lista.forEach { card ->
                when (card.categoria) {
                    TarefaCard.Categoria.TOTAL -> {
                        card_1.numero.text = "${card.qtde}"
                        card_1.texto.text = card.nome
                        card_1.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorAccent)
                        card_1.botao.setOnClickListener(this.getCardOnClickListener(TarefaCard.Categoria.TOTAL))
                    }

                    TarefaCard.Categoria.VALIDACAO -> {
                        card_2.numero.text = "${card.qtde}"
                        card_2.texto.text = card.nome
                        card_2.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorAccent)
                        card_2.botao.setOnClickListener(this.getCardOnClickListener(TarefaCard.Categoria.VALIDACAO))
                    }

                    TarefaCard.Categoria.PENDENTE -> {
                        card_3.numero.text = "${card.qtde}"
                        card_3.texto.text = card.nome
                        card_3.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
                        card_3.botao.setOnClickListener(this.getCardOnClickListener(TarefaCard.Categoria.PENDENTE))
                    }

                    TarefaCard.Categoria.APROVADO -> {
                        card_4.numero.text = "${card.qtde}"
                        card_4.texto.text = card.nome
                        card_4.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.cards)
                        card_4.botao.setOnClickListener(this.getCardOnClickListener(TarefaCard.Categoria.APROVADO))
                    }

                    TarefaCard.Categoria.REPROVADO -> {
                        card_5.numero.text = "${card.qtde}"
                        card_5.texto.text = card.nome
                        card_5.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.cards)
                        card_5.botao.setOnClickListener(this.getCardOnClickListener(TarefaCard.Categoria.REPROVADO))
                    }

                    TarefaCard.Categoria.EXPIRADO -> {
                        card_6.numero.text = "${card.qtde}"
                        card_6.texto.text = card.nome
                        card_6.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorAccent)
                        card_6.botao.setOnClickListener(this.getCardOnClickListener(TarefaCard.Categoria.EXPIRADO))
                    }

                    TarefaCard.Categoria.REFAZER -> {
                        card_7.numero.text = "${card.qtde}"
                        card_7.texto.text = card.nome
                        card_7.backgroundTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorPrimary)
                        card_7.botao.setOnClickListener(this.getCardOnClickListener(TarefaCard.Categoria.REFAZER))
                    }

                    else -> { }
                }
            }
        })
    }

    private fun getCardOnClickListener(categoria: TarefaCard.Categoria) : View.OnClickListener {
        val intent = Intent(requireContext(), ListarTarefaActivity::class.java)
        intent.putExtra(ListarTarefaActivity.PARAM_INTENT_DATA_INICIO, viewModel.dataInicio)
        intent.putExtra(ListarTarefaActivity.PARAM_INTENT_DATA_FIM, viewModel.dataFim)
        intent.putExtra(ListarTarefaActivity.PARAM_INTENT_STATUS, categoria)
        return View.OnClickListener {
            startActivityForResult(intent, EDITAR)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == EDITAR && resultCode == Activity.RESULT_OK) {
            viewModel.buscarDashboardDados()
        }
    }

}
