package br.pucpr.appdev.familyday.ui.usuarios.model

import android.os.Parcelable
import androidx.databinding.BaseObservable
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.dto.UsuarioDTO
import com.fasterxml.jackson.annotation.*
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import org.apache.commons.lang3.time.DateFormatUtils
import org.apache.commons.lang3.time.DateUtils
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

@Parcelize
data class Usuario (
    @set:JsonProperty("_id")
    @get:JsonIgnore
    var id: String = "",
    var nome: String = "",

    @field:JsonIgnore
    var dataNascimento: Date? = null,
    var telefone: String = "99999999999",
    var tipo: Tipo? = null,
    var email: String = "",
    var responsavel: Usuario? = null,

    @field:JsonProperty("fotoPerfil")
    var urlFoto: String = "",
    var senha: String = "",
    var genero: Genero? = null,

    @get:JsonIgnore
    var dataCriacao: Date? = null,

    @get:JsonIgnore
    var dataAtualizacao: Date? = null,

    var ativo: Boolean = true,
    var deviceId: String = "-",
    val os: String = "ANDROID",

    @get:JsonIgnore
    @set:JsonProperty("familia")
    var familiaId: String = "",

    var confirmarSenha: String = "",

    @field:JsonProperty("nickName")
    var apelido: String = ""

    ) : Parcelable, BaseObservable() {

    companion object {
        private const val DATA_NASCIMENTO = "dataNascimento"
    }

    enum class Tipo(val id: Int) {
        @JsonProperty RESPONSAVEL(R.id.radio_responsavel),
        @JsonProperty DEPENDENTE(R.id.radio_dependente);

        companion object {
            fun buscarPorId(id: Int): Tipo? {
                enumValues<Tipo>().forEach {
                    if (it.id == id)
                        return it
                }
                return null
            }
        }
    }

    enum class Genero(val id: Int, val nome: String, val resId: Int) {
        @JsonProperty("M") MASCULINO(R.id.radio_masculino, "M", R.string.masculino),
        @JsonProperty("F") FEMININO(R.id.radio_feminino, "F", R.string.feminino),
        @JsonProperty("OUTRO") OUTRO(R.id.radio_outro, "OUTRO", R.string.outro);

        companion object {
            fun buscarPorId(id: Int): Genero? {
                enumValues<Genero>().forEach {
                    if (it.id == id)
                        return it
                }
                return null
            }
        }
    }

    fun primeiroNome(): String {
        return nome.split(" ")[0]
    }

    fun ultimoNome(): String {
        return nome.split(" ").last()
    }

    @JsonSetter(DATA_NASCIMENTO)
    fun setDataNascimentoAPI(date: String) {
        dataNascimento = DateUtils.parseDate(date, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    }

    @JsonGetter(DATA_NASCIMENTO)
    fun getDataNascimentoAPI(): String {
        return DateFormatUtils.format(this.dataNascimento!!, "yyyy-MM-dd")
    }

}

