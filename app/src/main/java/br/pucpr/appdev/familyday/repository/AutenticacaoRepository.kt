package br.pucpr.appdev.familyday.repository

import android.util.Base64
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.pucpr.appdev.familyday.enums.ApiState
import br.pucpr.appdev.familyday.model.ServiceStatus
import br.pucpr.appdev.familyday.rest.AutenticacaoAPI
import br.pucpr.appdev.familyday.rest.RetrofitFactory
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonSyntaxException
import kotlinx.coroutines.tasks.await
import java.net.UnknownHostException

class AutenticacaoRepository private constructor() {

    companion object {
        private const val TAG = "AUTENTICACAO-REPOSITORY"
        var instance: AutenticacaoRepository? = null
        get() {
            if (field == null)
                field = AutenticacaoRepository()
            return field
        }
        private set
    }

    private val service = RetrofitFactory.getInstance().get().create(AutenticacaoAPI::class.java)
    private val statusLiveData = MutableLiveData<ServiceStatus>()
    private val status = ServiceStatus()

    init {
//        statusLiveData.observeForever {
//            if (it.state != ApiState.RUNNING && it.state != ApiState.STOPPED) {
//                status.state = ApiState.STOPPED
//                status.codigo = ""
//                status.mensagem = ""
//
//                statusLiveData.postValue(status)
//            }
//        }
    }

    fun status() = statusLiveData as LiveData<ServiceStatus>

    private fun updateStatus(json: JsonObject, state: ApiState) {
        status.codigo = json.getAsJsonPrimitive("codigo").asString
        status.mensagem = json.getAsJsonPrimitive("mensagem").asString
        status.state = state
    }

    suspend fun login(email: String, senha: String): String? {
        val loginBase64 = Base64.encodeToString("$email:$senha".toByteArray(), Base64.NO_WRAP)
        val deviceId = FirebaseInstanceId.getInstance().instanceId.await().token
        val requestBody = HashMap<String, String>()

        requestBody["deviceId"] = deviceId
        requestBody["os"] = "ANDROID"

        status.state = ApiState.RUNNING
        try {
            val response = service.login("Basic $loginBase64", requestBody)
            Log.d(TAG, "Response Code: ${response.code()}")

            when(response.code()) {
                200 -> {
                    status.state = ApiState.OK
                    statusLiveData.postValue(status)
                    return response.body()?.getAsJsonPrimitive("token")?.asString
                }

                400 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "Chamada incorreta para a API")
                    Log.d(TAG, response.errorBody()?.toString())
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                401 -> {
                    val responseError = response.errorBody()?.string()
                    val jsonErro = Gson().fromJson(responseError, JsonObject::class.java)
                    Log.d(TAG, "Login incorreto")
                    Log.d(TAG, jsonErro.toString())
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                else -> {
                    val jsonErro = try { Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java) } catch (e: JsonSyntaxException) { null }
                    Log.d(TAG, "Erro na API")
                    Log.d(TAG, "Error message returned: ${response.errorBody()?.string()}")

                    if (jsonErro != null)
                        updateStatus(jsonErro, ApiState.UNKNOWN_ERROR)
                }
            }
        } catch (hostException: UnknownHostException) {
            Log.e(TAG, "Não foi possível conectar no host. Mensagem de erro: ${hostException.localizedMessage}", hostException)
            status.state = ApiState.NETWORK_ERROR
        } catch (e: Exception) {
            Log.e(TAG, "Algo não saiu como esperado. Mensagem de erro: ${e.localizedMessage}", e)
            status.state = ApiState.EXCEPTION
        }

        statusLiveData.postValue(status)
        return null
    }

    suspend fun recuperarSenha(email: String): Boolean {
        val requestBody = HashMap<String, String>()

        requestBody["emailOuNick"] = email
        status.state = ApiState.RUNNING
        try {
            val response = service.recuperarSenha(requestBody)

            when(response.code()) {
                200 -> {
                    status.state = ApiState.OK
                    statusLiveData.postValue(status)
                    return true
                }

                400 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "400: Chamada incorreta para a API")
                    Log.d(TAG, response.errorBody()?.toString())
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                401 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "401: Email incorreto")
                    Log.d(TAG, "$jsonErro")
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                else -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "${response.code()}: Erro na API")
                    updateStatus(jsonErro, ApiState.UNKNOWN_ERROR)
                }
            }
        } catch (hostException: UnknownHostException) {
            Log.e(TAG, "Não foi possível conectar no host. Mensagem de erro: ${hostException.localizedMessage}", hostException)
            status.state = ApiState.NETWORK_ERROR
        } catch (e: Exception) {
            Log.e(TAG, "Algo não saiu como esperado. Mensagem de erro: ${e.localizedMessage}", e)
            status.state = ApiState.EXCEPTION
        }

        statusLiveData.postValue(status)
        return false
    }

    suspend fun validarCodigo(email: String, codigo: String): Boolean {
        val requestBody = HashMap<String, String>()

        requestBody["emailOuNick"] = email
        requestBody["codigo"] = codigo

        status.state = ApiState.RUNNING
        try {
            val response = service.validarCodigo(requestBody)

            when(response.code()) {
                200 -> {
                    status.state = ApiState.OK
                    statusLiveData.postValue(status)
                    return true
                }

                400 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "400: Chamada incorreta para a API")
                    Log.d(TAG, response.errorBody()?.toString())
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                401 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "401: Código incorreto")
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                else -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "${response.code()}: Erro na API")
                    updateStatus(jsonErro, ApiState.UNKNOWN_ERROR)
                }
            }
        } catch (hostException: UnknownHostException) {
            Log.e(TAG, "Não foi possível conectar no host. Mensagem de erro: ${hostException.localizedMessage}", hostException)
            status.state = ApiState.NETWORK_ERROR
        } catch (e: Exception) {
            Log.e(TAG, "Algo não saiu como esperado. Mensagem de erro: ${e.localizedMessage}", e)
            status.state = ApiState.EXCEPTION
        }

        statusLiveData.postValue(status)
        return false
    }

    suspend fun alterarSenha(email: String, codigo: String, senha: String): Boolean {
        val requestBody = HashMap<String, String>()

        requestBody["emailOuNick"] = email
        requestBody["codigo"] = codigo
        requestBody["senha"] = senha

        status.state = ApiState.RUNNING
        try {
            val response = service.alterarSenha(requestBody)

            when(response.code()) {
                200 -> {
                    status.state = ApiState.OK
                    statusLiveData.postValue(status)
                    return true
                }

                400 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "400: Chamada incorreta para a API")
                    Log.d(TAG, response.errorBody()?.toString())
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                401 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "401: Login incorreto")
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                else -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "${response.code()}: Erro na API")
                    updateStatus(jsonErro, ApiState.UNKNOWN_ERROR)
                }
            }
        } catch (hostException: UnknownHostException) {
            Log.e(TAG, "Não foi possível conectar no host. Mensagem de erro: ${hostException.localizedMessage}", hostException)
            status.state = ApiState.NETWORK_ERROR
        } catch (e: Exception) {
            Log.e(TAG, "Algo não saiu como esperado. Mensagem de erro: ${e.localizedMessage}", e)
            status.state = ApiState.EXCEPTION
        }

        statusLiveData.postValue(status)
        return false
    }

}