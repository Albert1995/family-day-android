package br.pucpr.appdev.familyday.ui.tarefas.viewmodel

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.repository.FirebaseStorageRepository
import br.pucpr.appdev.familyday.repository.TarefaRepository
import br.pucpr.appdev.familyday.ui.tarefas.model.HistoricoTarefa
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import kotlinx.coroutines.launch

class DetalheHistoricoViewModel: ViewModel() {

    private val tarefaRepository = TarefaRepository.getInstance()
    private val storageRepository = FirebaseStorageRepository.instance!!
    private val updating = MutableLiveData<Boolean>()
    private val saved = MutableLiveData<Boolean>()

    var historico: HistoricoTarefa = HistoricoTarefa()
    var tarefa: Tarefa = Tarefa()

    var foto1: Bitmap? = null
    var foto2: Bitmap? = null
    var foto3: Bitmap? = null

    var urlFoto1: Uri? = null
    var urlFoto2: Uri? = null
    var urlFoto3: Uri? = null

    fun atualizarStatus(novoStatus: HistoricoTarefa.Status) {
        updating.postValue(true)
        viewModelScope.launch {
            if (novoStatus == HistoricoTarefa.Status.VALIDACAO && tarefa.necessarioComprovacao) {
                historico.fotos = arrayOf()

                if (foto1 != null) {
                    historico.fotos += storageRepository.salvarFoto("tarefas/${tarefa.id}", "1", foto1!!)
                } else {
                    historico.fotos += ""
                }

                if (foto2 != null) {
                    historico.fotos += storageRepository.salvarFoto("tarefas/${tarefa.id}", "2", foto2!!)
                } else {
                    historico.fotos += ""
                }

                if (foto3 != null) {
                    historico.fotos += storageRepository.salvarFoto("tarefas/${tarefa.id}", "3", foto1!!)
                } else {
                    historico.fotos += ""
                }
            }

            historico.status = novoStatus.status

            if (FamilyDayApp.getInstance().usuarioLogado!!.tipo == Usuario.Tipo.RESPONSAVEL) {
                saved.postValue(tarefaRepository.atualizarStatusResponsavel(tarefa.id, historico, novoStatus.status))
            } else {
                saved.postValue(tarefaRepository.atualizarStatusMembro(tarefa.id, historico, novoStatus.status))
            }

            updating.postValue(false)
        }
    }

    fun carregarFotos(): LiveData<Boolean> {
        val live = MutableLiveData<Boolean>()

        viewModelScope.launch {
            if (historico.fotos.isNotEmpty()) {
                if (historico.fotos[0].isNotBlank())
                    urlFoto1 = storageRepository.recuperarFoto(historico.fotos[0])

                if (historico.fotos.size >= 2 && historico.fotos[1].isNotBlank())
                    urlFoto2 = storageRepository.recuperarFoto(historico.fotos[1])

                if (historico.fotos.size >= 3 && historico.fotos[2].isNotBlank())
                    urlFoto3 = storageRepository.recuperarFoto(historico.fotos[2])
            }

            live.postValue(true)
        }

        return live
    }

    fun isUpdating() = updating as LiveData<Boolean>

    fun isSaved() = saved as LiveData<Boolean>

}