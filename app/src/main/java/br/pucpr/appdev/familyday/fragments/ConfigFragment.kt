package br.pucpr.appdev.familyday.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.adapter.ConfigAdapter
import br.pucpr.appdev.familyday.interfaces.ICallbackConfiguracoes
import kotlinx.android.synthetic.main.fragment_config.*

class ConfigFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_config, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        config_list.adapter = ConfigAdapter(activity as ICallbackConfiguracoes)
    }

}