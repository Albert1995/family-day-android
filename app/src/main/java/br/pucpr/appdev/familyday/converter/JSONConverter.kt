package br.pucpr.appdev.familyday.converter

import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.lang.reflect.Type

class JSONConverter : Converter.Factory() {

    override fun responseBodyConverter(
        type: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? {
        for (a in annotations) {
            if (a.annotationClass == Json::class) {
                return GsonConverterFactory.create().responseBodyConverter(type, annotations, retrofit)
            }
        }
        return JacksonConverterFactory.create().responseBodyConverter(type, annotations, retrofit)
    }

}

annotation class Json {

}