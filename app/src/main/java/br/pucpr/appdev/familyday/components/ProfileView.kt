package br.pucpr.appdev.familyday.components

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View


class ProfileView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {



    override fun onDraw(canvas: Canvas) {
        val bg = Paint()

        /*red.color = Color.RED
        red.style = Paint.Style.FILL
        red.isAntiAlias = true*/

        bg.shader = LinearGradient(0f, 0f, 0f, height.toFloat(), Color.parseColor("#F58B00"), Color.parseColor("#FF4600"), Shader.TileMode.MIRROR)

        setBackgroundColor(Color.TRANSPARENT)

        var path: Path = Path()

        path.moveTo(0f,0f)
        path.lineTo(width.toFloat(), 0f)
        path.lineTo(width.toFloat(), height.toFloat())
        path.lineTo(width.toFloat() / 2, height.toFloat() - 80)
        path.lineTo(0f, height.toFloat())
        path.lineTo(0f, 0f)
        path.close()

        canvas.drawPath(path, bg)

    }

}