package br.pucpr.appdev.familyday.dto

data class FamiliaDTO (
    var id: String = "",
    var nome: String = "",
    var membros: List<String>
) {

}