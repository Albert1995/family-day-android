package br.pucpr.appdev.familyday.ui.tarefas.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.repository.FamiliaRepository
import br.pucpr.appdev.familyday.repository.MetaRepository
import br.pucpr.appdev.familyday.repository.TarefaRepository
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa
import br.pucpr.appdev.familyday.ui.tarefas.model.TarefaErro
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.utils.DateUtils
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class CadastroTarefaViewModel : ViewModel() {

    companion object {
        private const val TAG = "CADASTRO-TAREFA-VM"
    }

    var tarefa = Tarefa()

    var erro : TarefaErro = TarefaErro()

    var diario : Boolean = false

    var recorrencia : Boolean = false

    var dataInicio: String = ""
    var dataFim: String = ""

    private val mRepo : TarefaRepository = TarefaRepository.getInstance()

    private val mFamiliaRepo : FamiliaRepository = FamiliaRepository.getInstance()

    private val mMetaRepo : MetaRepository = MetaRepository.getInstance()

    private val mIsUpdating = MutableLiveData(false)

    private val mIsSaved = MutableLiveData(false)

    private val mUsuarios = MutableLiveData<List<Usuario>>()

    private val mMetas = MutableLiveData<List<Meta>>()

    fun isUpdating() = mIsUpdating as LiveData<Boolean>

    fun isSaved() = mIsSaved as LiveData<Boolean>

    fun getUsuarios() = mUsuarios as LiveData<List<Usuario>>

    fun getMetas() = mMetas as LiveData<List<Meta>>

    private fun validar() {
        val app = FamilyDayApp.getInstance()

        erro.titulo = if (tarefa.titulo == "")
            app.getString(R.string.tarefa_titulo_branco)
        else ""

        erro.pontos = if (tarefa.pontos <= 0)
            app.getString(R.string.tarefa_pontos_zero)
        else ""

        erro.meta = if (tarefa.metaTarefa == null)
            app.getString(R.string.tarefa_meta_nao_marcado)
        else ""

        val data = org.apache.commons.lang3.time.DateUtils.addDays(Date(), -1)
        erro.dataInicio = if (tarefa.dataInicio == null)
            app.getString(R.string.tarefa_data_inicio_branco)
        else if (tarefa.dataInicio?.before(data)!! && tarefa.id.isBlank())
            app.getString(R.string.tarefa_data_inicio_passado)
        else ""

        erro.dataFim = if (tarefa.dataFim == null)
            app.getString(R.string.tarefa_data_fim_branco)
        else if (tarefa.dataFim?.before(tarefa.dataInicio)!!)
            app.getString(R.string.tarefa_data_fim_invertido)
        else ""

        erro.membros = if (tarefa.membrosLista.isEmpty())
            app.getString(R.string.tarefa_membro_nao_marcado)
        else ""

        Log.e(TAG, erro.toString())
    }

    fun salvar() {
        tarefa.dataInicio = DateUtils.safeStringToDate("dd/MM/yyyy", dataInicio)
        tarefa.dataFim = if (recorrencia) DateUtils.safeStringToDate("dd/MM/yyyy", dataFim) else tarefa.dataInicio

        validar()
        erro.notifyChange()

        if (erro.hasErros()) {
            mIsUpdating.postValue(false)
            return
        }

        mIsUpdating.postValue(true)
        GlobalScope.launch {
            if (tarefa.id != "") {
                mRepo.atualizar(tarefa.id, tarefa)
            } else {
                mRepo.criar(tarefa)
            }

            mIsSaved.postValue(true)
            mIsUpdating.postValue(false)
        }
    }

    fun listarUsuarios() {
        mIsUpdating.postValue(true)

        GlobalScope.launch {
            var lista = mFamiliaRepo.listarMembros()
            mUsuarios.postValue(lista)
            mIsUpdating.postValue(false)
        }
    }

    fun listarMetas() {
        mIsUpdating.postValue(true)

        GlobalScope.launch {
            var lista = mMetaRepo.buscarTodos()

            if (tarefa.metaTarefa != null) {
                lista.forEach {
                    if (it.id == tarefa.metaTarefa?.id) {
                        tarefa.metaTarefa = it
                    }
                }
            }

            mMetas.postValue(lista)
            mIsUpdating.postValue(false)
        }
    }

    fun setSemana(i: Int, check: Boolean) {
        if (check) {
            tarefa.semana.add(i)
        } else {
            tarefa.semana.remove(i)
        }
    }

    fun tarefaHasMembro(membro: Usuario) : Boolean {
        var achou = false
        if (tarefa.membrosLista.size > 0) {
            tarefa.membrosLista.forEach {
                if (it.id == membro.id) {
                    achou = true
                    return@forEach
                }
            }
        }
        return achou
    }

    fun setMeta(position: Int) {
        tarefa.metaTarefa = mMetas.value?.get(position)
    }

}