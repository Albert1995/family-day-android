package br.pucpr.appdev.familyday.enums

enum class ViewModelStatus {

    SINCRONIZADO, NAO_SINCRONIZADO, ERRO, SINCRONIZANDO, LOGADO

}