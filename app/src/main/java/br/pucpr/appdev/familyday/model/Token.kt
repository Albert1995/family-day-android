package br.pucpr.appdev.familyday.model

data class Token(
    val token: String = ""
) {

    override fun toString(): String {
        return token
    }

}