package br.pucpr.appdev.familyday.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.model.Familia
import br.pucpr.appdev.familyday.model.FamiliaErro
import br.pucpr.appdev.familyday.repository.FamiliaRepository
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import kotlinx.coroutines.launch

class FamiliaViewModel : ViewModel() {

    var familia = Familia(dono = Usuario())
    var erro = FamiliaErro()

    private val updating = MutableLiveData<Boolean>()
    private val saved = MutableLiveData<Boolean>()

    private val familiaRepository = FamiliaRepository.getInstance()

    private fun validar() {
        erro.nome = if (familia.nome.isBlank()) FamilyDayApp.getInstance().getString(R.string.familia_campo_nome_branco) else ""
    }

    fun salvar() {
        validar()

        erro.notifyChange()
        if (erro.hasErros()) {
            return
        }

        updating.postValue(true)

        viewModelScope.launch {
            saved.postValue(familiaRepository.criar(familia))
            updating.postValue(false)
        }
    }

    fun isUpdating() = updating as LiveData<Boolean>

    fun isSaved() = saved as LiveData<Boolean>


}