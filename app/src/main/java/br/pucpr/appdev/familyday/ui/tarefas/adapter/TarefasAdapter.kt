package br.pucpr.appdev.familyday.ui.tarefas.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.ui.tarefas.model.HistoricoTarefa
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa
import org.apache.commons.lang3.time.DateFormatUtils
import org.apache.commons.lang3.time.DateUtils
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs

class TarefasAdapter(private var listaTarefas: List<Tarefa>) : RecyclerView.Adapter<TarefasAdapter.ViewHolder>() {

    private var listaHistoricos: MutableList<HistoricoTarefa> = mutableListOf()

    init {
        setLista(listaTarefas)
    }

    fun setLista(novaLista: List<Tarefa>) {
        listaTarefas = novaLista
        listaHistoricos.clear()
        novaLista.forEach {
            listaHistoricos.addAll(it.historico)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val ctx: Context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(ctx)
        val item: View = inflater.inflate(R.layout.item_list_tarefa, parent, false)
        return ViewHolder(item)
    }

    override fun getItemCount(): Int {
        return listaHistoricos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val historico = listaHistoricos[position]
        val app = FamilyDayApp.getInstance()
        val cat = HistoricoTarefa.Status.values().find { c -> c.status == historico.status }

        holder.historico = historico
        holder.tarefa = listaTarefas.first { t -> t.historico.contains(historico) }
        holder.nome.text = holder.tarefa?.titulo
        holder.status.text = app.getString(cat!!.labelId)
        holder.status.setTextColor(ContextCompat.getColor(app, cat.color))

        val diffInMillies: Long = abs(historico.dataExecucao!!.time - DateUtils.truncate(Date(), Calendar.DAY_OF_MONTH).time)
        val diff: Long = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.DAYS)

        when {
            DateUtils.isSameDay(historico.dataExecucao, Date()) -> {
                when (cat) {
                    HistoricoTarefa.Status.APROVADO -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_aprovado_hoje, historico.usuario?.primeiroNome())
                    }

                    HistoricoTarefa.Status.PENDENTE -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_pendente_hoje, historico.usuario?.primeiroNome())
                    }

                    HistoricoTarefa.Status.REPROVADO -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_reprovado_hoje, historico.usuario?.primeiroNome())
                    }

                    HistoricoTarefa.Status.VALIDACAO -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_validacao_hoje, historico.usuario?.primeiroNome())
                    }

                    else -> { }
                }
            }
            diff == -1L -> {
                when (cat) {
                    HistoricoTarefa.Status.APROVADO -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_aprovado_ontem, historico.usuario?.primeiroNome())
                    }

                    HistoricoTarefa.Status.REPROVADO -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_reprovado_ontem, historico.usuario?.primeiroNome())
                    }

                    HistoricoTarefa.Status.VALIDACAO -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_validacao_ontem, historico.usuario?.primeiroNome())
                    }

                    HistoricoTarefa.Status.EXPIRADO -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_expirado_ontem, historico.usuario?.primeiroNome())
                    }

                    else -> { }
                }
            }
            diff == 1L -> {
                when (cat) {
                    HistoricoTarefa.Status.PENDENTE -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_pendente_amanha, historico.usuario?.primeiroNome())
                    }

                    else -> { }
                }
            }
            else -> {
                when (cat) {
                    HistoricoTarefa.Status.APROVADO -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_aprovado, DateFormatUtils.format(historico.dataExecucao, "dd/MM"), historico.usuario?.primeiroNome())
                    }

                    HistoricoTarefa.Status.REPROVADO -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_reprovado, DateFormatUtils.format(historico.dataExecucao, "dd/MM"), historico.usuario?.primeiroNome())
                    }

                    HistoricoTarefa.Status.VALIDACAO -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_validacao, DateFormatUtils.format(historico.dataExecucao, "dd/MM"), historico.usuario?.primeiroNome())
                    }

                    HistoricoTarefa.Status.EXPIRADO -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_expirado, DateFormatUtils.format(historico.dataExecucao, "dd/MM"), historico.usuario?.primeiroNome())
                    }

                    HistoricoTarefa.Status.PENDENTE -> {
                        holder.descricao.text = app.getString(R.string.item_tarefa_descricao_pendente, DateFormatUtils.format(historico.dataExecucao, "dd/MM"), historico.usuario?.primeiroNome())
                    }

                    else -> { }
                }
            }
        }
    }

    fun getByPosition(position: Int) : Tarefa {
        return listaTarefas[position]
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nome : TextView = itemView.findViewById(R.id.nome_tarefa)
        val status : TextView = itemView.findViewById(R.id.status)
        val descricao : TextView = itemView.findViewById(R.id.descricao)
        var historico: HistoricoTarefa? = null
        var tarefa: Tarefa? = null
    }
}