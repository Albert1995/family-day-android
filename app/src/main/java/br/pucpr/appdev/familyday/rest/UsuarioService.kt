package br.pucpr.appdev.familyday.rest

import br.pucpr.appdev.familyday.converter.Json
import br.pucpr.appdev.familyday.dto.UsuarioDTO
import br.pucpr.appdev.familyday.enums.Constants
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface UsuarioService {

    @Json
    @POST("usuarios")
    fun cadastrar(@Body usuario : UsuarioDTO): Call<JsonObject>

    @GET("usuarios/me")
    fun info(@Header(Constants.HEADER_TOKEN) token: String): Call<Usuario>

    @Json
    @PUT("")
    fun atualizar(@Header(Constants.HEADER_TOKEN) token: String, @Body usuario: UsuarioDTO): Call<JsonObject>

    @GET("usuarios/me")
    suspend fun info() : Response<Usuario>

    @Json
    @POST("usuarios")
    suspend fun criar(@Body usuario: Usuario) : Response<JsonObject>

    @Json
    @POST("usuarios/cadastrar-membro-familia")
    suspend fun criarMembro(@Body usuario: Usuario) : Response<JsonObject>

    @Json
    @PUT("usuarios/{id}")
    suspend fun atualizar(@Path("id") id: String, @Body usuario: Usuario) : Response<JsonObject>

    @Json
    @GET("usuarios/gerar-qrcode/{id}")
    suspend fun buscarQRCode(@Path("id") id : String) : Response<JsonObject>

}