package br.pucpr.appdev.familyday.components

import android.content.Context
import android.graphics.Color
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.text.set
import androidx.databinding.*
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.interfaces.Validador
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.component_campo_editar.view.*
import java.text.SimpleDateFormat
import java.util.*

class CampoEditar(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private var layout : TextInputLayout

    private var valido : Boolean = false
    private lateinit var validador : Validador

    init {
        inflate(context, R.layout.component_campo_editar, this)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.CampoEditar)

        layout = findViewById(R.id.text_layout)
        layout.hint = attributes.getString(R.styleable.CampoEditar_hint)

        if (attributes.getBoolean(R.styleable.CampoEditar_editable, true)) {
            text_edit.inputType = when (attributes.getInt(R.styleable.CampoEditar_inputType, 0)) {
                1 -> InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                2 -> InputType.TYPE_CLASS_NUMBER
                else -> InputType.TYPE_CLASS_TEXT
            }
        } else {
            text_edit.inputType = InputType.TYPE_NULL
            text_edit.isEnabled = false
        }

        if (attributes.getInt(R.styleable.CampoEditar_limit, -1) > 0) {
            text_edit.filters += InputFilter.LengthFilter(attributes.getInt(R.styleable.CampoEditar_limit, 0))
        }

        attributes.recycle()
    }

    fun adicionarErro(mensagem: String) {
        layout.error = mensagem
        text_edit.setBackgroundColor(Color.TRANSPARENT)
        text_edit.setPadding((15 * resources.displayMetrics.density + 0.5f).toInt(),0,0,0)
    }

    fun adicionarErro(value: Int) {
        adicionarErro(FamilyDayApp.getInstance().getString(value))
    }

    fun removerErro() {
        layout.error = ""
    }

    fun addValidador(validacao: Validador) {
        validador = validacao
    }

    fun addListener(listener: TextWatcher) {
        text_edit.addTextChangedListener(listener)
    }

    fun setText(value: String) {
        text_edit.setText(value)
    }

    companion object {

        private fun defaultTextWatcher(attrChange: InverseBindingListener) = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                attrChange.onChange()
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {

            }

            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {

            }

        }

        @BindingAdapter("textBind")
        @JvmStatic fun setTextBind(view: CampoEditar, value: String?) {
            value?.let { str: String ->
                view.text_edit.setText(str)
            }
        }

        @InverseBindingAdapter(attribute = "textBind")
        @JvmStatic fun getTextBind(view: CampoEditar) : String {
            return view.text_edit.text.toString()
        }

        @BindingAdapter("app:textBindAttrChanged")
        @JvmStatic fun setListener(view: CampoEditar, attrChange: InverseBindingListener) {
            attrChange.let {
                view.text_edit.addTextChangedListener(defaultTextWatcher(attrChange))
            }
        }


        @BindingAdapter("numberInput")
        @JvmStatic fun setNumberInput(view: CampoEditar, value: Int) {
            if (value != 0)
                value.toString().let { str: String ->
                    view.text_edit.setText(str)
                }
        }

        @InverseBindingAdapter(attribute = "numberInput")
        @JvmStatic fun getNumberInput(view: CampoEditar) : Int {
            if (view.text_edit.text.toString() == "")
                return 0
            return Integer.valueOf(view.text_edit.text.toString())
        }

        @BindingAdapter("app:numberInputAttrChanged")
        @JvmStatic fun setNumberInputListener(view: CampoEditar, attrChange: InverseBindingListener) {
            attrChange.let {
                view.text_edit.addTextChangedListener(defaultTextWatcher(attrChange))
            }
        }

        @BindingAdapter("erro")
        @JvmStatic fun setErro(view: CampoEditar, value: String) {
            if (value != "") {
                view.adicionarErro(value)
            } else {
                view.removerErro()
            }
        }

        @BindingAdapter("validador")
        @JvmStatic fun setValidador(view : CampoEditar, value : Object) {

        }

        @BindingAdapter("dateBind")
        @JvmStatic fun setDateBind(view: CampoEditar, value: Date?) {
            value?.let { date: Date ->
                view.text_edit.setText(SimpleDateFormat("dd/MM/yyyy").format(date))
            }
        }

        @InverseBindingAdapter(attribute = "dateBind")
        @JvmStatic fun getDateBind(view: CampoEditar) : Date {
            return SimpleDateFormat("dd/MM/yyyy").parse(view.text_edit.text.toString())
        }

        @BindingAdapter("app:dateBindAttrChanged")
        @JvmStatic fun setDateInputListener(view: CampoEditar, attrChange: InverseBindingListener) {
            attrChange.let {
                view.text_edit.addTextChangedListener(defaultTextWatcher(attrChange))
            }
        }

    }


}

