package br.pucpr.appdev.familyday.rest

import br.pucpr.appdev.familyday.converter.Json
import br.pucpr.appdev.familyday.enums.Constants
import br.pucpr.appdev.familyday.model.Familia
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface FamiliaService {

    @Json
    @POST("familias")
    suspend fun criar(@Body familia: Familia) : Response<JsonObject>

    @Json
    @POST("familias/{id}/membros/{idMembro}")
    fun adicionarMembro(@Header(Constants.HEADER_TOKEN) token: String, @Path("id") id: String, @Path("idMembro") idMembro: String) : Call<JsonObject>

    @GET("familias/listar-membros")
    fun listarMembros(@Header(Constants.HEADER_TOKEN) token: String) : Call<List<Usuario>>

    @GET("familias/listar-membros")
    suspend fun listarMembros() : Response<List<Usuario>>

}
