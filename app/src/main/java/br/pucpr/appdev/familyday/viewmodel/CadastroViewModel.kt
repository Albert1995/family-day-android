package br.pucpr.appdev.familyday.viewmodel

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.repository.FirebaseStorageRepository
import br.pucpr.appdev.familyday.repository.UsuarioRepository
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.ui.usuarios.model.UsuarioErro

import kotlinx.coroutines.launch
import org.apache.commons.lang3.time.DateUtils
import java.util.*

/**
 * Realiza operações de cadastro do usuário na API
 *
 * @author Albert Werihermann
 * @since 1.0
 */
class CadastroViewModel: ViewModel() {

    companion object {
        private const val TAG = "CADASTRO-VIEWMODEL"
    }

    var usuario : Usuario = Usuario(tipo = Usuario.Tipo.RESPONSAVEL)
    val erro: UsuarioErro = UsuarioErro()
    var dataNascimentoTexto: String = ""
    var termosAceitos: Boolean = false
    var novoMembroFamilia: Boolean = false
    var urlFoto : Uri? = null
    var foto: Bitmap? = null

    private val updating = MutableLiveData<Boolean>()
    private val saved = MutableLiveData<Boolean>()
    private val usuarioRepository = UsuarioRepository.instance!!
    private val storageRepository = FirebaseStorageRepository.instance!!

    private val regexNome = Regex("[A-z]+( [A-z]+)+")
    private val regexEmail = Regex("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")

    /**
     * Valida todos os campos do usuário
     */
    private fun validar() {
        val app = FamilyDayApp.getInstance()
        erro.nome = if (usuario.nome.isBlank()) app.getString(R.string.usuario_nome_branco)
            else if (!usuario.nome.matches(regexNome)) app.getString(R.string.usuario_nome_incompleto)
            else ""

        erro.dataNascimento = if (dataNascimentoTexto.isBlank()) app.getString(R.string.usuario_data_nascimento_branco)
            else if (usuario.dataNascimento!!.after(Date())) app.getString(R.string.usuario_data_nascimento_futuro)
            else if (usuario.dataNascimento!!.after(DateUtils.addYears(Date(), -18)) && usuario.tipo == Usuario.Tipo.RESPONSAVEL) app.getString(R.string.usuario_data_nascimento_menor)
            else ""

        erro.email = if (usuario.tipo == Usuario.Tipo.DEPENDENTE && usuario.email.isBlank()) ""
            else if (usuario.email.isBlank()) app.getString(R.string.usuario_email_branco)
            else if (!usuario.email.matches(regexEmail)) app.getString(R.string.usuario_email_invalido)
            else ""

        erro.genero = if (usuario.genero == null) app.getString(R.string.usuario_genero_nao_marcado) else ""

        erro.tipo = if (usuario.tipo == null) app.getString(R.string.cadastro_usuario_tipo_nao_marcado) else ""

        if (!novoMembroFamilia) {
            erro.senha = when {
                usuario.senha.isBlank() -> app.getString(R.string.usuario_senha_branco)
                usuario.senha.length !in 5..13 -> app.getString(R.string.usuario_senha_regra)
                usuario.senha != usuario.confirmarSenha -> app.getString(R.string.usuario_confirmar_senha_regra)
                else -> ""
            }

            erro.confirmarSenha = if (usuario.senha != usuario.confirmarSenha) app.getString(R.string.usuario_confirmar_senha_regra) else ""

            erro.termos = if (!termosAceitos) app.getString(R.string.cadastro_termos_nao_aceito) else ""
        }
    }

    fun atualizarDataNascimento() {
        usuario.dataNascimento = DateUtils.parseDate(dataNascimentoTexto, "dd/MM/yyyy")
    }

    /**
     * Método para salvar o objeto usuário na API
     */
    fun salvar() {
        usuario.confirmarSenha = usuario.senha

        validar()
        erro.notifyChange()

        if (erro.hasErro()) {
            saved.postValue(false)
            return
        }

        updating.postValue(true)
        viewModelScope.launch {
            if (usuario.id.isBlank()) {
                saved.postValue(if (novoMembroFamilia) usuarioRepository.criarMembro(usuario) else usuarioRepository.criar(usuario))
            } else {
                saved.postValue(usuarioRepository.atualizar(usuario, urlFoto, foto))
            }

            updating.postValue(false)
        }
    }

    fun recuperarUsuario(): LiveData<Usuario> {
        val liveData = MutableLiveData<Usuario>()
        updating.postValue(true)

        viewModelScope.launch {
            val usuario = usuarioRepository.recuperarInfo()

            liveData.postValue(usuario)
            updating.postValue(false)
        }

        return liveData
    }

    fun carregarFoto(): LiveData<Uri> {
        val uri = MutableLiveData<Uri>()

        viewModelScope.launch {
            uri.postValue(storageRepository.recuperarFoto(usuario.urlFoto))
        }

        return uri
    }

    fun isUpdating() = updating as LiveData<Boolean>

    fun isSaved() = saved as LiveData<Boolean>

    fun apiState() = usuarioRepository.status()

}