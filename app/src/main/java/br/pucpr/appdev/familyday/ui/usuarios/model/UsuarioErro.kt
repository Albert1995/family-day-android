package br.pucpr.appdev.familyday.ui.usuarios.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable

class UsuarioErro : BaseObservable() {

    @Bindable var nome : String = ""
    @Bindable var dataNascimento : String = ""
    @Bindable var email: String = ""
    @Bindable var genero: String = ""
    @Bindable var senha: String = ""
    @Bindable var confirmarSenha: String = ""
    @Bindable var tipo: String = ""
    @Bindable var termos: String = ""
    @Bindable var apelido: String = ""

    fun hasErro() : Boolean {
        return (nome.isNotBlank())
            .or(dataNascimento.isNotBlank())
            .or(email.isNotBlank())
            .or(genero.isNotBlank())
            .or(senha.isNotBlank())
            .or(confirmarSenha.isNotBlank())
            .or(tipo.isNotBlank())
            .or(termos.isNotBlank())
            .or(apelido.isNotBlank())
    }

}