package br.pucpr.appdev.familyday.repository

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.pucpr.appdev.familyday.enums.ApiState
import br.pucpr.appdev.familyday.model.ServiceStatus
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.rest.RetrofitFactory
import br.pucpr.appdev.familyday.rest.UsuarioService
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.tasks.await
import java.net.UnknownHostException

class UsuarioRepository private constructor() {

    private val mUsuarioService: UsuarioService = RetrofitFactory.getInstance().get().create(UsuarioService::class.java)
    private val storage: FirebaseStorageRepository = FirebaseStorageRepository.instance!!

    private val mApiState = MutableLiveData(ApiState.STOPPED)

    private val status = ServiceStatus()
    private val statusLiveData = MutableLiveData<ServiceStatus>()

    companion object {
        private const val TAG = "USUARIO-REPOSITORY"
        private const val PASTA_FIREBASE = "usuarios"

        var instance : UsuarioRepository? = null
        get() {
            if (field == null)
                field = UsuarioRepository()
            return field
        }
        private set
    }

    fun status() = statusLiveData as LiveData<ServiceStatus>

    private fun updateStatus(json: JsonObject, state: ApiState) {
        status.codigo = json.getAsJsonPrimitive("codigo").asString
        status.mensagem = json.getAsJsonPrimitive("mensagem").asString
        status.state = state
    }

    suspend fun recuperarInfo() : Usuario? {
        try {
            val response = mUsuarioService.info()

            when (response.code()) {
                200 -> {
                    return response.body()
                }

                404 -> {

                }

                else -> {

                }
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }

        return null
    }

    suspend fun criar(obj: Usuario, urlFoto : Uri? = null, foto: Bitmap? = null): Boolean {
        mApiState.postValue(ApiState.RUNNING)
        try {
            val deviceId = FirebaseInstanceId.getInstance().instanceId.await().token
            obj.deviceId = deviceId
            val response = mUsuarioService.criar(obj)

            when (response.code()) {
                201 -> {
                    Log.d(TAG, response.body()?.toString())
                    status.state = ApiState.OK
                    statusLiveData.postValue(status)
                    obj.id = response.body()?.getAsJsonPrimitive("id")?.asString ?: ""

                    if (urlFoto != null) {
                        obj.urlFoto = storage.salvarFoto(PASTA_FIREBASE, obj.id, urlFoto)
                        atualizar(obj)
                    } else if (foto != null) {
                        obj.urlFoto = storage.salvarFoto(PASTA_FIREBASE, obj.id, foto)
                        atualizar(obj)
                    }

                    return true
                }

                400 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "400: Chamada incorreta para a API")
                    Log.d(TAG, response.errorBody()?.toString())
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                else -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "${response.code()}: Erro na API")
                    updateStatus(jsonErro, ApiState.UNKNOWN_ERROR)
                }
            }
        } catch (hostException: UnknownHostException) {
            Log.e(TAG, "Não foi possível conectar no host. Mensagem de erro: ${hostException.localizedMessage}", hostException)
            status.state = ApiState.NETWORK_ERROR
        } catch (e: Exception) {
            Log.e(TAG, "Algo não saiu como esperado. Mensagem de erro: ${e.localizedMessage}", e)
            status.state = ApiState.EXCEPTION
        }

        statusLiveData.postValue(status)
        return false
    }

    suspend fun criarMembro(obj: Usuario, urlFoto : Uri? = null, foto: Bitmap? = null): Boolean {
        mApiState.postValue(ApiState.RUNNING)
        try {
            val response = mUsuarioService.criarMembro(obj)

            when (response.code()) {
                201 -> {
                    Log.d(TAG, response.body()?.toString())
                    status.state = ApiState.OK
                    statusLiveData.postValue(status)

                    return true
                }

                400 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "400: Chamada incorreta para a API")
                    Log.d(TAG, jsonErro.toString())
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                else -> {
                    Log.d(TAG, "${response.code()}: Erro na API")
                    Log.d(TAG, response.errorBody()?.string())

                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)

                    updateStatus(jsonErro, ApiState.UNKNOWN_ERROR)
                }
            }
        } catch (hostException: UnknownHostException) {
            Log.e(TAG, "Não foi possível conectar no host. Mensagem de erro: ${hostException.localizedMessage}", hostException)
            status.state = ApiState.NETWORK_ERROR
        } catch (e: Exception) {
            Log.e(TAG, "Algo não saiu como esperado. Mensagem de erro: ${e.localizedMessage}", e)
            status.state = ApiState.EXCEPTION
        }

        statusLiveData.postValue(status)
        return false
    }

    suspend fun atualizar(obj: Usuario, urlFoto: Uri? = null, foto: Bitmap? = null): Boolean {
        mApiState.postValue(ApiState.RUNNING)
        try {
            if (urlFoto != null) {
                obj.urlFoto = storage.salvarFoto(PASTA_FIREBASE, obj.id, urlFoto)
            } else if (foto != null) {
                obj.urlFoto = storage.salvarFoto(PASTA_FIREBASE, obj.id, foto)
            }

            val response = mUsuarioService.atualizar(obj.id, obj)

            Log.d(TAG, "Response code: ${response.code()}")
            when (response.code()) {
                200 -> {
                    status.state = ApiState.OK
                    statusLiveData.postValue(status)
                    return true
                }

                400 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "Chamada incorreta para a API")
                    Log.d(TAG, jsonErro.toString())
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                else -> {
                    val error = response.errorBody()?.string()
                    Log.d(TAG, "Algo de ruim aconteceu na API. Erro: $error")
                    mApiState.postValue(ApiState.UNKNOWN_ERROR)
                }
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.EXCEPTION)
        }

        statusLiveData.postValue(status)
        return false
    }

    suspend fun buscarQRCode(id: String) : String? {
        try {
            val response = mUsuarioService.buscarQRCode(id)

            when(response.code()) {
                200 -> {
                    return response.body()!!.getAsJsonObject("retorno").getAsJsonPrimitive("QRCode").asString.replace("data:image/png;base64,","")
                }
            }
        } catch (e: Exception) {

        }

        return ""
    }

}