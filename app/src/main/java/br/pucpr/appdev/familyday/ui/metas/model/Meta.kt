package br.pucpr.appdev.familyday.ui.metas.model

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Meta(
    @set:JsonProperty("_id")
    @get:JsonIgnore
    var id: String = "",

    var titulo: String = "",

    var descricao : String = "",

    @get:JsonProperty("pontosAlvo")
    var pontos: Int = 0,

    @JsonIgnore
    var familiaId: String = "",

    @get:JsonIgnore
    var status : String = "",

    @set:JsonIgnore
    @get:JsonProperty("foto")
    var url: String = ""
) : Parcelable, BaseObservable()