package br.pucpr.appdev.familyday.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.enums.Constants.SharedPreferencesKeys
import br.pucpr.appdev.familyday.utils.ActivityUtils
import br.pucpr.appdev.familyday.viewmodel.StartViewModel
import com.google.firebase.iid.FirebaseInstanceId

class StartActivity : AppCompatActivity() {

    companion object {
        private const val TAG: String = "START"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        val viewModel = ViewModelProvider(this).get(StartViewModel::class.java)

        viewModel.usuario().observe(this, Observer {
            if (it == null) {
                startActivity(Intent(this, LoginActivity::class.java))
            } else {
                FamilyDayApp.getInstance().save(SharedPreferencesKeys.ID_USUARIO, it.id)
                FamilyDayApp.getInstance().save(SharedPreferencesKeys.ID_FAMILIA, it.familiaId)
                startActivity(Intent(this, MainActivity::class.java))
            }

            finish()
        })

    }

}
