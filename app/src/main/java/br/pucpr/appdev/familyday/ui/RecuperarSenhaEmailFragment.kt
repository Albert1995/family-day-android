package br.pucpr.appdev.familyday.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.databinding.FragmentRecuperarSenhaEmailBinding
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.viewmodel.RecuperarSenhaViewModel
import kotlinx.android.synthetic.main.fragment_recuperar_senha_email.*

class RecuperarSenhaEmailFragment : Fragment() {

    companion object {
        private const val TAG = "RECOVER-PASS-1"
    }

    private lateinit var viewModel : RecuperarSenhaViewModel
    private lateinit var loadingDialog: ProgressDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(requireActivity()).get(RecuperarSenhaViewModel::class.java)
        loadingDialog = ProgressDialog(TAG, requireActivity().supportFragmentManager)

        val binding : FragmentRecuperarSenhaEmailBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_recuperar_senha_email, container, false)
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadingDialog.liveDataListener(viewModel.isUpdating(), this)
        initializeViewModel()

        enviar_btn.setOnClickListener(this::enviarOnClick)
    }

    private fun enviarOnClick(v: View) {
        if (viewModel.email.isBlank()) {
            email.error = getString(R.string.recuperar_senha_error_email)
        } else {
            viewModel.enviarCodigo()
        }
    }

    private fun initializeViewModel() {
        viewModel.hasSent().observe(viewLifecycleOwner, Observer {
            if (it) {
                (requireActivity() as RecuperarSenhaActivity).proximoPasso()
                viewModel.reset()
            }
        })
    }

}
