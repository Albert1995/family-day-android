package br.pucpr.appdev.familyday.ui.usuarios.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.marginBottom
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.repository.FirebaseStorageRepository
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.w3c.dom.Text

class UsuarioAdapter(var lista : List<Usuario>, val usuario: Usuario? = null) : BaseAdapter() {

    private val storage = FirebaseStorageRepository.instance!!

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val usuario = lista[position]
        var view = convertView

        val inflater = LayoutInflater.from(parent?.context)
        view = inflater.inflate(R.layout.item_grid_usuario, null)

        when(usuario.genero) {
            Usuario.Genero.MASCULINO -> {
                view?.findViewById<LinearLayout>(R.id.layout)?.setBackgroundResource(R.drawable.item_grid_usuario_masculino)
                view?.findViewById<ImageView>(R.id.user_icon)?.setImageResource(if (usuario.tipo == Usuario.Tipo.DEPENDENTE) R.drawable.boy else R.drawable.father)
                view?.findViewById<TextView>(R.id.nome)?.text = usuario.primeiroNome()
            }

            Usuario.Genero.FEMININO -> {
                view?.findViewById<LinearLayout>(R.id.layout)?.setBackgroundResource(R.drawable.item_grid_usuario_feminino)
                view?.findViewById<ImageView>(R.id.user_icon)?.setImageResource(if (usuario.tipo == Usuario.Tipo.DEPENDENTE) R.drawable.girl else R.drawable.mother)
                view?.findViewById<TextView>(R.id.nome)?.text = usuario.primeiroNome()
            }

            Usuario.Genero.OUTRO -> {
                view?.findViewById<LinearLayout>(R.id.layout)?.setBackgroundResource(R.drawable.item_grid_usuario_masculino)
                view?.findViewById<ImageView>(R.id.user_icon)?.setImageResource(R.drawable.boy)
                view?.findViewById<TextView>(R.id.nome)?.text = usuario.primeiroNome()
            }
        }

        if (usuario.id == this.usuario?.id) {
            val nome = "${usuario.primeiroNome()} (${FamilyDayApp.getInstance().getString(R.string.voce)})"
            view?.findViewById<TextView>(R.id.nome)?.text = nome
        }

        if (usuario.urlFoto.isNotBlank()) {
            val live = MutableLiveData<Uri>()
            val img = view.findViewById<RoundedImageView>(R.id.user_icon)

            GlobalScope.launch {
                live.postValue(storage.recuperarFoto(usuario.urlFoto))
            }

            live.observeForever(object : Observer<Uri> {
                override fun onChanged(it: Uri?) {
                    Glide.with(view).load(it).into(img)
                    img.isOval = true
                    img.borderWidth = 1f
                    img.borderColor = ContextCompat.getColor(view.context, R.color.colorAccent)
                    live.removeObserver(this)
                }
            })
        }

        return view!!
    }

    override fun getItem(position: Int): Any {
        return lista[position]
    }

    override fun getItemId(position: Int): Long {
        return -1
    }

    override fun getCount(): Int {
        return lista.size
    }


}
