package br.pucpr.appdev.familyday.viewmodel

import android.app.Application
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import br.pucpr.appdev.familyday.enums.Constants
import br.pucpr.appdev.familyday.interfaces.SimpleTextWatcher
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa
import br.pucpr.appdev.familyday.rest.RetrofitFactory
import br.pucpr.appdev.familyday.utils.ActivityUtils
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class TarefaViewModel(application: Application) : AndroidViewModel(application) {

    companion object {
        private const val TAG = "TAREFA-VM"
    }

    val tarefa : Tarefa =
        Tarefa()

    var recorrencia : MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = false
    }


    fun dataInicioListener() : TextWatcher {
        return object : SimpleTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                tarefa.dataInicio = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(s.toString())

                if (!recorrencia.value!!) {
                    tarefa.dataFim = tarefa.dataInicio
                }
            }

        }
    }

    fun dataFimListener() : TextWatcher {
        return object : SimpleTextWatcher() {
            override fun afterTextChanged(s: Editable?) {
                tarefa.dataFim = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(s.toString())
            }

        }
    }

    fun salvar() {
        RetrofitFactory.getInstance().tarefaService.criar(
            ActivityUtils.getOnSharedPreferences(getApplication(), Constants.SharedPreferencesKeys.TOKEN),
            tarefa
        ).apply {
            enqueue(object : Callback<JsonObject> {
                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    Log.e(TAG, "Erro ${t.message}")
                }

                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    Log.d(TAG, "Response : ${response.code()}")

                    when (response.code()) {
                        400 -> {
                            Log.d(TAG, "${response.errorBody()?.string()}")
                        }
                    }
                }

            })
        }
    }

}