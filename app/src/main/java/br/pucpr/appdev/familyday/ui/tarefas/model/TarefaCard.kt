package br.pucpr.appdev.familyday.ui.tarefas.model

import android.os.Parcelable
import br.pucpr.appdev.familyday.R
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TarefaCard(
    var nome: String = "",
    var qtde: Int = 0,
    var categoria: Categoria
) : Parcelable {
    enum class Categoria(val id: Long, val labelId: Int, val statusDashboard: String, val status: String, val color: Int) {
        TOTAL(1, R.string.tarefa_dash_total, "total", "null", -1),
        VALIDACAO(2, R.string.tarefa_dash_aprovacao, "validacao", "VALIDACAO", R.color.status_validacao),
        APROVADO(3, R.string.tarefa_dash_feitas, "aprovado", "APROVADO", R.color.status_aprovado),
        REPROVADO(4, R.string.tarefa_dash_reprovadas, "reprovado", "REPROVADO", R.color.status_reprovado),
        PENDENTE(5, R.string.tarefa_dash_pendentes, "pendente", "PENDENTE", R.color.status_pendente),
        EXPIRADO(6, R.string.tarefa_dash_expirada, "expirado", "EXPIRADO", R.color.status_expirado),
        REFAZER(6, R.string.tarefa_dash_refazer, "refazer", "REFAZER", R.color.status_expirado);
    }
}