package br.pucpr.appdev.familyday.ui.tarefas

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.rest.RetrofitFactory
import br.pucpr.appdev.familyday.rest.TarefaService
import br.pucpr.appdev.familyday.ui.metas.CadastroMetaActivity
import br.pucpr.appdev.familyday.ui.tarefas.model.HistoricoTarefa
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa
import br.pucpr.appdev.familyday.ui.tarefas.viewmodel.DetalheHistoricoViewModel
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.utils.DateUtils
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import com.makeramen.roundedimageview.RoundedImageView
import kotlinx.android.synthetic.main.activity_cadastro_meta.*
import kotlinx.android.synthetic.main.activity_detalhe_historico.*
import kotlinx.android.synthetic.main.activity_detalhe_historico.toolbar
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

class DetalheHistoricoActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    companion object {
        const val PARAM_INTENT_TAREFA_HISTORICO = "historico"
        const val PARAM_INTENT_TAREFA = "tarefa"
        const val TAG = "HISTORICO-ACTIVITY"

        private const val RESULT_CAPTURE_IMAGE = 100
        private const val REQUEST_CAMERA = 101
    }

    private lateinit var historico: HistoricoTarefa
    private lateinit var tarefa: Tarefa
    private lateinit var viewModel: DetalheHistoricoViewModel
    private lateinit var loadingDialog: ProgressDialog
    private val usuarioLogado = FamilyDayApp.getInstance().usuarioLogado!!
    private var imageClicked: RoundedImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhe_historico)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        historico = intent.getParcelableExtra(PARAM_INTENT_TAREFA_HISTORICO)
        tarefa = intent.getParcelableExtra(PARAM_INTENT_TAREFA)

        initializeViewModel()

        loadingDialog = ProgressDialog(TAG, supportFragmentManager)
        loadingDialog.liveDataListener(viewModel.isUpdating(), this)

        popularUI()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        return true
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

    private fun initializeViewModel() {
        viewModel = ViewModelProvider(this).get(DetalheHistoricoViewModel::class.java)
        viewModel.historico = historico
        viewModel.tarefa = tarefa

        viewModel.isSaved().observe(this, Observer {
            if (it) {
                setResult(RESULT_OK)
                finish()
            }
        })
    }

    private fun popularUI() {
        data.text = DateUtils.safeDateToString("dd/MM/yyyy", historico.dataExecucao!!)
        status.text = getString(HistoricoTarefa.Status.values().find { c -> c.status == historico.status }!!.labelId)
        nome_tarefa.text = tarefa.titulo
        nome_usuario.text = historico.usuario?.nome
        descricao.text = tarefa.descricao

        if (historico.status != "VALIDACAO" || usuarioLogado.tipo != Usuario.Tipo.RESPONSAVEL) {
            aprovar_btn.visibility = View.GONE
            reprovar_btn.visibility = View.GONE
            refazer_btn.visibility = View.GONE
        }

        if ((historico.status == "PENDENTE" || historico.status == "REFAZER") && usuarioLogado.tipo == Usuario.Tipo.DEPENDENTE) {
            concluir_btn.visibility = View.VISIBLE

            foto_1.setOnClickListener(this::loadImageOnClick)
            foto_2.setOnClickListener(this::loadImageOnClick)
            foto_3.setOnClickListener(this::loadImageOnClick)
        } else {
            concluir_btn.visibility = View.GONE

            viewModel.carregarFotos().observe(this, Observer {
                if (viewModel.urlFoto1 != null) {
                    Glide.with(this).load(viewModel.urlFoto1).into(foto_1)
                    placeholder_foto_1.visibility = View.GONE
                }

                if (viewModel.urlFoto2 != null) {
                    Glide.with(this).load(viewModel.urlFoto2).into(foto_2)
                    placeholder_foto_2.visibility = View.GONE
                }

                if (viewModel.urlFoto3 != null) {
                    Glide.with(this).load(viewModel.urlFoto3).into(foto_3)
                    placeholder_foto_3.visibility = View.GONE
                }
            })
        }
    }

    fun aprovarOnClick(v: View) {
        viewModel.atualizarStatus(HistoricoTarefa.Status.APROVADO)
    }

    fun reprovarOnClick(v: View) {
        viewModel.atualizarStatus(HistoricoTarefa.Status.REPROVADO)
    }

    fun refazerOnClick(v: View) {
        viewModel.atualizarStatus(HistoricoTarefa.Status.REFAZER)
    }

    fun concluirOnClick(v: View) {
        viewModel.atualizarStatus(HistoricoTarefa.Status.VALIDACAO)
    }

    private fun loadPicture(data: Bitmap) {
        if (imageClicked != null) {
            Glide.with(this).load(data).into(imageClicked!!)

            when (imageClicked!!.id) {
                R.id.foto_1 -> {
                    viewModel.foto1 = data
                    placeholder_foto_1.visibility = View.GONE
                }

                R.id.foto_2 -> {
                    viewModel.foto2 = data
                    placeholder_foto_2.visibility = View.GONE
                }

                R.id.foto_3 -> {
                    viewModel.foto3 = data
                    placeholder_foto_3.visibility = View.GONE
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RESULT_CAPTURE_IMAGE && resultCode == RESULT_OK && data != null) {
            loadPicture(data.extras?.get("data") as Bitmap)
        }
    }

    private fun loadImageOnClick(v: View) {
        imageClicked = v as RoundedImageView
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)) {
            openCamera()
        } else {
            EasyPermissions.requestPermissions(this, "Necessário acesso para adicionar fotos",
                REQUEST_CAMERA, Manifest.permission.CAMERA)
        }
    }

    private fun openCamera() {
        val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(takePicture, RESULT_CAPTURE_IMAGE)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        AlertDialog.Builder(this)
            .setMessage(R.string.acesso_imagens_negado)
            .setNeutralButton(AlertDialog.BUTTON_NEUTRAL, null)
            .create()
            .show()
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        when (requestCode) {
            REQUEST_CAMERA -> openCamera()
        }
    }
}
