package br.pucpr.appdev.familyday.ui.tarefas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.GestureDetector
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.ui.tarefas.adapter.HistoricoTarefaAdapter
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa
import kotlinx.android.synthetic.main.activity_detalhe_tarefa.*

class DetalheTarefaActivity : AppCompatActivity() {

    companion object {
        const val PARAM_INTENT_TAREFA = "tarefa"
        private const val TAG = "TAREFA-DETALHE"
    }

    private lateinit var tarefa: Tarefa

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhe_tarefa)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tarefa = intent.getParcelableExtra(PARAM_INTENT_TAREFA)

        popularInterface()

        val adapter = HistoricoTarefaAdapter(tarefa.historico.toList())
        val gesture = GestureDetector(this, object: GestureDetector.SimpleOnGestureListener() {
            override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
                val viewTouched = historico.findChildViewUnder(e.x, e.y)

                if (viewTouched != null) {
                    val historico = adapter.getByPosition(historico.getChildAdapterPosition(viewTouched))
                    tarefa.membrosLista.forEach {
                        if (historico.membro == it.id) {
                            historico.usuario = it
                        }
                    }

                    val intent = Intent(this@DetalheTarefaActivity, DetalheHistoricoActivity::class.java)
                    intent.putExtra(DetalheHistoricoActivity.PARAM_INTENT_TAREFA_HISTORICO, historico)
//                    intent.putExtra(DetalheHistoricoActivity.PARAM_INTENT_TAREFA_HISTORICO_TITULO, tarefa.titulo)
//                    intent.putExtra(DetalheHistoricoActivity.PARAM_INTENT_ID_TAREFA, tarefa.id)
                    startActivity(intent)
                }

                return true
            }
        })


        historico.layoutManager = LinearLayoutManager(this)
        historico.adapter = adapter
        historico.addOnItemTouchListener(object: RecyclerView.OnItemTouchListener {
            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {

            }

            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                val viewTouched = rv.findChildViewUnder(e.x, e.y)
                return viewTouched != null && gesture.onTouchEvent(e)
            }

            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()

        return true
    }

    private fun popularInterface() {
        titulo.text = tarefa.titulo
        //descricao.text = tarefa.descricao
        pontos.text = tarefa.pontos.toString().plus(if (tarefa.pontos > 1) "pontos" else "ponto")
    }

    fun editarOnClick(v : View) {
        val i = Intent(this, CadastroTarefaActivity::class.java)
        i.putExtra(CadastroTarefaActivity.PARAM_INTENT_TAREFA, tarefa)
        startActivity(i)
    }
}
