package br.pucpr.appdev.familyday.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.enums.ApiState
import br.pucpr.appdev.familyday.model.ServiceStatus
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa
import br.pucpr.appdev.familyday.rest.RetrofitFactory
import br.pucpr.appdev.familyday.rest.TarefaService
import br.pucpr.appdev.familyday.ui.tarefas.model.HistoricoTarefa
import br.pucpr.appdev.familyday.ui.tarefas.model.TarefaCard
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import com.google.gson.Gson
import com.google.gson.JsonObject
import java.net.UnknownHostException
import java.util.*

class TarefaRepository private constructor() {

    private var mTarefaService : TarefaService = RetrofitFactory.getInstance().get().create(TarefaService::class.java)

    private val mApiState = MutableLiveData(ApiState.STOPPED)

    private val status = ServiceStatus()
    private val statusLiveData = MutableLiveData<ServiceStatus>()

    companion object {
        private const val TAG = "TAREFA-REPOSITORY"
        private var instance : TarefaRepository? = null

        fun getInstance() : TarefaRepository {
            if (instance == null)
                instance = TarefaRepository()

            return instance!!
        }

    }

    fun status() = statusLiveData as LiveData<ServiceStatus>

    private fun updateStatus(json: JsonObject, state: ApiState) {
        status.codigo = json.getAsJsonPrimitive("codigo").asString
        status.mensagem = json.getAsJsonPrimitive("mensagem").asString
        status.state = state
    }

    suspend fun buscarPorId(id: String) : Tarefa? {
        try {
            var response = mTarefaService.buscar(id)

            when(response.code()) {
                200 -> {
                    return response.body()!!
                }
            }
        } catch (e: Exception) {

        }

        return null
    }

    suspend fun listar(dataInicio: Date, dataFim: Date, status: String, tipo: Usuario.Tipo) : List<Tarefa> {
        try {
            val response = if (tipo == Usuario.Tipo.RESPONSAVEL) mTarefaService.listar(dataInicio, dataFim, status) else mTarefaService.listarMembro(dataInicio, dataFim, status)

            when (response.code()) {
                200 -> {
                    mApiState.postValue(ApiState.OK)
                    return response.body()!!
                }

                404 -> {

                }

                else -> {

                }
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }

        return listOf()
    }

    suspend fun criar(obj: Tarefa) {
        try {
            val response = mTarefaService.criar(obj)

            when (response.code()) {
                201 -> {
                    mApiState.postValue(ApiState.OK)
                }

                else -> {

                }
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }
    }

    suspend fun atualizar(id: String, obj: Tarefa) {
        try {
            val response = mTarefaService.atualizar(id, obj)

            when (response.code()) {
                201 -> {
                    mApiState.postValue(ApiState.OK)
                }

                else -> {

                }
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }
    }

    suspend fun dashboard(dataInicio: Date, dataFim: Date) : List<TarefaCard> {
        try {
            val response = mTarefaService.dashboard(dataInicio, dataFim)

            when(response.code()) {
                200 -> {
                    val json = response.body()!!
                    val lista = mutableListOf<TarefaCard>()
                    val app = FamilyDayApp.getInstance()

                    TarefaCard.Categoria.values().forEach {
                        lista.add(TarefaCard(nome = app.getString(it.labelId), qtde = json.getAsJsonPrimitive(it.statusDashboard).asInt, categoria = it))
                    }

                    return lista
                }

                else -> {

                }
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }

        return listOf()
    }

    suspend fun atualizarStatusResponsavel(idTarefa: String, historico: HistoricoTarefa, novoStatus: String): Boolean {


        try {
            val response = mTarefaService.atualizarStatusResponsavel(idTarefa, historico)

            when(response.code()) {
                200 -> {
                    mApiState.postValue(ApiState.OK)
                    return true
                }

                400 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "Chamada incorreta para a API")
                    Log.d(TAG, jsonErro.toString())
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                else -> {
                    val errorBody = response.errorBody()?.string()
                    Log.e(TAG, "Erro ao realizar operação na API")
                    Log.e(TAG, errorBody)
                    mApiState.postValue(ApiState.API_ERROR)
                }
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }

        return false
    }

    suspend fun atualizarStatusMembro(idTarefa: String, historico: HistoricoTarefa, novoStatus: String): Boolean {
        try {
            val response = mTarefaService.atualizarStatusMembro(idTarefa, historico)

            Log.d(TAG, "Response code: ${response.code()}")
            when(response.code()) {
                200 -> {
                    mApiState.postValue(ApiState.OK)
                    return true
                }

                400 -> {
                    val jsonErro = Gson().fromJson(response.errorBody()?.string(), JsonObject::class.java)
                    Log.d(TAG, "Chamada incorreta para a API")
                    Log.d(TAG, jsonErro.toString())
                    updateStatus(jsonErro, ApiState.API_ERROR)
                }

                else -> {
                    val errorBody = response.errorBody()?.string()
                    Log.e(TAG, "Erro ao realizar operação na API")
                    Log.e(TAG, errorBody)
                    mApiState.postValue(ApiState.API_ERROR)
                }
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }

        return false
    }

}