package br.pucpr.appdev.familyday.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.pucpr.appdev.familyday.repository.AutenticacaoRepository
import kotlinx.coroutines.launch

class RecuperarSenhaViewModel: ViewModel() {

    var codigo: String = ""
    var email: String = ""
    var senha: String = ""
    var confirmaSenha: String = ""

    private val authRepo = AutenticacaoRepository.instance!!

    private val updating = MutableLiveData<Boolean>()
    private val sent = MutableLiveData<Boolean>()

    fun isUpdating() = updating as LiveData<Boolean>
    fun hasSent() = sent as LiveData<Boolean>

    fun reset() {
        sent.postValue(false)
        updating.postValue(false)
    }

    fun enviarCodigo() {
        updating.postValue(true)
        viewModelScope.launch {
            sent.postValue(authRepo.recuperarSenha(email))
            updating.postValue(false)
        }
    }

    fun validarCodigo() {
        updating.postValue(true)
        viewModelScope.launch {
            sent.postValue(authRepo.validarCodigo(email, codigo))
            updating.postValue(false)
        }
    }

    fun renovarSenha() {
        updating.postValue(true)
        viewModelScope.launch {
            sent.postValue(authRepo.alterarSenha(email, codigo, senha))
            updating.postValue(false)
        }
    }

}