package br.pucpr.appdev.familyday.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import br.pucpr.appdev.familyday.R
import com.appeaser.sublimepickerlibrary.SublimePicker
import com.appeaser.sublimepickerlibrary.helpers.SublimeListenerAdapter
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions
import java.text.DateFormat
import java.util.*


class CalendarRangePicker(val listener: SublimeListenerAdapter) : DialogFragment() {

    var mDateFormatter: DateFormat? = null
    var mTimeFormatter: DateFormat? = null

    var mSublimePicker: SublimePicker? = null

    init {
        mDateFormatter = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
        mTimeFormatter = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
        mTimeFormatter?.timeZone = TimeZone.getTimeZone("GMT+0");
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mSublimePicker = activity?.layoutInflater?.inflate(R.layout.sublime_picker, container) as SublimePicker
        val arguments = arguments
        var options: SublimeOptions? = null

        if (arguments != null) {
            options = arguments.getParcelable("SUBLIME_OPTIONS")
        }

        mSublimePicker?.initializePicker(options, listener)
        return mSublimePicker
    }


}