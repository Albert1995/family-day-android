package br.pucpr.appdev.familyday.interfaces

interface ILoginCallback : ICallback {

    fun loginSucesso()

    fun loginErro()

    fun loginStart()

}