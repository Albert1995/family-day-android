package br.pucpr.appdev.familyday.enums

enum class FrequenciaTarefa(val id: Int, val descricao: String) {
    DIARIA(1, "Diária"),
    SEMANAL(2, "Semanal"),
    MENSAL(3, "Mensal");
}