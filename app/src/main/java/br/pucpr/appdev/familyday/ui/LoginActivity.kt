package br.pucpr.appdev.familyday.ui

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Pair
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.FluxoCadastroActivity
import br.pucpr.appdev.familyday.LoginDependenteActivity
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.databinding.ActivityLoginBinding
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.enums.Constants.GoogleAnalyticsEvents
import br.pucpr.appdev.familyday.viewmodel.LoginViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_login.*

/**
 * Tela de Login do app
 *
 * @author Albert Weihermann
 * @since 1.0
 */
class LoginActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "LOGIN-ACTIVITY"
    }

    private lateinit var loginViewModel: LoginViewModel

    private lateinit var progressDialog: ProgressDialog

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        progressDialog = ProgressDialog()
        binding.viewModel = loginViewModel

        initializeObservers()
        initializeTextWatchers()

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
    }

    /**
     * Encaminha para a tela de cadastro de um novo usuário
     */
    fun cadastroOnClick(v: View) {
        val bundleAnalytics = Bundle()
        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, "Link Cadastro")
        firebaseAnalytics.logEvent(GoogleAnalyticsEvents.LINK, bundleAnalytics)

        startActivity(Intent(this, FluxoCadastroActivity::class.java))
    }

    /**
     * Encaminha para a tela de recuperacao da senha do usuário
     */
    fun recuperarOnClick(v: View) {
        val bundleAnalytics = Bundle()
        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, "Link Cadastro")
        firebaseAnalytics.logEvent(GoogleAnalyticsEvents.LINK, bundleAnalytics)

        startActivity(Intent(this, RecuperarSenhaActivity::class.java))
    }

    /**
     * Realiza o login do usuário no app. Em caso de falha no login, mensagens de erro serão
     * apresentados em tela.
     */
    fun loginOnClick(v: View) {
        val bundleAnalytics = Bundle()
        bundleAnalytics.putString("METODO_LOGIN", "Padrão")
        firebaseAnalytics.logEvent(GoogleAnalyticsEvents.BOTAO, bundleAnalytics)

        loginViewModel.login()
    }

    fun qrCodeOnClick(v: View) {
        startActivity(Intent(this, LoginDependenteActivity::class.java))
    }

    /**
     * Adiciona os observadores da UI na ViewModel
     */
    private fun initializeObservers() {
        // Observa se o login foi realizado com sucesso
        loginViewModel.isLogged().observe(this, Observer {
            val bundleAnalytics = Bundle()
            bundleAnalytics.putBoolean("SUCESSO_LOGIN", it)

            if (it) {
                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                this@LoginActivity.finish()
            } else {
                bundleAnalytics.putString("ERRO_LOGIN", loginViewModel.erro.toString())
            }

            firebaseAnalytics.logEvent(GoogleAnalyticsEvents.LOGIN, bundleAnalytics)
        })

        // Observa se há processo em background para o login em execução
        loginViewModel.isUpdating().observe(this, Observer {
            if (it) {
                progressDialog.showMe(TAG, this@LoginActivity.supportFragmentManager)
            } else if (progressDialog.isShowing()) {
                progressDialog.dismiss()
            }
        })

        // Observa qual o retorno da API para apresentação correta da mensagem
        loginViewModel.apiStatus().observe(this, Observer {
            if (it.codigo == "ERROR_016" || it.codigo == "ERROR_001")
                loginViewModel.erro.mensagem = getString(R.string.erro_016)

            loginViewModel.erro.notifyChange()
        })
    }

    /**
     * Adiciona listeners ao mudar o texto dos campos de texto
     */
    private fun initializeTextWatchers() {
        val textWatcherLimpaMensagemErro = object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                loginViewModel.erro.mensagem = ""
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
        }

        email.addTextChangedListener(textWatcherLimpaMensagemErro)
        senha.addTextChangedListener(textWatcherLimpaMensagemErro)
    }

}
