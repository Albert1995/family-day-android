package br.pucpr.appdev.familyday.rest

import br.pucpr.appdev.familyday.converter.Json
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface TestesService {

    @Json
    @GET("dashboard/push/{titulo}/{texto}")
    suspend fun pushNotification(@Path("titulo") titulo: String, @Path("texto") texto: String): Response<JsonObject>

}