package br.pucpr.appdev.familyday.rest

import br.pucpr.appdev.familyday.converter.Json
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT

interface AutenticacaoAPI {

    @Json
    @POST("login")
    suspend fun login(@Header("Authorization") base64Login: String, @Body body: Map<String, String>): Response<JsonObject>

    @Json
    @POST("login/recuperar-senha")
    suspend fun recuperarSenha(@Body body: Map<String, String>): Response<JsonObject>

    @Json
    @POST("login/validar-codigo")
    suspend fun validarCodigo(@Body body: Map<String, String>): Response<JsonObject>

    @Json
    @PUT("login/alterar-senha")
    suspend fun alterarSenha(@Body body: Map<String, String>): Response<JsonObject>

}