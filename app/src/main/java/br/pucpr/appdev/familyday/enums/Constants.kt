package br.pucpr.appdev.familyday.enums

class Constants {
    companion object {
        const val HEADER_TOKEN = "x-access-token"
    }

    interface SharedPreferencesKeys {

        companion object {
            const val TOKEN = "token"
            const val ID_USUARIO = "usuarioID"
            const val ID_FAMILIA = "familiaID"
            const val TUTORIAL = "tutorial"
        }
    }

    interface GoogleAnalyticsEvents {
        companion object {
            const val BOTAO = "familyday_botao"
            const val LOGIN = "familyday_login"
            const val LINK = "familyday_link"
            const val ERRO = "familyday_erro"
            const val CADASTRO = "familyday_cadastro"
        }
    }
}