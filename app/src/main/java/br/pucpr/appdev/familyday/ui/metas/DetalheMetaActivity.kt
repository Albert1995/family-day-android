package br.pucpr.appdev.familyday.ui.metas

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.databinding.ActivityDetalheMetaBinding
import br.pucpr.appdev.familyday.ui.metas.adapter.MetaUsuarioAdapter
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import br.pucpr.appdev.familyday.ui.metas.viewmodel.DetalheMetaViewModel
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import com.bumptech.glide.Glide

import kotlinx.android.synthetic.main.activity_cadastro_usuario.toolbar
import kotlinx.android.synthetic.main.activity_detalhe_meta.*

class DetalheMetaActivity : AppCompatActivity() {

    companion object {
        const val PARAM_INTENT_META = "meta"

        private const val EDITAR = 100
    }

    private lateinit var viewModel: DetalheMetaViewModel
    private var retorno = Activity.RESULT_CANCELED

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityDetalheMetaBinding = DataBindingUtil.setContentView(this, R.layout.activity_detalhe_meta)

        viewModel = ViewModelProvider(this).get(DetalheMetaViewModel::class.java)
        viewModel.meta = intent.getParcelableExtra(PARAM_INTENT_META)

        popularUI()

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    private fun popularUI() {
        nome.text = viewModel.meta.titulo
        descricao.text = viewModel.meta.descricao

        aviso_sem_dados.visibility = View.GONE

        if (FamilyDayApp.getInstance().usuarioLogado!!.tipo == Usuario.Tipo.DEPENDENTE) {
            editar_btn.visibility = View.GONE
        }

        viewModel.dashboard().observe(this, Observer {
            val adapter = MetaUsuarioAdapter(it, viewModel.meta.id, viewModel.listaUsuario.value!!)
            lista_meta_usuario.layoutManager = LinearLayoutManager(this)
            lista_meta_usuario.adapter = adapter

            if (it.isNotEmpty()) {
                aviso_sem_dados.visibility = View.GONE
            } else {
                aviso_sem_dados.visibility = View.VISIBLE
            }
        })

        if (viewModel.meta.url.isNotBlank()) {
            viewModel.carregarFoto().observe(this, Observer {
                Glide.with(this).load(it).into(foto)
                foto.borderColor = ContextCompat.getColor(this, R.color.colorAccent)
                foto.borderWidth = 1f
                foto.isOval = true
            })
        }
    }

    fun editarOnClick(v: View) {
        val intent = Intent(this, CadastroMetaActivity::class.java)
        intent.putExtra(CadastroMetaActivity.PARAM_INTENT_OBJETO, viewModel.meta)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            setResult(retorno)
            finish()
        }

        return true
    }

    override fun onBackPressed() {
        setResult(retorno)
        super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == EDITAR && resultCode == Activity.RESULT_OK) {
            viewModel.meta = data!!.getParcelableExtra(PARAM_INTENT_META)
            popularUI()
            retorno = resultCode
        }
    }
}
