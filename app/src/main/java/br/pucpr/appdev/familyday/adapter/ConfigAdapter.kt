package br.pucpr.appdev.familyday.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.enums.Constants
import br.pucpr.appdev.familyday.interfaces.ICallbackConfiguracoes
import br.pucpr.appdev.familyday.rest.RetrofitFactory
import br.pucpr.appdev.familyday.rest.TestesService
import br.pucpr.appdev.familyday.ui.MainActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ConfigAdapter(val callback : ICallbackConfiguracoes) : RecyclerView.Adapter<ConfigAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val ctx: Context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(ctx)
        val item: View = inflater.inflate(R.layout.simple_item, parent, false)
        return ViewHolder(item)
    }

    override fun getItemCount(): Int {
        return 2
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when(position) {
            0 -> {
                holder.text.text = "SAIR"

                holder.text.setOnClickListener {
                    FamilyDayApp.getInstance().removeToken()
                    FamilyDayApp.getInstance().save(Constants.SharedPreferencesKeys.TOKEN, "")
                    callback.sair()
                }
            }

            1 -> {
                holder.text.text = "NOTIFICAÇÃO"
                holder.text.setOnClickListener {
                    GlobalScope.launch {
                        RetrofitFactory.getInstance().get().create(TestesService::class.java).pushNotification("Tarefa Concluída", "Alenxadre acabou de finalizar de Varrer a Casa")
                    }
                }
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val text: TextView

        init {
            text = itemView.findViewById(R.id.simple_item_text)
        }

    }

}