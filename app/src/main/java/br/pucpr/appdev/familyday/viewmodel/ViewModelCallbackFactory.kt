package br.pucpr.appdev.familyday.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.interfaces.ICallback
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor

class ViewModelCallbackFactory(val application: Application, val callback: ICallback)
    : ViewModelProvider.AndroidViewModelFactory(application) {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.superclass == AndroidCallbackViewModel::class.java) {
            val kotlinClass : KClass<*> = Class.forName(modelClass.name).kotlin
            return kotlinClass.primaryConstructor?.call(application, callback) as T
        }

        return super.create(modelClass)
    }

}