package br.pucpr.appdev.familyday.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.adapter.TarefaAdapter
import br.pucpr.appdev.familyday.viewmodel.ItemTarefaViewModel
import kotlinx.android.synthetic.main.activity_listar_tarefas.*

class TarefasFragment : Fragment() {

    companion object {
        const val CRIAR_TAREFA : Int = 201
    }

    private lateinit var adapter : TarefaAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_listar_tarefas, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var lista: MutableList<ItemTarefaViewModel> = ArrayList<ItemTarefaViewModel>()

        // TODO: recuperar as tarefas via serviço
        /*RetrofitFactory.getInstance().tarefaService.listar(ActivityUtils.getOnSharedPreferences(this.requireContext(), Constants.SharedPreferencesKeys.TOKEN)).apply {
            enqueue(object : Callback<List<Tarefa>> {
                override fun onFailure(call: Call<List<Tarefa>>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<List<Tarefa>>,
                    response: Response<List<Tarefa>>
                ) {
                    for (i in response.body()) {
                        lista.add(ItemTarefaViewModel(i))
                    }

                    adapter = TarefaAdapter(lista)
                    tarefas_lista.adapter = adapter
                    tarefas_lista.layoutManager = LinearLayoutManager(view.context)
                }

            })
        }*/



//        add_tarefa.setOnClickListener {
//            criarOnClick()
//        }

    }

    fun criarOnClick() {
        //startActivityForResult(Intent(this.context, CadastroTarefaVelhoActivity::class.java), CRIAR_TAREFA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CRIAR_TAREFA -> {
                //adapter.notifyDataSetChanged()
            }
        }
    }



}