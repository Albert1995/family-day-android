package br.pucpr.appdev.familyday.ui.metas

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.databinding.ActivityCadastroMetaBinding
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.enums.ApiState
import br.pucpr.appdev.familyday.ui.MainActivity
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import br.pucpr.appdev.familyday.ui.metas.viewmodel.CadastroMetaViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_cadastro_meta.*
import pub.devrel.easypermissions.EasyPermissions
import java.io.IOException


class CadastroMetaActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    companion object {
        private const val TAG = "CADASTRO-META-ACTIVITY"

        private const val RESULT_CAPTURE_IMAGE = 100
        private const val REQUEST_CAMERA = 101

        private const val RESULT_LOAD_IMAGE = 200
        private const val REQUEST_READ = 201

        const val PARAM_INTENT_OBJETO = "objeto"
    }

    private lateinit var viewModel : CadastroMetaViewModel
    private var loading : ProgressDialog = ProgressDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityCadastroMetaBinding = DataBindingUtil.setContentView(this,
            R.layout.activity_cadastro_meta
        )

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        initializeViewModel()

        binding.viewModel = viewModel
        binding.activity = this

        val meta = intent.getParcelableExtra<Meta>(PARAM_INTENT_OBJETO)
        if (meta != null) {
            viewModel.meta = meta
            pontos_edit.setText(viewModel.meta.pontos.toString())
            salvar_btn.setText(R.string.cadastro_meta_botao_atualizar)

            viewModel.urlFoto().observe(this, object: Observer<Uri> {
                override fun onChanged(it: Uri?) {
                    if (it != null) {
                        loadPicture(it)
                        viewModel.urlFoto().removeObserver(this)
                    }
                }
            })

            viewModel.recuperarFoto()
        }

        add_foto.setOnClickListener(this::loadImageOnClick)
        foto_icon.setOnClickListener(this::loadImageOnClick)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        return true
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

    private fun initializeViewModel() {
        viewModel = ViewModelProvider(this).get(CadastroMetaViewModel::class.java)

        viewModel.isUpdating().observe(this, Observer {updating ->
            if (updating) {
                loading.showMe(TAG, this.supportFragmentManager)
            } else if (loading.isShowing()) {
                loading.dismiss()
            }
        })

        viewModel.isSaved().observe(this, Observer {
            if (it) {
                val intent = Intent()
                intent.putExtra(DetalheMetaActivity.PARAM_INTENT_META, viewModel.meta)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        })
    }

    fun adicionarPontos(pontos: Int) {
        viewModel.meta.pontos += pontos

        if (viewModel.meta.pontos > 10000) {
            viewModel.meta.pontos = 10000
        } else if (viewModel.meta.pontos < 0) {
            viewModel.meta.pontos = 0
        }

        pontos_edit.setText(viewModel.meta.pontos.toString())

    }

    fun salvarOnClick(v: View) {
        viewModel.salvar()
    }

    private fun loadPicture(data: Uri) {
        Glide.with(this).load(data).into(foto_icon)
        viewModel.urlFoto = data
        viewModel.foto = null
        placeholder_foto.visibility = View.GONE
    }

    private fun loadPicture(data: Bitmap) {
        Glide.with(this).load(data).into(foto_icon)
        viewModel.foto = data
        viewModel.urlFoto = null
        placeholder_foto.visibility = View.GONE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            loadPicture(data.data as Uri)
		}

        if (requestCode == RESULT_CAPTURE_IMAGE && resultCode == RESULT_OK && data != null) {
            loadPicture(data.extras?.get("data") as Bitmap)
        }
    }

    private fun loadImageOnClick(v: View) {
        val opt = resources.getStringArray(R.array.captura_foto)

        AlertDialog.Builder(this).setTitle("Como deseja resgatar a foto?")
            .setItems(opt) { dialog: DialogInterface, item: Int ->

                when (opt[item]) {
                    opt[0] -> {
                        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)) {
                            openCamera()
                        } else {
                            EasyPermissions.requestPermissions(this, "Necessário acesso para adicionar fotos", REQUEST_CAMERA, Manifest.permission.CAMERA)
                        }
                    }

                    opt[1] -> {
                        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            openGallery()
                        } else {
                            EasyPermissions.requestPermissions(this, "Necessário acesso para adicionar fotos", REQUEST_READ, Manifest.permission.READ_EXTERNAL_STORAGE)
                        }
                    }
                }
            }.create().show()
    }

    private fun openCamera() {
        val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(takePicture, RESULT_CAPTURE_IMAGE)
    }

    private fun openGallery() {
        val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(i, RESULT_LOAD_IMAGE)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        AlertDialog.Builder(this)
            .setMessage(R.string.acesso_imagens_negado)
            .setNeutralButton(AlertDialog.BUTTON_NEUTRAL, null)
            .create()
            .show()
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        when (requestCode) {
            REQUEST_CAMERA -> openCamera()
            REQUEST_READ -> openGallery()
        }
    }

}
