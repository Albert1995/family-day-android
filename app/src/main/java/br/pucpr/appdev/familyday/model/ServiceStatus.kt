package br.pucpr.appdev.familyday.model

import br.pucpr.appdev.familyday.enums.ApiState

data class ServiceStatus(
    var codigo: String = "0",
    var mensagem: String = "",
    var state: ApiState = ApiState.STOPPED
)