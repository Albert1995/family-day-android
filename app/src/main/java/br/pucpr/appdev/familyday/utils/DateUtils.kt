package br.pucpr.appdev.familyday.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateUtils {

    companion object {

        private fun getCalendar(withHour: Boolean) : Calendar {
            val calendar : Calendar = Calendar.getInstance()

            if (!withHour) {
                calendar[Calendar.HOUR] = 0
                calendar[Calendar.MINUTE] = 0
                calendar[Calendar.SECOND] = 0
                calendar[Calendar.MILLISECOND] = 0
            }

            return calendar
        }

        fun getTodayDate(withHour: Boolean) : Date {
            val calendar : Calendar = getCalendar(withHour)
            return calendar.time
        }

        fun getDateFromTodayInYears(yearsFrom: Int, withHour: Boolean): Date {
            val calendar = getCalendar(withHour)
            calendar.add(Calendar.YEAR, yearsFrom)

            return calendar.time
        }

        fun getDateFromToday(daysFrom: Int, withHour: Boolean) : Date {
            val calendar : Calendar = getCalendar(withHour)
            calendar.add(Calendar.DAY_OF_YEAR, daysFrom)

            return calendar.time
        }

        fun firstDayOfMonth(withHour: Boolean) : Date {
            val calendar : Calendar = getCalendar(withHour)
            calendar[Calendar.DAY_OF_MONTH] = 1

            return calendar.time
        }

        fun lastDayOfMonth(withHour: Boolean) : Date {
            val calendar : Calendar = getCalendar(withHour)
            calendar.add(Calendar.MONTH, 1)
            calendar[Calendar.DAY_OF_MONTH] = 1
            calendar.add(Calendar.DAY_OF_MONTH, -1)

            return calendar.time
        }

        fun firstDayOfWeek(withHour: Boolean) : Date {
            val calendar : Calendar = getCalendar(withHour)
            calendar.add(Calendar.DAY_OF_YEAR, (calendar[Calendar.DAY_OF_WEEK] - calendar.firstDayOfWeek) * -1)
            return calendar.time
        }

        fun lastDayOfWeek(withHour: Boolean) : Date {
            val calendar : Calendar = getCalendar(withHour)
            calendar.add(Calendar.DAY_OF_YEAR, (calendar[Calendar.DAY_OF_WEEK] - calendar.firstDayOfWeek) * -1)
            calendar.add(Calendar.DAY_OF_YEAR, 6)
            return calendar.time
        }

        fun safeStringToDate(format: String, date: String): Date? {
            try {
                return SimpleDateFormat(format).parse(date)
            } catch(e: ParseException) {
                return null
            }
        }

        fun safeDateToString(format: String, date: Date): String? {
            try {
                return SimpleDateFormat(format).format(date)
            } catch(e: ParseException) {
                return null
            }
        }
    }

}