package br.pucpr.appdev.familyday.interfaces

import br.pucpr.appdev.familyday.viewmodel.CadastroViewModel

interface ICadastroCallback : ICallback {

    fun cadastroSucesso()

    fun cadastroErro()

    fun cadastroStart()

    fun validarCadastro() : Boolean

}