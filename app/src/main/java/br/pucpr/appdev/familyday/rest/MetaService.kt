package br.pucpr.appdev.familyday.rest

import br.pucpr.appdev.familyday.converter.Json
import br.pucpr.appdev.familyday.enums.Constants
import br.pucpr.appdev.familyday.model.MetaUsuario
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface MetaService {

    @Json
    @POST("metas")
    fun salvar(@Header(Constants.HEADER_TOKEN) token: String, @Body dto: Meta) : Call<JsonObject>

    @Json
    @POST("metas")
    suspend fun criar(@Body meta: Meta) : Response<JsonObject>

    @GET("metas")
    suspend fun listar(@Header(Constants.HEADER_TOKEN) token: String) : List<Meta>

    @GET("metas")
    suspend fun listar() : Response<List<Meta>>

    @Json
    @PUT("metas/{id}")
    fun atualizar(@Header(Constants.HEADER_TOKEN) token: String, @Path("id") id: String, @Body dto: Meta) : Call<JsonObject>

    @Json
    @PUT("metas/{id}")
    suspend fun atualizar(@Path("id") id: String, @Body dto: Meta) : Response<JsonObject>

    @DELETE("metas/{id}")
    fun excluir(@Header(Constants.HEADER_TOKEN) token: String, @Path("id") id: String) : Call<JsonObject>

    @GET("metas/{id}")
    suspend fun buscar(@Path("id") id: String) : Response<Meta>

    @GET("metas/dashboard/{id}")
    suspend fun dashboard(@Path("id") id: String) : Response<List<MetaUsuario>>

}