package br.pucpr.appdev.familyday.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import br.pucpr.appdev.familyday.interfaces.ICallback

open class AndroidCallbackViewModel(application: Application, val callback: ICallback) : AndroidViewModel(application) {

}