package br.pucpr.appdev.familyday

import android.app.Application
import android.content.Context
import br.pucpr.appdev.familyday.enums.Constants.SharedPreferencesKeys
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import com.google.firebase.messaging.FirebaseMessaging

class FamilyDayApp : Application() {

    companion object {
        private var instance : FamilyDayApp? = null
        private const val sharedPreferencesFile : String = "app-family-day"

        fun getInstance() : FamilyDayApp {
            return instance!!
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        FirebaseMessaging.getInstance().isAutoInitEnabled = true
    }

    var userToken : String = ""
    get() {
        if (field == "")
            field = this.getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE).getString(SharedPreferencesKeys.TOKEN, "")!!
        return field
    }
    private set

    var idFamilia : String = ""
    get() {
        if (field == "")
            field = this.getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE).getString(SharedPreferencesKeys.ID_FAMILIA, "")!!
        return field
    }
    private set

    var usuarioLogado: Usuario? = null

    fun save(key: String, value: String) {
        this.getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE).edit().putString(key, value).apply()
    }

    fun save(key: String, value: Boolean) {
        this.getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE).edit().putBoolean(key, value).apply()
    }

    fun getBooleanKey(key: String): Boolean {
        return this.getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE).getBoolean(key, true)
    }

    fun removeToken() {
        userToken = ""
        saveToken("")
    }

    fun saveToken(token: String) {
        this.save(SharedPreferencesKeys.TOKEN, token)
        userToken = token
    }

}