package br.pucpr.appdev.familyday.repository

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.core.net.toUri
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.tasks.await
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File

class FirebaseStorageRepository {

    companion object {
        private const val TAG = "STORAGE-REPOSITORY"
        var instance : FirebaseStorageRepository? = null
            get() {
                if (field == null)
                    field = FirebaseStorageRepository()
                return field
            }
            private set
    }

    private val storageReference: StorageReference = FirebaseStorage.getInstance().reference

    suspend fun salvarFoto(pasta: String, nomeFoto: String, urlFoto : Uri): String {
        val diretorioFirebase = FamilyDayApp.getInstance().usuarioLogado!!.familiaId.plus("/").plus(pasta).plus("/").plus(nomeFoto)
        val metaStorage = storageReference.child(diretorioFirebase)
        metaStorage.putFile(urlFoto).await()
        return diretorioFirebase
    }

    suspend fun salvarFoto(pasta: String, nomeFoto: String, foto: Bitmap): String {
        val bos = ByteArrayOutputStream()
        foto.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        val bs = ByteArrayInputStream(bitmapdata)

        val diretorioFirebase = FamilyDayApp.getInstance().usuarioLogado!!.familiaId.plus("/").plus(pasta).plus("/").plus(nomeFoto)
        val metaStorage = storageReference.child(diretorioFirebase)
        metaStorage.putStream(bs).await()
        return diretorioFirebase
    }

    suspend fun atualizarFoto(pasta: String, nomeFoto: String, url : Uri) {
        val metaStorage = storageReference.child(FamilyDayApp.getInstance().usuarioLogado!!.familiaId.plus("/").plus(pasta).plus("/").plus(nomeFoto))
        metaStorage.delete().await()
        salvarFoto(pasta, nomeFoto, url)
    }

    suspend fun atualizarFoto(pasta: String, nomeFoto: String, foto: Bitmap) {
        val metaStorage = storageReference.child(FamilyDayApp.getInstance().usuarioLogado!!.familiaId.plus("/").plus(pasta).plus("/").plus(nomeFoto))
        metaStorage.delete().await()
        salvarFoto(pasta, nomeFoto, foto)
    }

    suspend fun recuperarFoto(urlFirebase: String) : Uri? {
        try {
            val file = File.createTempFile("image", ".jpg")
            val metaStorage = storageReference.child(urlFirebase)
            metaStorage.getFile(file).await()
            return file.toUri()
        } catch (e: Exception) {
            Log.e(TAG, "Não foi possível recuperar a foto. Erro: ${e.localizedMessage}")
        }

        return null
    }

}