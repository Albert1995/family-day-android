package br.pucpr.appdev.familyday.enums

enum class ApiState {
    OK, NETWORK_ERROR, API_ERROR, UNKNOWN_ERROR, STOPPED, RUNNING, EXCEPTION
}