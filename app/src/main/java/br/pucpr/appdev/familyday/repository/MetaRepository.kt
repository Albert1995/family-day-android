package br.pucpr.appdev.familyday.repository

import android.R.attr.bitmap
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.net.Uri
import android.util.Log
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.enums.ApiState
import br.pucpr.appdev.familyday.model.MetaUsuario
import br.pucpr.appdev.familyday.rest.MetaService
import br.pucpr.appdev.familyday.rest.RetrofitFactory
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.tasks.await
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.net.UnknownHostException


/**
 * Singleton
 */
class MetaRepository private constructor() {

    private val mMetaService : MetaService = RetrofitFactory.getInstance().get().create(MetaService::class.java)

    private val mMetasLiveData : MutableLiveData<List<Meta>> = MutableLiveData()
    private val mMetas : List<Meta> = listOf()
    private val mApiState : MutableLiveData<ApiState> = MutableLiveData(ApiState.STOPPED)
    private val storageReference: StorageReference = FirebaseStorage.getInstance().reference
    private val storage = FirebaseStorageRepository.instance!!

    companion object {
        private const val PASTA = "metas"
        private var instance : MetaRepository? = null
        private const val TAG = "METAS-REPOSITORY"

        fun getInstance() : MetaRepository {
            if (instance == null) {
                instance = MetaRepository()
            }

            return instance!!
        }
    }

    init {
        mMetasLiveData.value = mMetas

        mApiState.observeForever {
            if (it != ApiState.RUNNING) {
                mApiState.postValue(ApiState.STOPPED)
            }
        }
    }

    fun apiState() = mApiState as LiveData<ApiState>

    suspend fun buscarPorId(id: String) : Meta? {
        try {
            val response = mMetaService.buscar(id)
        } catch (e: Exception) {

        }

        return null
    }

    suspend fun buscarTodos() : List<Meta> {
        try {
            val response = mMetaService.listar()

            when (response.code()) {
                200 -> {
                    mApiState.postValue(ApiState.OK)
                    return response.body()!!
                }

                401 -> {
                    mApiState.postValue(ApiState.API_ERROR)
                }

                else -> {
                    mApiState.postValue(ApiState.UNKNOWN_ERROR)
                }
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }

        return listOf()
    }

    suspend fun criar(obj: Meta, urlFoto : Uri? = null, foto: Bitmap? = null) {
        try {
            val response = mMetaService.criar(obj)
            when (response.code()) {
                201 -> {
                    if (urlFoto != null) {
                        obj.id = response.body()!!.getAsJsonObject("retorno").getAsJsonPrimitive("metaId").asString
                        obj.url = storage.salvarFoto(PASTA, obj.id, urlFoto)
                    } else if (foto != null) {
                        obj.id = response.body()!!.getAsJsonObject("retorno").getAsJsonPrimitive("metaId").asString
                        obj.url = storage.salvarFoto(PASTA, obj.id, foto)
                    }
                    atualizar(obj)

                    mApiState.postValue(ApiState.OK)
                }

                401 -> {
                    mApiState.postValue(ApiState.API_ERROR)
                }

                else -> {
                    mApiState.postValue(ApiState.UNKNOWN_ERROR)
                }
            }
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            Log.e(TAG, e.message)
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }
    }

    suspend fun atualizar(obj: Meta, urlFoto: Uri? = null, foto: Bitmap? = null) {
        try {
            if (urlFoto != null) {
                obj.url = storage.salvarFoto(PASTA, obj.id, urlFoto)
            } else if (foto != null) {
                obj.url = storage.salvarFoto(PASTA, obj.id, foto)
            }

            val response = mMetaService.atualizar(obj.id, obj)
            when (response.code()) {
                201 -> {
                    mApiState.postValue(ApiState.OK)
                }

                401 -> {
                    mApiState.postValue(ApiState.API_ERROR)
                }

                else -> {
                    mApiState.postValue(ApiState.UNKNOWN_ERROR)
                }
            }
        } catch (e: UnknownHostException) {
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }
    }

    suspend fun dashboard(id: String): List<MetaUsuario> {
        try {
            val response = mMetaService.dashboard(id)

            Log.d(TAG, "Response code: ${response.code()}")
            when (response.code()) {
                200 -> {
                    mApiState.postValue(ApiState.OK)
                    return response.body()!!
                }

                else -> {

                }
            }
        } catch (e: UnknownHostException) {
            mApiState.postValue(ApiState.NETWORK_ERROR)
        } catch (e: Exception) {
            mApiState.postValue(ApiState.UNKNOWN_ERROR)
        }

        return listOf()
    }

}