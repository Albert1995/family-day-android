package br.pucpr.appdev.familyday.ui.tarefas.model

import android.os.Parcelable
import androidx.databinding.BaseObservable
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import com.fasterxml.jackson.annotation.JsonGetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSetter
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import java.util.*

@Parcelize
data class Tarefa (
    @get:JsonIgnore
    @set:JsonProperty("id")
    var id: String = "",

    var titulo: String = "",

    var descricao: String = "",

    @field:JsonProperty("exigeComprovacao")
    var necessarioComprovacao: Boolean = true,

    var diariamente : Boolean = false,

    var pontos: Int = 0,

    var dataInicio: Date? = null,

    var dataFim: Date? = null,

    @JsonIgnore
    var membrosLista: MutableList<Usuario> = mutableListOf(),

    @field:JsonIgnore
    var metaTarefa: Meta? = null,

    @field:JsonProperty("personalizado")
    var semana: MutableList<Int> = mutableListOf(),

    @get:JsonIgnore
    @set:JsonProperty("familia")
    var familia: String = "",

    var historico: @RawValue Array<HistoricoTarefa> = arrayOf()

) : BaseObservable(), Parcelable {

    @JsonGetter("meta")
    fun getMetaJson() : String {
        return metaTarefa?.id ?: ""
    }

    @JsonSetter("meta")
    fun setMetaJson(meta: String) {
        this.metaTarefa = Meta(id = meta)
    }

    @JsonGetter("membros")
    fun getMembros() : Array<String> {
        var arr = arrayOf<String>()

        membrosLista.forEach {
            arr += it.id
        }

        return arr
    }

    @JsonSetter("membros")
    fun setMembros(membros: Array<String>) {
        membros.forEach {
            membrosLista.add(Usuario(id = it))
        }
    }
}