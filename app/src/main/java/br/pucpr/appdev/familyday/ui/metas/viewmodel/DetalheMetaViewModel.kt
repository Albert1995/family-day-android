package br.pucpr.appdev.familyday.ui.metas.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.pucpr.appdev.familyday.model.MetaUsuario
import br.pucpr.appdev.familyday.repository.FamiliaRepository
import br.pucpr.appdev.familyday.repository.FirebaseStorageRepository
import br.pucpr.appdev.familyday.repository.MetaRepository
import br.pucpr.appdev.familyday.repository.UsuarioRepository
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import kotlinx.coroutines.launch

class DetalheMetaViewModel: ViewModel() {

    private val storageRepository = FirebaseStorageRepository.instance!!
    private val metaRepository = MetaRepository.getInstance()
    private val familiaRepository = FamiliaRepository.getInstance()
    private val updating = MutableLiveData<Boolean>()


    var meta: Meta = Meta()
    set(value) {
        field = value
        titulo = "${value.titulo} (${value.pontos} Pontos)"
    }

    private val listaMetasUsuario = MutableLiveData<List<MetaUsuario>>()
    val listaUsuario = MutableLiveData<List<Usuario>>()

    var titulo: String = ""

    fun carregarFoto(): LiveData<Uri> {
        val uri = MutableLiveData<Uri>()

        viewModelScope.launch {
            uri.postValue(storageRepository.recuperarFoto(meta.url))
        }

        return uri
    }

    fun dashboard(): MutableLiveData<List<MetaUsuario>> {
        updating.postValue(true)

        viewModelScope.launch {
            listaUsuario.postValue(familiaRepository.listarMembros())
            listaMetasUsuario.postValue(metaRepository.dashboard(meta.id))
            updating.postValue(false)
        }

        return listaMetasUsuario
    }

    fun isUpdating() = updating as LiveData<Boolean>

}