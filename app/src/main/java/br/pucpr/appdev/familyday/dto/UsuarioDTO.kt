package br.pucpr.appdev.familyday.dto

data class UsuarioDTO(
    var id: String = "",
    var nome: String = "",
    var dataNascimento: String = "",
    var telefone: String = "",
    var tipo: String = "",
    var email: String = "",
    var responsavel: String? = "",
    var urlFoto: String = "",
    var senha: String = "",
    var genero: String = "",
    var confirmarSenha: String = "",
    var deviceId: String = "",
    val os : String = "ANDROID"
)