package br.pucpr.appdev.familyday.ui.tarefas.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.ui.tarefas.model.TarefaCard

class TarefaDashboardAdapter(var lista: Array<TarefaCard>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        val card = lista[position]

        if (view == null) {
            val layoutInflater = LayoutInflater.from(parent?.context)
            view = layoutInflater.inflate(R.layout.item_card, null)
        }

        view!!.findViewById<TextView>(R.id.numero).text = card.qtde.toString()
        view.findViewById<TextView>(R.id.texto).text = card.nome

        return view
    }

    override fun getItem(position: Int): Any {
        return lista[position]
    }

    override fun getItemId(position: Int): Long {
        return -1
    }

    override fun getCount(): Int {
        return lista.size
    }

}