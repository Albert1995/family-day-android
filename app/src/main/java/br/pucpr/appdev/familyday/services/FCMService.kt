package br.pucpr.appdev.familyday.services

import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlin.random.Random


class FCMService : FirebaseMessagingService() {

    companion object {
        private val TAG: String = "FCM-Service"
    }

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom())

        // Check if message contains a data payload.
        if (remoteMessage.getData().size > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData())
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage!!.getNotification()!!.body)

            var notification = NotificationCompat.Builder(this, "1")
                .setSmallIcon(R.drawable.ic_alarm_black_24dp)
                .setContentTitle("FamilyDay")
                .setContentText(remoteMessage.getNotification()!!.body)
                .setPriority(NotificationCompat.PRIORITY_HIGH).build()

            NotificationManagerCompat.from(FamilyDayApp.getInstance()).notify(Random.nextInt(), notification)
        }
    }

}
