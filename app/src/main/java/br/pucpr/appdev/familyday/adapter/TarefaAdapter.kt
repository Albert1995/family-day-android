package br.pucpr.appdev.familyday.adapter

import android.content.Context
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.utils.ActivityUtils
import br.pucpr.appdev.familyday.viewmodel.ItemTarefaViewModel

import android.view.animation.Transformation

class TarefaAdapter(private val lista: List<ItemTarefaViewModel>) : RecyclerView.Adapter<TarefaAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val ctx: Context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(ctx)
        val item: View = inflater.inflate(R.layout.item_list_ext, parent, false)
        val holder = ViewHolder(item)
        return holder
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tarefaVM: ItemTarefaViewModel = lista.get(position)
        holder.titulo.text = tarefaVM.tarefa.titulo
        holder.expandirIcone.rotation =
            (if (tarefaVM.isExpandido()) ViewHolder.ITEM_ABERTO else ViewHolder.ITEM_FECHADO)
        holder.detalhes.visibility = if (tarefaVM.isExpandido()) View.VISIBLE else View.GONE
        holder.tarefaVM = tarefaVM
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        companion object {
            const val ITEM_ABERTO: Float = 180f
            const val ITEM_FECHADO: Float = 0f
        }

        val titulo: TextView by ActivityUtils.bind(R.id.item_tarefa_titulo, itemView)
        val detalhes: LinearLayout by ActivityUtils.bind(R.id.item_tarefa_detalhes, itemView)
        val expandirIcone: ImageView by ActivityUtils.bind(R.id.item_tarefa_expandir, itemView)
        val linkDetalhe: TextView by ActivityUtils.bind(R.id.item_tarefa_link_detalhe, itemView)

        lateinit var tarefaVM: ItemTarefaViewModel

        init {
            val content: String = itemView.context.getString(R.string.link_detalhe)
            var link: SpannableString = SpannableString(content)
            link.setSpan(UnderlineSpan(), 0, content.length, 0)
            linkDetalhe.setText(link)

            expandirIcone.setOnClickListener {
                tarefaVM.toogleExpandido()
                expandirIcone.animate().setDuration(200).rotation(if (tarefaVM.isExpandido()) 180f else 0f)

                if (tarefaVM.isExpandido())
                    expandirSecaoDetalhes()
                else
                    recolherSecaoDetalhe()
            }
        }

        private fun expandirSecaoDetalhes() {
            detalhes.measure(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            var actualHeight = detalhes.measuredHeight
            detalhes.layoutParams.height = 0
            detalhes.visibility = View.VISIBLE
            val animation = object : Animation() {
                override fun applyTransformation(
                    interpolatedTime: Float,
                    t: Transformation
                ) {
                    Log.d("anim", String.format("%f", interpolatedTime))
                    detalhes.layoutParams.height = if (interpolatedTime == 1f)
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    else
                        (actualHeight * interpolatedTime).toInt()

                    detalhes.requestLayout()
                }
            }

            animation.duration = (actualHeight / detalhes.context.resources.displayMetrics.density).toLong()
            detalhes.startAnimation(animation)
        }

        private fun recolherSecaoDetalhe() {
            var actualHeight = detalhes.measuredHeight
            val animation = object : Animation() {
                override fun applyTransformation(
                    interpolatedTime: Float,
                    t: Transformation
                ) {
                    Log.d("anim", String.format("%f", interpolatedTime))
                    if (interpolatedTime == 1f)
                        detalhes.visibility = View.GONE
                    else {
                        detalhes.layoutParams.height =
                            actualHeight - (actualHeight * interpolatedTime).toInt()
                        detalhes.requestLayout()
                    }


                }

            }

            animation.duration = (actualHeight / detalhes.context.resources.displayMetrics.density).toLong()
            detalhes.startAnimation(animation)
        }

    }
}

