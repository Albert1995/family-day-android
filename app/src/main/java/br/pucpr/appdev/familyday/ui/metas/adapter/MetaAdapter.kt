package br.pucpr.appdev.familyday.ui.metas.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.FamilyDayApp
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.repository.FirebaseStorageRepository
import br.pucpr.appdev.familyday.ui.metas.model.Meta
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario
import br.pucpr.appdev.familyday.utils.ActivityUtils
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import com.mikhaellopez.circularprogressbar.CircularProgressBar
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MetaAdapter(var lista: List<Meta>, val tipo: Usuario.Tipo) : BaseAdapter() {

    private val storage = FirebaseStorageRepository.instance!!

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val meta = lista[position]
        var view = convertView

        val inflater = LayoutInflater.from(parent?.context)
        view = inflater.inflate(R.layout.item_list_meta, null)

        view?.findViewById<TextView>(R.id.meta)?.text = meta.titulo
        view?.findViewById<TextView>(R.id.pontos)?.text = FamilyDayApp.getInstance().getString(R.string.meta_detalhe_pontos, meta.pontos)
        view?.findViewById<CircularProgressBar>(R.id.progresso)?.visibility = if (tipo == Usuario.Tipo.RESPONSAVEL) View.INVISIBLE else View.VISIBLE

        val img = view.findViewById<RoundedImageView>(R.id.foto)
        if (meta.url.isNotBlank()) {
            val live = MutableLiveData<Uri>()

            GlobalScope.launch {
                live.postValue(storage.recuperarFoto(meta.url))
            }

            live.observeForever(object : Observer<Uri> {
                override fun onChanged(it: Uri?) {
                    Glide.with(view).load(it).into(img)
                    img.isOval = true
                    img.borderWidth = 1f
                    img.borderColor = ContextCompat.getColor(view.context, R.color.colorAccent)
                    live.removeObserver(this)
                }
            })
        }

        return view!!
    }

    override fun getItem(position: Int): Any {
        return lista[position]
    }

    override fun getItemId(position: Int): Long {
        return -1
    }

    override fun getCount(): Int {
        return lista.size
    }


}