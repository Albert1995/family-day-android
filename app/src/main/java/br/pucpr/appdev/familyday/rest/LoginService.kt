package br.pucpr.appdev.familyday.rest

import br.pucpr.appdev.familyday.converter.Json
import br.pucpr.appdev.familyday.model.Token
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface LoginService {

    @Json
    @POST("login")
    fun login(@Header("Authorization") base64Login: String, @Body body: Map<String, String>): Call<JsonObject>

}