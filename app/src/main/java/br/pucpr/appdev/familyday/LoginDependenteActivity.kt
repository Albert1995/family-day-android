package br.pucpr.appdev.familyday

import android.Manifest
import android.content.Intent
import android.hardware.Camera
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.pucpr.appdev.familyday.dialogs.ProgressDialog
import br.pucpr.appdev.familyday.ui.CadastroActivity
import br.pucpr.appdev.familyday.ui.metas.CadastroMetaActivity
import br.pucpr.appdev.familyday.ui.usuarios.CadastroDependenteActivity
import br.pucpr.appdev.familyday.viewmodel.CadastroViewModel
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_login_dependente.*
import me.dm7.barcodescanner.core.CameraUtils
import me.dm7.barcodescanner.zxing.ZXingScannerView
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest

class LoginDependenteActivity : AppCompatActivity(), ZXingScannerView.ResultHandler, EasyPermissions.PermissionCallbacks {

    companion object {
        private const val TAG = "QRCODE-LOGIN"
        private const val REQUEST_CODE_CAMERA = 182
    }

    private lateinit var viewModel: CadastroViewModel
    private lateinit var loadingDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_dependente)

        viewModel = ViewModelProvider(this).get(CadastroViewModel::class.java)

        loadingDialog = ProgressDialog(TAG, supportFragmentManager)
        loadingDialog.liveDataListener(viewModel.isUpdating(), this)
    }

    /********
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!IMPORTANTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!IMPORTANTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!IMPORTANTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!IMPORTANTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     *
     * ALTERAR O CÓDIGO PARA LEITURA DO QRCODE
     */

    override fun onResume() {
        super.onResume()
        qrcode.setResultHandler( this )
        startCamera()
    }

    override fun onPause() {
        super.onPause()
        qrcode.stopCamera()

        val camera = CameraUtils.getCameraInstance()
        if( camera != null ){
            (camera as Camera).release()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        EasyPermissions.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults,
            this )
    }

    override fun onPermissionsDenied(
        requestCode: Int,
        perms: MutableList<String>) {

        askCameraPermission()
    }

    private fun askCameraPermission(){
        EasyPermissions.requestPermissions(
            PermissionRequest.Builder(this, REQUEST_CODE_CAMERA, Manifest.permission.CAMERA)
                .setRationale("A permissão de uso de câmera é necessária para que o aplicativo funcione.")
                .setPositiveButtonText("Ok")
                .setNegativeButtonText("Cancelar")
                .build() )
    }

    override fun onPermissionsGranted(
        requestCode: Int,
        perms: MutableList<String>) {

        startCamera()
    }

    private fun startCamera(){
        if( EasyPermissions.hasPermissions( this, Manifest.permission.CAMERA ) ){
            qrcode.startCamera()
        } else {
            askCameraPermission()
        }
    }
    override fun handleResult(result: Result?) {
        if (result?.barcodeFormat == BarcodeFormat.QR_CODE) {
            Log.i(TAG, "Conteúdo do código lido: ${result.text}")

            FamilyDayApp.getInstance().saveToken(result.text)
            viewModel.recuperarUsuario().observe(this, Observer {

                if (it != null) {
                    FamilyDayApp.getInstance().removeToken()
                    val i = Intent(this, CadastroActivity::class.java)
                    i.putExtra(CadastroActivity.PARAM_INTENT_USUARIO, it)
                    i.putExtra(CadastroActivity.PARAM_INTENT_TOKEN, result.text)
                    startActivity(i)
                } else {
                    erro_qrcode.visibility = View.VISIBLE
                    FamilyDayApp.getInstance().removeToken()
                }

            })


        }
    }
}
