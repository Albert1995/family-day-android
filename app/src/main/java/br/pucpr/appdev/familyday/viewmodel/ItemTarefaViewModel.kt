package br.pucpr.appdev.familyday.viewmodel

import androidx.lifecycle.ViewModel
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa

class ItemTarefaViewModel(
    val tarefa: Tarefa
) : ViewModel() {

    private var expandido: Boolean = false

    fun toogleExpandido() {
        expandido = !expandido
    }

    fun isExpandido() : Boolean {
        return expandido
    }

}