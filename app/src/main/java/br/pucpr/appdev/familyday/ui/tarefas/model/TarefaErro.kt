package br.pucpr.appdev.familyday.ui.tarefas.model

import androidx.databinding.BaseObservable

class TarefaErro : BaseObservable() {

    var titulo = ""
    var pontos = ""
    var meta = ""
    var dataInicio = ""
    var dataFim = ""
    var membros = ""

    fun hasErros() : Boolean {
        return titulo.isNotBlank()
            .or(pontos.isNotBlank())
            .or(meta.isNotBlank())
            .or(dataInicio.isNotBlank())
            .or(dataFim.isNotBlank())
            .or(membros.isNotBlank())
    }

    override fun toString(): String {
        return "TarefaErro(titulo='$titulo', pontos='$pontos', meta='$meta', dataInicio='$dataInicio', dataFim='$dataFim', membros='$membros')"
    }


}