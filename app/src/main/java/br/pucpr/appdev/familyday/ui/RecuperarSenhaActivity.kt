package br.pucpr.appdev.familyday.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import br.pucpr.appdev.familyday.R
import kotlinx.android.synthetic.main.activity_recuperar_senha.*

class RecuperarSenhaActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recuperar_senha)

        val adapter = FragmentAdapter(supportFragmentManager, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT)
        adapter.lista = listOf(
            RecuperarSenhaEmailFragment(),
            RecuperarSenhaCodigoFragment(),
            RecuperarSenhaNovaSenhaFragment()
        )
        view_pager.adapter = adapter

    }

    fun proximoPasso() {
        view_pager.setCurrentItem(view_pager.currentItem + 1, true)
    }

    class FragmentAdapter(fm: FragmentManager, behavior: Int) :
        FragmentStatePagerAdapter(fm, behavior) {

        var lista: List<Fragment> = listOf()

        override fun getItem(position: Int): Fragment {
            return lista[position]
        }

        override fun getCount(): Int {
            return lista.size
        }

    }
}
