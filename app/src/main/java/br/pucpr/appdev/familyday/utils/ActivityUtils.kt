package br.pucpr.appdev.familyday.utils

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.annotation.IdRes

class ActivityUtils {

    companion object {

        fun <T : View> bind(@IdRes res : Int, view: View) : Lazy<T> {
            @Suppress("UNCHECKED_CAST")
            return lazy { view.findViewById(res) as T }
        }

        fun <T : View> bind(@IdRes res : Int, activity: Activity) : Lazy<T> {
            @Suppress("UNCHECKED_CAST")
            return lazy { activity.findViewById(res) as T }
        }

        fun saveOnSharedPreferences(ctx: Context, key: String, value: String) {
            var prefs = ctx.getSharedPreferences("app-family-day", Context.MODE_PRIVATE)

            prefs.edit().putString(key, value).apply()
        }

        fun removeOnSharedPreferences(ctx: Context, key: String) {
            var prefs = ctx.getSharedPreferences("app-family-day", Context.MODE_PRIVATE)
            prefs.edit().remove(key).apply()
        }

        fun getOnSharedPreferences(ctx: Context, key: String): String {
            var prefs = ctx.getSharedPreferences("app-family-day", Context.MODE_PRIVATE)
            return prefs.getString(key, "")
        }

    }

}

fun <T : View> Activity.bind(@IdRes res : Int) : Lazy<T> {
    @Suppress("UNCHECKED_CAST")
    return lazy { findViewById(res) as T }
}