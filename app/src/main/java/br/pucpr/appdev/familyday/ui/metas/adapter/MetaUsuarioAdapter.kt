package br.pucpr.appdev.familyday.ui.metas.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.pucpr.appdev.familyday.R
import br.pucpr.appdev.familyday.model.MetaUsuario
import br.pucpr.appdev.familyday.ui.usuarios.model.Usuario

class MetaUsuarioAdapter(var lista: List<MetaUsuario>, var metaId: String, var listaUsuarios: List<Usuario>): RecyclerView.Adapter<MetaUsuarioAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MetaUsuarioAdapter.ViewHolder {
        val ctx: Context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(ctx)
        val item: View = inflater.inflate(R.layout.item_list_meta_usuario, parent, false)
        return ViewHolder(
            item
        )
    }

    override fun getItemCount(): Int {
        return lista.size
    }

    override fun onBindViewHolder(holder: MetaUsuarioAdapter.ViewHolder, position: Int) {
        val item = lista[position]
        val meta = item.metas.first { m -> m.id == metaId }

        if (item.metas.firstOrNull { m -> m.id == metaId } != null) {
            holder.nome.text =
                listaUsuarios.first { u -> u.id == lista[position].usuario }.primeiroNome()
            holder.progresso.progress = ((meta.pontosGanhos.toFloat() / meta.pontosAlvo.toFloat()) * 100).toInt()
            holder.pontos.text = "${meta.pontosGanhos} / ${meta.pontosAlvo}"
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val nome = itemView.findViewById<TextView>(R.id.nome)
        val progresso = itemView.findViewById<ProgressBar>(R.id.progresso)
        val pontos = itemView.findViewById<TextView>(R.id.pontos)

    }

}