package br.pucpr.appdev.familyday.rest

import br.pucpr.appdev.familyday.converter.Json
import br.pucpr.appdev.familyday.enums.Constants
import br.pucpr.appdev.familyday.ui.tarefas.model.HistoricoTarefa
import br.pucpr.appdev.familyday.ui.tarefas.model.Tarefa
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*
import java.util.*

interface TarefaService {

    @Json
    @POST("tarefas")
    fun criar (@Header(Constants.HEADER_TOKEN) token: String, @Body tarefa: Tarefa): Call<JsonObject>

    @Json
    @POST("tarefas")
    suspend fun criar (@Body tarefa: Tarefa): Response<JsonObject>

    @PUT("tarefas/{id}")
    fun atualizar (@Header(Constants.HEADER_TOKEN) token: String, @Path("id") id: String, @Body tarefa: Tarefa)

    @Json
    @PUT("tarefas/{id}")
    suspend fun atualizar (@Path("id") id: String, @Body tarefa: Tarefa): Response<JsonObject>

    @DELETE("tarefas/{id}")
    fun excluir (@Header(Constants.HEADER_TOKEN) token: String, @Path("id") id: String)

    @GET("tarefas")
    fun listar(@Header(Constants.HEADER_TOKEN) token: String) : Call<List<Tarefa>>

    @GET("tarefas/listar-tarefas-familia/{dataInicio}/{dataFim}/{status}")
    suspend fun listar(@Path("dataInicio") dataInicio: Date,
                       @Path("dataFim") dataFim: Date,
                       @Path("status") status: String) : Response<List<Tarefa>>

    @GET("tarefas/listar-tarefas-membro/{dataInicio}/{dataFim}/{status}")
    suspend fun listarMembro(@Path("dataInicio") dataInicio: Date,
                             @Path("dataFim") dataFim: Date,
                             @Path("status") status: String) : Response<List<Tarefa>>

    @Json
    @GET("tarefas/listar-dashboard/{dataInicio}/{dataFim}")
    suspend fun dashboard(@Path("dataInicio") dataInicio: Date, @Path("dataFim") dataFim: Date) : Response<JsonObject>

    @GET("tarefas/{id}")
    suspend fun buscar(@Path("id") id: String) : Response<Tarefa>

    @Json
    @PUT("tarefas/responsavel-atualiza-status/{id}")
    suspend fun atualizarStatusResponsavel(@Path("id") id: String, @Body body: HistoricoTarefa) : Response<JsonObject>

    @Json
    @PUT("tarefas/membro-atualiza-status/{id}")
    suspend fun atualizarStatusMembro(@Path("id") id: String, @Body body: HistoricoTarefa) : Response<JsonObject>

}